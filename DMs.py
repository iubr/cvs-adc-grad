#               
#                               ADC_GRADIENT
#     --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------

# load required packages
import adcc
from adcc.functions import empty_like, zeros_like, ones_like 
#(can be replaced by np.zeros, np.ones)
import numpy as np
import time
import Amplitude_Response as AR

class DM:
    def __init__(self, refstate, vec=None, order="adc0"):
        """Initializes the density matrices required for analytical
        (CVS)-ADC gradients
        
        Parameters:
        ----------
        refstate: reference state from an adc calculation
        vec     : excitation vector corresponding to
                  the excited state of interest
        order   : order of the adc matrix ("0"/"adc0","1"/"adc1","2"/"adc2",
                  "2x/adc2x"); 
                  "adc3" not available yet;
                  use "cvs2-singles" and "cvs2-doubles" 
                  to compute the components of the density matrices 
                  resulting only from the singles and, respectively,
                  doubles block of the ADC matrix (currently only for cvs-adc2).

                  cvs2-debug: computes only the components of the 
                  density matrices coming from the X^dagger M X.
                  hf: computes only the components from the hf refrence state.
                  cvs2-couplings: computes only the components related 
                  to the coupling blocks.
                  mp2: computes the amplitude response components 
                  of the density matrix, using only the MP2 t multipliers.
                  cvs2-amplitude: computes only the amplitude response components
                  of the density matrix.
                  mp2-energy: computes only the 2DM corresponding to the
                  MP2 correction to the ground state energy.
                  gs_mp2: computes the density matrices required for 
                  computing the GS MP(2) gradient
        """
        self.reference_state = refstate
        self.vector = vec
        self.order = order
        self.blocks = []
        self.matrix = {}

    def remove_blocks(self):
        """
        Deletes all the blocks in the density matrix
        """
        for block in self.blocks:
            del self.matrix[block]
        self.blocks=[]

    def reset(self, refstate, vec):
        """
        Resets the reference state and excitation vector

        Parameters:
        ----------
        refstate: new reference state
        vec: new excitation vector
        """
        self.reference_state = refstate
        self.vector = vec

    def compute_blocks(self, blocks, t=None, dm0=None, verbose=False):
        start = time.time()
        #print("Computing DM blocks... ", blocks)
        for block in blocks:
            self.compute_block(block, t, dm0)
        stop = time.time()
        if blocks:
            if len(blocks[0])==4:
                pdm = '2'
            else:
                pdm = '1'
            if verbose:
                print(pdm+"PDM blocks... ", blocks)
                txt = ("%7s %1sPDMs  took %10.5f s."
                     % (self.order, pdm, stop-start)) 
                print(txt) 

class DM_1(DM):
    def add_block(self, block_type):
        """Adds a block of type block_type to the multiplier

        Parameters:
        ----------
        block_type: type of block: "CC", "OO", "VV"
        """
        if block_type in ["CC", "OO", "VV"]:
                if block_type not in self.blocks:
                    self.blocks.append(block_type)

                    if block_type == "CC":
                        self.matrix["CC"] = adcc.zeros_like(
                                self.reference_state.fock("o2o2")).to_ndarray()
                        #maybe do this directly with numpy??

                    elif block_type == "OO":
                        self.matrix["OO"] = adcc.zeros_like(
                                self.reference_state.fock("o1o1")).to_ndarray()

                    elif block_type == "VV":
                        self.matrix["VV"]=adcc.zeros_like(
                                self.reference_state.fock("v1v1")).to_ndarray()
                else:
                    pass
        else:
            errtxt = "Not a valid block for the one-particle density matrix."
            raise ValueError(block_type, errtxt)

        
    def __str__(self):
        txt=''
        for block in self.blocks:
            txt = txt+block+'\n'
            for p in range(self.matrix[block].shape[0]):
                for q in range(self.matrix[block].shape[1]):
                    to_add = "%7.2f" % self.matrix[block][p,q]
                    txt=txt+to_add
                txt=txt+'\n'   
            txt = txt+'-------------------End--------------------\n\n'
        return txt 
    
    def compute_block(self, block_type, t=None, dm0=None):
        """Computes the elements of the 1-particle DM
        required for (CVS)-ADC analytical gradients,
        based on the type of DM ("CC", "OO" or "VV") 
        and the order of the ADC matrix.
        """
        if block_type not in self.blocks:
            self.add_block(block_type)
        
        # TODO: maybe use "if self.order in" rather than "not in"
        if self.order not in ["hf", "cvs2-doubles", "cvs2-amplitude",
                              "mp2", "mp2-energy", "gs_mp2"]:
            try:
                v_s = self.vector["ph"].to_ndarray()
            except (ValueError, TypeError):
                raise ValueError(self.order, block_type, "No singles block...")

        if self.order in ["cvs2-doubles", "cvs2", "cvs-adc2", "cvs2-amplitude",
                          "mp2", "mp2-energy", "adc2", "cvs2-debug",
                          "cvs2x", "cvs-adc2x", "cvs2x-debug"]:  
            try:
                v_d = self.vector["pphh"].to_ndarray()
            except (ValueError, IndexError):
                v_d = adcc.zeros_like(
                        self.reference_state.eri("o1o2v1v1")).to_ndarray()
                #print(v_d)
                #self.vector["d"].to_ndarray()
                #raise IndexError(self.order, block_type, "No doubles block...")

        if self.order in ["cvs2", "cvs-adc2", "cvs2x", "cvs-adc2x"]:
            t2oovv = adcc.LazyMp(
                        self.reference_state).t2("o1o1v1v1").to_ndarray()
            t2ocvv = adcc.LazyMp(
                        self.reference_state).t2("o1o2v1v1").to_ndarray()
            t2ccvv = adcc.LazyMp(
                        self.reference_state).t2("o2o2v1v1").to_ndarray()
            if t == None:
                try:
                    t = AR.T_Multipliers(self.reference_state,
                                         self.vector, self.order)
                    # Only OOVV needs to be calculated separately
                    # since ccvv and ocvv are 1/4 t2ccvv, 1/4t2ocvv respectively
                    t.add_block(block_type="OOVV") 
                    t.compute_block(block_type="OOVV") 
                except (ValueError, TypeError):
                    errtxt = ":1DM, couldn't get the amplitude rsp."
                    errtxt += " multipliers."
                    raise ValueError(self.order, errtxt)

        if self.order in ["gs_mp2", "adc2"]:
            t2oovv = adcc.LazyMp(
                        self.reference_state).t2("o1o1v1v1").to_ndarray()

        if block_type == "CC":
            if self.order in ["hf"]:
                np.fill_diagonal(self.matrix[block_type], 1.0)
            elif self.order in ["cvs-adc0", "cvs-adc1", "cvs0", "cvs1",
                                "cvs2-singles"]:
                self.matrix[block_type] = -np.einsum('Ja,Ia->IJ',
                                                      v_s, v_s, optimize=True)
            elif self.order in ["cvs2", "cvs-adc2", "cvs2x", "cvs-adc2x"]:
                self.matrix[block_type] = ( -np.einsum('Ja,Ia->IJ',
                                                       v_s, v_s, optimize=True) 
                                            -np.einsum('kJba,kIba->IJ',
                                                        v_d, v_d, optimize=True)
                                            -0.5*np.einsum('IKab,JKab->IJ',
                                                           t2ccvv, t2ccvv,
                                                           optimize=True)
                                            -0.5*np.einsum('kIab,kJab->IJ',
                                                            t2ocvv, t2ocvv,
                                                            optimize=True)
                                            )
            elif self.order in ["cvs2-doubles"]:
                self.matrix[block_type] = -np.einsum('kJba,kIba->IJ',
                                                      v_d, v_d, optimize=True)
            elif self.order in ["cvs2-debug", "cvs2x-debug"]:
                self.matrix[block_type] = ( -np.einsum('Ja,Ia->IJ',
                                                       v_s, v_s, optimize=True) 
                                            -np.einsum('kJba,kIba->IJ',
                                                        v_d, v_d, optimize=True)
                                            )
            else:
                self.add_block(block_type)
                #raise ValueError(self.order, "Not implemented.")  
                  
        elif block_type == "OO":
            if self.order in ["hf"]:
                np.fill_diagonal(self.matrix[block_type], 1.0)
            elif self.order in ["gs_mp2"]:
                self.matrix[block_type] = (-0.5*np.einsum('ikab,jkab->ij',
                                                          t2oovv, t2oovv,
                                                          optimize=True)
                                           )
            elif self.order in ["0","adc0","1","adc1"]:
                #np.fill_diagonal(self.matrix[block_type], 1.0)
                #above added the HF ref. state density matrix
                self.matrix[block_type] += -np.einsum('ja,ia->ij',
                                                       v_s, v_s, optimize=True)
            elif self.order in ["cvs0", "cvs-adc0", "cvs1", "cvs-adc1"]:
                pass

            elif self.order in ["cvs2", "cvs-adc2", "cvs2x", "cvs-adc2x"]:
                self.matrix[block_type] = ( - np.einsum('jKba,iKba->ij',
                                                         v_d, v_d,
                                                         optimize=True) 
                                            - np.einsum('ikab,jkab->ij',
                                                         t.matrix["OOVV"],
                                                         t2oovv, optimize=True) 
                                            - np.einsum('jkab,ikab->ij',
                                                         t2oovv,
                                                         t.matrix["OOVV"], 
                                                         optimize=True)
                                            -0.5*np.einsum('iKab,jKab->ij',
                                                            t2ocvv, t2ocvv,
                                                            optimize=True)
                                            -0.5*np.einsum('ikab,jkab->ij',
                                                            t2oovv, t2oovv,
                                                            optimize=True)
                                            )
            elif self.order in ["adc2"]:
                self.matrix[block_type] = ( -np.einsum('ia,ja->ij', v_s, v_s,
                                                        optimize=True)
                                            -2.0*np.einsum('ikab,jkab->ij',
                                                        v_d, v_d,
                                                        optimize=True)
                                            )
            elif self.order in ["cvs2-amplitude", "mp2"]:
                self.matrix[block_type] = ( - np.einsum('ikab,jkab->ij',
                                                         t.matrix["OOVV"],
                                                         t2oovv, optimize=True) 
                                            - np.einsum('jkab,ikab->ij',
                                                         t2oovv,
                                                         t.matrix["OOVV"], 
                                                         optimize=True)
                                            )
            elif self.order in ["cvs2-debug", "cvs2x-debug"]:
                self.matrix[block_type] = -np.einsum('jKba,iKba->ij',
                                                      v_d, v_d, optimize=True) 
            elif self.order in ["cvs2-singles"]:
                pass
            elif self.order in ["cvs2-doubles"]:
                self.matrix[block_type] = -np.einsum('jKba,iKba->ij',
                                                      v_d, v_d, optimize=True)
            else:
                self.add_block(block_type)
                #raise ValueError(self.order, "Not implemented.")
                
        elif block_type=="VV": 
            if self.order in ["0","adc0","1","adc1"]: 
                self.matrix[block_type] = np.einsum('ib,ia->ab',
                                                     v_s, v_s, optimize=True)
            elif self.order in ["gs_mp2"]:
                self.matrix[block_type] = (0.5*np.einsum('ijac,ijbc->ab',
                                                          t2oovv, t2oovv,
                                                          optimize=True)
                                           )
            elif self.order in ["cvs-adc0", "cvs-adc1", "cvs0", "cvs1"]:
                self.matrix[block_type] = np.einsum('Ib,Ia->ab',
                                                     v_s, v_s, optimize=True)
            elif self.order in ["cvs2", "cvs-adc2", "cvs2x", "cvs-adc2x"]:
                self.matrix[block_type] = ( np.einsum('Ib,Ia->ab',
                                                       v_s, v_s, optimize=True) 
                                            + 2.0*np.einsum('jIcb,jIca->ab',
                                                             v_d, v_d,
                                                             optimize=True)
                                            + np.einsum('ijac,ijbc->ab',
                                                         t.matrix["OOVV"],
                                                         t2oovv, optimize=True)
                                            + np.einsum('ijbc,ijac->ab',
                                                         t.matrix["OOVV"],
                                                         t2oovv, optimize=True)
                                            + 0.5*np.einsum('IJac,IJbc->ab',
                                                             t2ccvv, t2ccvv,
                                                             optimize=True)
                                            + 0.5*np.einsum('ijac,ijbc->ab',
                                                             t2oovv, t2oovv,
                                                             optimize=True)
                                            + np.einsum('iJac,iJbc->ab',
                                                         t2ocvv, t2ocvv,
                                                         optimize=True)
                                            )
            elif self.order in ["adc2"]:
                self.matrix[block_type] = ( np.einsum('ib,ia->ab', v_s, v_s,
                                                        optimize=True)
                                            +2.0*np.einsum('ijac,ijbc->ab',
                                                        v_d, v_d,
                                                        optimize=True)
                                            )
            elif self.order in ["cvs2-amplitude", "mp2"]:
                self.matrix[block_type] = ( np.einsum('ijac,ijbc->ab',
                                                       t.matrix["OOVV"],
                                                       t2oovv, optimize=True)
                                            + np.einsum('ijbc,ijac->ab',
                                                         t.matrix["OOVV"],
                                                         t2oovv, optimize=True)
                                            )
            elif self.order in ["cvs2-debug", "cvs2x-debug"]:
                self.matrix[block_type] = ( np.einsum('Ib,Ia->ab',
                                                       v_s, v_s, optimize=True) 
                                            + 2.0*np.einsum('jIcb,jIca->ab',
                                                             v_d, v_d,
                                                             optimize=True)
                                            )
            elif self.order in ["cvs2-singles"]: 
                self.matrix[block_type] = np.einsum('Ib,Ia->ab',
                                                     v_s, v_s, optimize=True)
            elif self.order in ["cvs2-doubles"]:
                self.matrix[block_type] = 2.0*np.einsum('jIcb,jIca->ab',
                                                        v_d, v_d, optimize=True)
            else:
                self.add_block(block_type)
                #raise NotImplementedError(self.order)

        else:
            errtxt = "is not a valid block for the one-particle density matrix."
            raise ValueError(block_type, errtxt)
  
    def dot(self, mat, block_type):
        """Computes the dot product between the block_type block
        of the density matrix and another matrix of the same dimensions. 

        Parameters:
        ----------
        mat: matrix with the same shape as the requested block
             of the Lagrange multiplier
        block_type: requested block of the Lagrange multipliers
                    ("CC", "OO", "VV", "OC", "OV", "CV")
        """
        if block_type not in self.blocks:
            raise ValueError(block_type, " does not exist.")
        else:
            if mat.shape != self.matrix[block_type].shape: 
                raise ValueError("Matrices have different dimensions!")
            else:
                E = np.einsum('pq,pq->',mat,self.matrix[block_type],
                               optimize=True)
                return E         

class DM_2(DM):
    def add_block(self, block_type):
        if block_type in ["CVCV", "OVOV", "OCCV", "OOVV", "OVVV",
                          "CCVV", "OCVV", "OOOO", "CCCC", "OCOC",
                          "OOOV", "VVVV"]:
            if block_type not in self.blocks:
                self.blocks.append(block_type)

                if block_type =="CVCV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o2v1o2v1")).to_ndarray()
                elif block_type == "OCCV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o1o2o2v1")).to_ndarray()
                elif block_type == "OOVV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o1o1v1v1")).to_ndarray()
                elif block_type == "OVVV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o1v1v1v1")).to_ndarray()
                elif block_type == "OVOV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o1v1o1v1")).to_ndarray()
                elif block_type == "CCVV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o2o2v1v1")).to_ndarray()
                elif block_type == "OCVV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o1o2v1v1")).to_ndarray()
                elif block_type == "OOOO":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o1o1o1o1")).to_ndarray()
                elif block_type == "CCCC":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o2o2o2o2")).to_ndarray()
                elif block_type == "OCOC":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o1o2o1o2")).to_ndarray()
                elif block_type == "OOOV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("o1o1o1v1")).to_ndarray()
                elif block_type == "VVVV":
                    self.matrix[block_type] = adcc.zeros_like(
                            self.reference_state.eri("v1v1v1v1")).to_ndarray()
            else:
                pass 
        else:
            errtxt = "is not a valid block for the two-particle density matrix."
            raise ValueError(block_type, errtxt)

    def __str__(self):
        txt=''
        for block in self.blocks:
            txt = txt+block+'\n'
            for p in range(self.matrix[block].shape[0]):
                for q in range(self.matrix[block].shape[1]):
                    for r in range(self.matrix[block].shape[2]):
                        for s in range(self.matrix[block].shape[3]):
                            to_add = "%7.2f" % self.matrix[block][p,q,r,s]
                            txt=txt+to_add
                        txt=txt+'\n'
                    txt=txt+'------\n'   
                txt = txt+'******\n'
            txt = txt+'-------------------End--------------------\n\n'
        return txt


    def dot(self, mat, block_type):
        """Computes the dot product between the 2-particle DM
        and another matrix of the same dimensions. 
        Required, for example, to compute the excitation energy 

        Parameters:
        ----------
        mat: matrix with the same shape as the 2-DM matrix
        (eg. corresponding block of the ERI matrix)
        """
        if block_type not in self.blocks:
            raise ValueError(block_type, " does not exist.")
        else:
            if mat.shape != self.matrix[block_type].shape:
                raise ValueError("Matrices have different dimensions!")

            else:
                E = np.einsum('pqrs,pqrs->',mat,
                               self.matrix[block_type],optimize=True)
                return E
    
    def compute_block(self, block_type, t=None, dm0=None, DM1=None):
        """Computes the elements of the 2-particle DM
        required for (CVS)-ADC analytical gradients, 
        based on the type of DM ("CVCV", "OCCV", "OOVV" or "OVVV")
        and the order of the ADC matrix.
        """

        if block_type not in self.blocks:
            self.add_block(block_type)

        if self.order not in ["cvs2-doubles", "cvs2-amplitude", 
                              "mp2", "mp2-energy", "hf", "gs_mp2"]:
            try:
                v_s = self.vector["ph"].to_ndarray()
            except (ValueError, TypeError):
                errtxt = "2PDM, the excitation vector has no singles block."
                raise ValueError(self.order, errtxt)

        if self.order not in ["hf", "adc0", "0", "1", "adc1", "cvs0",
                              "cvs1", "cvs-adc0", "cvs-adc1", 
                              "cvs2-singles", "cvs2-amplitude", 
                              "mp2", "mp2-energy", "gs_mp2"]:
            try:
                v_d = self.vector["pphh"].to_ndarray()
            except (ValueError, TypeError):
                v_d = adcc.zeros_like(
                        self.reference_state.eri("o1o2v1v1")).to_ndarray()

        if self.order in ["gs_mp2"]:
            t2oovv = adcc.LazyMp(
                        self.reference_state).t2("o1o1v1v1").to_ndarray()

        if self.order=="adc2":
            t2oovv = adcc.LazyMp(
                        self.reference_state).t2("o1o1v1v1").to_ndarray()
            if dm0 == None:
                dm0 = DM_1(self.reference_state, self.vector, "adc0")
                dm0.compute_blocks(["OO","VV"])
            if DM1 == None:
                DM1 = DM_2(self.reference_state, self.vector, "adc1")
                DM1.compute_block("OVOV")
        
        #missing 2, adc2, mp2, mp2-energy...
        if self.order in ["cvs2", "cvs-adc2", "cvs2-singles", "cvs2-debug",
                          "cvs2-amplitude", "mp2", "mp2-energy", "cvs2x",
                          "cvs-adc2x", "cvs2x-debug"]:
            t2oovv = adcc.LazyMp(
                            self.reference_state).t2("o1o1v1v1").to_ndarray()
            t2ocvv = adcc.LazyMp(
                            self.reference_state).t2("o1o2v1v1").to_ndarray()
            t2ccvv = adcc.LazyMp(
                            self.reference_state).t2("o2o2v1v1").to_ndarray()
            if dm0 == None:
                dm0 = DM_1(self.reference_state, self.vector, "cvs-adc0")
                dm0.compute_block("VV")
            if t == None and self.order != "mp2-energy":
                try:
                    t = AR.T_Multipliers(self.reference_state,
                                         self.vector, self.order)
                    t.add_block(block_type="OOVV")
                    t.compute_block(dm1=dm0,block_type="OOVV")
                except (ValueError, TypeError):
                    errtxt = "2PDM, couldn't compute the "
                    errtxt += "amplitude response multipliers."
                    raise ValueError(self.order, errtxt)
                
        if block_type == "CVCV":
            if self.order in ["0", "adc0", "1", "adc1", "2", "adc2",
                              "cvs0", "cvs-adc0", "cvs2-doubles",
                              "cvs2-couplings"]: 
                pass
            elif self.order in ["cvs1", "cvs-adc1", "cvs2",
                                "cvs-adc2", "cvs2-debug", "cvs2-singles"]:
                self.matrix[block_type] = -np.einsum('Ib,Ja->IaJb',
                                                      v_s, v_s,
                                                      optimize=True)
            elif self.order in ["cvs2x", "cvs-adc2x", "cvs2x-debug"]:
                 self.matrix[block_type] = (-np.einsum('Ib,Ja->IaJb',
                                                      v_s, v_s,
                                                      optimize=True)
                                            -0.5*np.einsum('kIbc,kJac->IaJb',
                                                        v_d, v_d,
                                                        optimize=True)
                                            -0.5*np.einsum('kIcb,kJca->IaJb',
                                                        v_d, v_d,
                                                        optimize=True)
                                            +0.5*np.einsum('kIcb,kJac->IaJb',
                                                        v_d, v_d,
                                                        optimize=True)
                                            +0.5*np.einsum('kIbc,kJca->IaJb',
                                                        v_d, v_d,
                                                        optimize=True)
                                            )
            else:
                self.add_block(block_type)
                 
        elif block_type == "OCCV":
            if self.order in ["0", "adc0", "1", "adc1", "2",
                              "adc2", "cvs1", "cvs-adc1", "cvs2-singles",
                              "cvs2-doubles", "cvs0", "cvs-adc0"]: 
                pass
            elif self.order in ["cvs2", "cvs-adc2", "cvs2-debug", "cvs2x-debug",
                                "cvs2-couplings", "cvs2x", "cvs-adc2x"]:
                self.matrix[block_type] = 1.0/np.sqrt(2.0)*(
                                          np.einsum('Ib,kJba->kJIa',
                                                     v_s, v_d, optimize=True)
                                         -np.einsum('Ib,kJab->kJIa',
                                                     v_s, v_d, optimize=True)
                                                           )
            else:
                self.add_block(block_type)

        elif block_type == "OOVV":
            if self.order in ["0", "adc0", "1", "adc1", "cvs0",
                              "cvs-adc0", "cvs1", "cvs-adc1", "cvs2-doubles", 
                              "cvs2-couplings"]:
                pass
            elif self.order in ["gs_mp2"]:
                self.matrix[block_type] = -t2oovv 
            elif self.order in ["adc2"]:
                self.matrix[block_type] = ( #-0.5*t2oovv
                                            +0.5*np.einsum('ijcb,ca->ijab',
                                                          t2oovv,
                                                          dm0.matrix["VV"],
                                                          optimize=True)
                                            -0.5*np.einsum('ijca,cb->ijab',
                                                          t2oovv,
                                                          dm0.matrix["VV"],
                                                          optimize=True)
                                            +0.5*np.einsum('kiab,jk->ijab',
                                                          t2oovv,
                                                          dm0.matrix["OO"],
                                                          optimize=True)
                                            -0.5*np.einsum('kjab,ik->ijab',
                                                          t2oovv,
                                                          dm0.matrix["OO"],
                                                          optimize=True)
                                            +np.einsum('kjcb,kaic->ijab',
                                                        t2oovv,
                                                        DM1.matrix["OVOV"],
                                                        optimize=True)
                                            +np.einsum('kica,kbjc->ijab',
                                                        t2oovv,
                                                        DM1.matrix["OVOV"],
                                                        optimize=True)
                                            ) 
            elif self.order in ["cvs2", "cvs-adc2", "cvs2-singles",
                                "cvs2-debug", "cvs2-amplitude", "mp2", 
                                "mp2-energy", "cvs2x", "cvs-adc2x",
                                "cvs2x-debug"]:
                if self.order in ["cvs2-singles", "cvs2-debug", "cvs2x-debug"]:
                    self.matrix[block_type] = ( - 0.5*np.einsum('ijbc,ca->ijab',
                                                            t2oovv, 
                                                            dm0.matrix["VV"],
                                                            optimize=True)
                                                + 0.5*np.einsum('ijac,cb->ijab',
                                                            t2oovv,
                                                            dm0.matrix["VV"],
                                                            optimize=True)
                                                )
                elif self.order in ["mp2", "cvs2-amplitude"]:
                    self.matrix[block_type] = -t2oovv - 4.0*t.matrix["OOVV"]
                    # !see above!
                elif self.order in ["mp2-energy"]:
                    self.matrix[block_type] = -0.5*t2oovv
                else: # this is cvs2, cvs-adc2, cvs2x, and cvs-adc2x
                    self.matrix[block_type] = ( +0.5*np.einsum('ijcb,ca->ijab',
                                                             t2oovv,
                                                             dm0.matrix["VV"],
                                                             optimize=True)
                                                -0.5*np.einsum('ijca,cb->ijab',
                                                             t2oovv,
                                                             dm0.matrix["VV"],
                                                             optimize=True)
                                                -t2oovv
                                                -2.0*t.matrix["OOVV"]
                                                )   
            else:
                self.add_block(block_type)
                
        elif block_type == "OVVV":
            if self.order in ["0", "adc0", "1", "adc1", "cvs0",
                              "cvs-adc0", "cvs1", "cvs-adc1",
                              "cvs2-singles", "cvs2-doubles"]:
                pass
            elif self.order in ["cvs2", "cvs-adc2", "cvs2-debug", "cvs2x-debug",
                                "cvs2-couplings", "cvs2x", "cvs-adc2x"]:
                self.matrix[block_type] =  ( 2.0/np.sqrt(2.0)
                                             * np.einsum('Ja,iJcb->iabc',
                                                          v_s, v_d,
                                                          optimize=True)
                                            )
            elif self.order in ["adc2"]:
                self.matrix[block_type] = 2.0*np.sqrt(2.0)*(
                                                np.einsum('jibc,ja->iabc',
                                                           v_d, v_s,
                                                           optimize=True)
                                                        )
            else:
                self.add_block(block_type)

        elif block_type == "OOOV":
            if self.order in ["adc2"]:
                self.matrix[block_type] = 2.0*np.sqrt(2.0)*(
                                                np.einsum('ijba,kb->ijka',
                                                          v_d, v_s,
                                                          optimize=True)
                                                        )    
            else:
                self.add_block(block_type)

        elif block_type == "OVOV":
            if self.order in ["0", "adc0"]:
                pass
            elif self.order in ["cvs0", "cvs1", "cvs2", "cvs-adc0",
                                "cvs-adc1", "cvs-adc2", "cvs2-singles",
                                "cvs2-doubles", "cvs2-debug", "cvs2-couplings"]:
                pass
            elif self.order in ["adc1", "1", "2", "adc2"]:
                self.matrix[block_type] = -np.einsum('ib,ja->iajb',
                                                      v_s, v_s,
                                                      optimize=True)
            elif self.order in ["cvs2x", "cvs-adc2x", "cvs2x-debug"]:
                self.matrix[block_type] = 0.5*( -np.einsum("iKbc,jKac->iajb",
                                                       v_d, v_d, optimize=True)
                                            -np.einsum("iKcb,jKca->iajb",
                                                       v_d, v_d, optimize=True)
                                            +np.einsum("iKcb,jKac->iajb",
                                                       v_d, v_d, optimize=True)
                                            +np.einsum("iKbc,jKca->iajb",
                                                       v_d, v_d, optimize=True)
                                            )
            else:
                self.add_block(block_type)

        elif block_type == "CCVV":
            if self.order in ["0", "adc0", "1", "adc1", "2", "adc2",
                              "cvs0", "cvs1", "cvs-adc0", "cvs-adc1",
                              "cvs2-doubles", "cvs2-couplings"]:
                pass
            elif self.order in ["cvs2-debug", "cvs-adc2", "cvs2", "cvs2x", 
                                "cvs-adc2x","cvs2-singles", "mp2-energy",
                                "cvs2x-debug"]:
                if self.order in ["cvs2-debug", "cvs2-singles", "cvs2x-debug"]:
                    pass
                elif self.order in ["mp2-energy"]:
                    self.matrix[block_type] = -0.5*t2ccvv
                else: # cvs2, cvs-adc2, cvs2x, cvs-adc2x
                    self.matrix[block_type] = -t2ccvv
            else:
                self.add_block(block_type)

        elif block_type == "OCVV":
            if self.order in ["0", "adc0", "1", "adc1", "2", "adc2", 
                              "cvs0", "cvs1", "cvs-adc0", "cvs-adc1",
                              "cvs2-doubles", "cvs2-couplings"]:
                pass
            elif self.order in ["cvs2-debug", "cvs-adc2", "cvs2", "cvs2x",
                                "cvs-adc2x", "cvs2-singles", "mp2-energy",
                                "cvs2x-debug"]:
                if self.order in ["mp2-energy"]:
                    self.matrix[block_type] = -0.5*t2ocvv
                elif self.order in ["cvs-adc2", "cvs2", "cvs2x", "cvs-adc2x"]:
                    self.matrix[block_type] = -t2ocvv
                else:
                    pass # Have to check how the others look like

            else:
                self.add_block(block_type)
        elif block_type == "OOOO":
            for i in range(self.matrix[block_type].shape[0]):
                np.fill_diagonal(self.matrix[block_type][i,:,i,:], -2.0)
        elif block_type == "CCCC":
            for I in range(self.matrix[block_type].shape[0]):
                np.fill_diagonal(self.matrix[block_type][I,:,I,:], -2.0)
        elif block_type == "OCOC":
            if self.order in ["cvs-adc2x", "cvs2x", "cvs2x-debug"]:
                self.matrix[block_type] = np.einsum("iJab,kLab->iJkL",
                                                     v_d, v_d, optimize=True)
            elif self.order in ["hf"]:
                for J in range(self.matrix[block_type].shape[1]): 
                    np.fill_diagonal(self.matrix[block_type][:,J,:,J], -1.0)
            else:
                self.add_block(block_type)
        elif block_type == "VVVV":
            if self.order in ["cvs-adc2x", "cvs2x", "cvs2x-debug"]:
                self.matrix[block_type] = 2.0*np.einsum("iJcd,iJab->abcd",
                                                        v_d, v_d, optimize=True)
            else:
                # add a block of zeros
                self.add_block(block_type)
         
        else:
            errtxt = "is not a valid block of the two-particle density matrix."
            raise ValueError(block_type, errtxt)
            
