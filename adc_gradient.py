#               
#                               ADC_GRADIENT
#	  --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------


# TODO: remove commented out code
# TODO: remove unused code
# TODO: run trough yapf and flake8
# load required packages
import adcc
import DMs
import Amplitude_Response as AR
import Orbital_Response as OR
import import_integral_derivatives as ideriv
import ao_derivatives as aod
import numpy as np
import time


def get_type(c):
    return{
        "C" : "o2",
        "O" : "o1",
        "V" : "v1",
        }[c]

def translate_block(block):
    '''Translates my notation for the blocks
       to the notation used in adcc'''
    s=""
    for c in block:
        s+=get_type(c)
    return s
    
def get_blocks_DM1(order):
    return{
        "hf": [],
        "gs_mp2": ["OO", "VV"],
        "adc0" : ["OO", "VV"],
        "cvs-adc0" : ["CC", "VV"],
        "cvs0" : ["CC", "VV"],
        "adc1" : ["OO", "VV"],
        "cvs-adc1" : ["CC", "VV"],
        "cvs1" : ["CC", "VV"],
        "adc2" : ["OO", "VV"],
        "cvs-adc2" : ["OO", "CC", "VV"],
        "cvs2" : ["OO", "CC", "VV"],
        "cvs-adc2x": ["OO", "CC", "VV"],
        "cvs2x": ["OO", "CC", "VV"],
        }[order]

def get_blocks_DM2(order):
    return{
        "hf": [],
        "gs_mp2" : ["OOVV"],
        "adc0" : [],
        "cvs-adc0" : [],
        "cvs0" : [],
        "adc1" : ["OVOV"],
        "cvs-adc1" : ["CVCV"],
        "cvs1" : ["CVCV"],
        "adc2" : [], #not implemented
        "cvs-adc2" : ["CVCV", "CCVV", "OCVV", "OCCV", "OOVV", "OVVV"], 
        "cvs2" : ["CVCV", "CCVV", "OCVV", "OCCV", "OOVV", "OVVV"],
        "cvs-adc2x" : ["CVCV", "OCOC", "CCVV", "OCVV", "OCCV", 
                       "OOVV", "OVVV", "OVOV", "VVVV"],
        "cvs2x" : ["CVCV", "OCOC", "CCVV", "OCVV", "OCCV", 
                   "OOVV", "OVVV", "OVOV", "VVVV"], 
        }[order]

#this is not needed -- done automatically by OR
#TODO: remove
def get_blocks_amplitude_response(order):
    return{
            "cvs2" : ["OOVV"], #["CCVV", "OCVV", "OOVV"],
            "cvs-adc2" : ["OOVV"],
            "cvs2x" : ["OOVV"],
            "cvs-adc2x" : ["OOVV"],
            "adc2" : ["OOVV"],
            }[order]

def get_blocks_Lrsp(order):
# The order is important; e.g. OO and OV block are required for OV
# VV will be computed twice if used after OV;
    return{
        "hf" : [],
        "gs_mp2": ["OV"],
        "adc0" : ["OV"], 
        "cvs-adc0" : ["OV", "CV"],
        "cvs0" : ["OV", "CV"],
        "adc1" : ["OV"], 
        "cvs-adc1" : ["OC", "OV", "CV"],
        "cvs1" : ["OC", "OV", "CV"],
        "adc2" : [], #not implemented
        "cvs-adc2" : ["OC", "OV", "CV"],
        "cvs2" : ["OC", "OV", "CV"],
        "cvs-adc2x" : ["OC", "OV", "CV"],
        "cvs2x" : ["OC", "OV", "CV"],
        }[order]

def get_blocks_Orsp(order):
# The order is important; e.g. OO and OV block are required for OV
# VV will be computed twice if used after OV;
    return{
        "hf" : ["OO"],
        "gs_mp2": ["OO", "VV", "OV"],
        "adc0" : ["OO", "VV", "OV"], 
        "cvs-adc0" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs0" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "adc1" : ["OO", "VV", "OV"], 
        "cvs-adc1" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs1" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "adc2" : [], #not implemented
        "cvs-adc2" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs2" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs-adc2x" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs2x" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        }[order]


def get_h_blocks(order):
    return{
        "hf" : ["OO"],
        "gs_mp2": ["OO", "VV", "OV"],
        "adc0" : ["OO", "VV", "OV"], 
        "cvs-adc0" : ["OO", "CC", "VV", "CV", "OV"],
        "cvs0" : ["OO", "CC", "VV", "CV", "OV"],
        "adc1" : ["OO", "VV", "OV"], #only non-zero blocks are included
        "cvs-adc1" : ["OO", "CC", "VV", "OC", "OV", "CV"],
        "cvs1" : ["OO", "CC", "VV", "OC", "OV", "CV"],
        "adc2" : [], #not implemented
        "cvs-adc2" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs2" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs-adc2x" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs2x" : ["CC", "OO", "OV", "VV", "OC", "OV", "CV"]
        }[order]

def get_s_blocks(order):
    return{
        "hf" : ["OO"],
        "gs_mp2": ["OO", "VV", "OV"],
        "adc0" : ["OO", "VV", "OV"],
        "cvs-adc0" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs0" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "adc1" : ["OO", "VV", "OV"], 
        "cvs-adc1" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs1" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "adc2" : [], #not implemented
        "cvs-adc2" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs2" : ["CC", "OO", "OV", "VV", "OC", "OV", "CV"],
        "cvs-adc2x" : ["CC", "OO", "VV", "OC", "OV", "CV"],
        "cvs2x" : ["CC", "OO", "OV", "VV", "OC", "OV", "CV"],
        }[order]


def get_eri_blocks(order):
    return{
        "hf" : ["OOOO"],
        "gs_mp2": ["OOOO", "OOVV"],
        "adc0" : ["OOOO"], #
        "cvs-adc0" : ["CCCC", "OOOO", "OCOC"],
        "cvs0" : ["CCCC", "OOOO", "OCOC"],
        "adc1" : ["OVOV", "OOOO"], 
        # only non-zero blocks are included; 
        # for now, only DM2, to add parts corresponding to lambdas
        "cvs-adc1" : ["CVCV", "CCCC", "OOOO", "OCOC"],
        "cvs1" : ["CVCV", "CCCC", "OOOO", "OCOC"],
        "adc2" : [], #not implemented
        "cvs-adc2" : ["CCCC", "OOOO", "OCOC", "CVCV", "CCVV", "OCVV", 
                      "OCCV", "OOVV", "OVVV"],
        "cvs2" : ["CCCC", "OOOO", "OCOC", "CVCV", "CCVV", "OCVV", 
                  "OCCV", "OOVV", "OVVV"],
        "cvs-adc2x" : ["CCCC", "OOOO", "OCOC", "CVCV", "CCVV", "OCVV",
                       "OCCV", "OOVV", "OVVV", "OVOV", "VVVV"],
        "cvs2x" : ["CCCC", "OOOO", "OCOC", "CVCV", "CCVV", "OCVV",
                   "OCCV", "OOVV", "OVVV", "OVOV", "VVVV"]
        }[order]


class gradient():
    def __init__(self, refstate, vec=None, order='adc0', atoms=None, backend='',
                 molecule=None, scf=None, basis=None, mo_coeff=None,
                 grad_type='analytical'):
        """Initializes the object used to compute the CVS-ADC energy gradient
        
        Parameters:
        ----------
        refstate : reference state from an adc calculation
        vec      : excitation vector corresponding to the excited state
                   of interest
        order    : order of the adc matrix ("0"/"adc0","1"/"adc1","2"/"adc2"); 
                   "adc2-x" and "adc3" not available yet
        atoms    : array with the indices of the atoms to be included 
                   in the gradient calculation; 
        backend  : code which provides the integral derivatives: 
                   veloxchem, pyscf, psi4, random (for test purposes only)
        molecule : molecule object (specific to pyscf or veloxchem)
        scf      : scf reference state in the format of pyscf, veloxchem
        basis    : basis set object in the format of pyscf or veloxchem
        mo_coeff : numpy array of the mo_coefficients
        grad_type: analytical or numerical 
        """
        self.reference_state = refstate
        self.vector = vec
        self.order = order
        self.gradient_type = grad_type

        if backend == "pyscf" and atoms == None:
            self.atoms = np.arange(0, molecule.natm, 1)
        else:
            self.atoms = atoms
        self.backend = backend
        self.matrix = np.zeros((len(self.atoms),3)) 

        if molecule is not None:
            self.molecule = molecule
        if scf is not None:
            self.scf = scf
            if backend == 'pyscf':
                try:
                    self.mo_coeff = scf.mo_coeff
                except:
                    self.mo_coeff = mo_coeff
        if basis is not None:
            self.basis = basis
        if mo_coeff is not None:
            if backend == 'pyscf':
                pass
            else:
                self.mo_coeff = mo_coeff

    def reset(self, refstate=None, vector=None, molecule=None, scf=None):
        """
        Resets the reference state and excitation vector

        Parameters:
        ----------
        refstate: new scf reference state (from adcc)
        vec     : new excitation vector (from adcc)
        molecule: new molecule (from adc_ptimization_engine)
        """
        #Most importantly: reset gradient to zero!
        self.matrix = np.zeros((len(self.atoms),3))
        if refstate is not None:
            self.reference_state = refstate
        if vector is not None:
            self.vector = vector
        if molecule is not None:
            self.molecule = molecule
        if scf is not None:
            self.scf = scf
            if self.backend=='pyscf':
                self.mo_coeff = scf.mo_coeff

    def get_gradient(self):
        """
        Returns the gradient
        """
        return self.matrix

    def compute_numerical_gradient(self, step=1e-3, core=None, singlets=1,
                                   triplets=None, spin_flip=None,
                                   states=None, v_index=0,
                                   scf_tol=1e-12, adc_tol=1e-6):
        """Computes the gradient numerically using
            a finite differences algorithm

        Parameters:
        ----------
        step    : step size for finite differences (bohr)
        core    : core orbital space for cvs-adc
        singlets: number of singlet states to be computed
        triplets: number of triplet states to be computed
        states  : number of states to be computed
        v_index : index of the excited state for which the 
                  gradient is omputed.
        scf_tol : convergence threshold for scf
        adc_tol : convergence threshold for adc
        """
        if self.backend == "pyscf":
            try:
                import pyscf
            except ImportError:
                error_text = "Cannot import pyscf.\n"
                error_text += "Please install:\n"
                error_text += "pip install pyscf."
                raise ImportError(error_text)
        
            indices = []
            #get atom indices 
            for i in self.atoms:
                indices.append(4*(i+5))
                indices.append(4*(i+5)+1)
                indices.append(4*(i+5)+2)

            for cind in indices: 
                h = []
                E = []
                gs = []
                for dh in [-step, step]:
                    mol = self.molecule.copy() 
                    mol._env[cind] += dh
                    h.append(mol._env[cind])
                    if spin_flip is not None:
                        scfref = pyscf.scf.UHF(mol)
                    else:
                        scfref = pyscf.scf.RHF(mol)
                    scfref.conv_tol = scf_tol 
                    scfref.kernel()
                    # ...trick adcc into thinking the 
                    # conv tolerance is higher..
                    # TODO: remove
                    scfref.conv_tol = scf_tol * 1e-10 
                    if self.order == "hf":
                        pass
                    elif self.order == "gs_mp2":
                        adc = adcc.run_adc(scfref, method="adc0",
                                           core_orbitals=core, 
                                           n_states=states,
                                           n_spin_flip=spin_flip,
                                           n_singlets=singlets, 
                                           n_triplets=triplets,
                                           conv_tol=adc_tol)
                    else:
                        adc = adcc.run_adc(scfref, method=self.order,
                                           core_orbitals=core, 
                                           n_states=states,
                                           n_spin_flip=spin_flip,
                                           n_singlets=singlets,
                                           n_triplets=triplets,
                                           conv_tol=adc_tol)
                    if self.order in ["hf", "gs_mp2"]:
                        E.append(0.0)
                    else:
                        E.append(adc.excitation_energy[v_index])
                    if self.order in ["hf", "cvs0", "cvs1", "adc0", "adc1",
                                      "cvs-adc0", "cvs-adc1"]:
                        gs_energy = scfref.energy_tot()
                    else:
                        adc_mp2 = adcc.LazyMp(adc.reference_state)
                        gs_energy = adc_mp2.energy(2)
                    gs.append(gs_energy)
                dE = (E[1]+gs[1]-E[0]-gs[0])/(h[1]-h[0])
                i = int((cind-20)/4)
                j = cind-4*(i+5)
                self.matrix[i,j] = dE
        else:
            raise NotImplementedError("Only pyscf is implemented for now.")

    def compute(self, maxiter=50, hf=None, seed=None, verbose=False):
        """Computes the CVS-ADC energy gradient
        
        Parameters:
        ----------
        maxiter: maximum number of iterations used in computing 
                 the Orbital response Lagrange multipliers
        hf     : matrix of len(self.atoms) x 3; 
                 nuclear gradient; Should be renamed
        seed   : seed for the random number generator used to import random
                 integral derivatives (for testing purpose only)
        """

        #0. Prepare afew variables + HF DMs:
        natoms = len(self.atoms)
        dm1_hf = DMs.DM_1(self.reference_state,
                          vec=None, order="hf")
        DM2_hf = DMs.DM_2(self.reference_state,
                          vec=None, order="hf")
        if "cvs" in self.order:
            hf_blocks_dm1 = ["CC", "OO"]
            hf_blocks_DM2 = ["CCCC", "OOOO", "OCOC"]
        else:
            hf_blocks_dm1 = ["OO"]
            hf_blocks_DM2 = ["OOOO"]

        dm1_hf.compute_blocks(hf_blocks_dm1, verbose=verbose)
        DM2_hf.compute_blocks(hf_blocks_DM2, verbose=verbose)

        #1. Compute ADC(0) 1PDM -- required for 2PDM
        if "cvs" in self.order:
            d0 = DMs.DM_1(self.reference_state, self.vector, "cvs-adc0")
            d0.compute_block("VV")
        elif "mp" in self.order or "hf" in self.order:
            d0 = None
        else:
            d0 = DMs.DM_1(self.reference_state, self.vector, "adc0")
            d0.compute_block("VV")

        # Compute amplitude response for adc2 and higher
        if self.order in ["cvs2", "adc2", "cvs-adc2"]:
            blocks_ar = get_blocks_amplitude_response(self.order)
            tz = AR.T_Multipliers(self.reference_state, self.vector, self.order)
            for block in blocks_ar:
                tz.add_block(block)
                tz.compute_block(dm1=d0, block_type=block)
        else:
            tz = None

        #2. Compute DMs
        #   2.1 1DM:
        blocks_dm1 = get_blocks_DM1(self.order)
        d1 = DMs.DM_1(self.reference_state, self.vector, self.order)
        d1.compute_blocks(blocks=blocks_dm1, t=tz, verbose=verbose)
        ###for block in blocks_dm1:
        ###    d1.add_block(block)
        ###    d1.compute_block(block_type=block, t=tz)

        #   2.2 2DM:
        blocks_dm2 = get_blocks_DM2(self.order)
        D2 = DMs.DM_2(self.reference_state, self.vector, self.order)
        if self.order not in ["adc0", "cvs-adc0", "cvs0"]:
            D2.compute_blocks(blocks=blocks_dm2, t=tz, dm0=d0, verbose=verbose)
        #for block in blocks_dm2:
        #    D2.add_block(block)
        #    D2.compute_block(block_type=block, t=tz, dm0=d0)
 
        #3. Compute Orbital response
        #   3.1 Lambda
        blocks_or = get_blocks_Orsp(self.order)
        blocks_lr = get_blocks_Lrsp(self.order) 

        L = OR.L_Multipliers(self.reference_state, order=self.order,
                             vec=self.vector)
        if "cvs" in self.order:
            # in ["cvs2x", "cvs2", "cvs-adc2x", "cvs-adc2"]:
            # OV and CV are computed together, so we remove the CV
            # block from the compute list
            L.compute_blocks(blocks=blocks_lr[:-1], dm1=d1,
                             DM2=D2, max_iterations=maxiter,
                             verbose=verbose)
        else:
            L.compute_blocks(blocks=blocks_lr, dm1=d1,
                             DM2=D2, max_iterations=maxiter,
                             verbose=verbose)
        #   3.2 Omega
        O = OR.O_Multipliers(self.reference_state, order=self.order)
        if self.order == "hf":
            O.add_block("OO")
            O.matrix["OO"] = -self.reference_state.fock("o1o1").to_ndarray()
        else:
            O.compute_blocks(blocks=blocks_or, L=L, dm1=d1, DM2=D2,
                             verbose=verbose)

        #4. Import integral derivatives and compute gradient
        #   4.1 decide what blocks are necessary for the particular ADC order
        h_blocks = get_h_blocks(self.order)
        s_blocks = get_s_blocks(self.order)
        eri_blocks = get_eri_blocks(self.order)
    
        ## Import all blocks from daltonproject, orpyscf
        if self.backend in ["dp", "pyscf"]:
            ## import from daltonproject
            if self.backend == "dp":
                hcore = ideriv.core_hamiltonian(self.reference_state,
                                            scf=self.scf,
                                            backend=self.backend,
                                            basis=self.basis,
                                            molecule=self.molecule,
                                            natoms=self.molecule.num_atoms,
                                            mo_coeff=self.mo_coeff)
                ovlp = ideriv.overlap(self.reference_state, scf=self.scf,
                                            backend=self.backend,
                                            basis=self.basis,
                                            molecule=self.molecule,
                                            natoms=self.molecule.num_atoms,
                                            mo_coeff=self.mo_coeff)
                eri = ideriv.eri(self.reference_state, scf=self.scf,
                                            backend=self.backend,
                                            basis=self.basis,
                                            molecule=self.molecule,
                                            natoms=self.molecule.num_atoms,
                                            mo_coeff=self.mo_coeff)
                eri_sum_i = ideriv.eri_sum_i(self.reference_state, scf=self.scf,
                                            backend=self.backend,
                                            basis=self.basis,
                                            molecule=self.molecule,
                                            natoms=self.molecule.num_atoms,
                                            mo_coeff=self.mo_coeff)
            ## importfrom pyscf
            elif self.backend == "pyscf":
                hcore = ideriv.core_hamiltonian(self.reference_state,
                                            scf=self.scf,
                                            backend=self.backend, 
                                            molecule=self.molecule,
                                            natoms=self.molecule.natm,
                                            mo_coeff=self.mo_coeff)
                ovlp = ideriv.overlap(self.reference_state, scf=self.scf,
                                            backend=self.backend, 
                                            molecule=self.molecule,
                                            natoms=self.molecule.natm,
                                            mo_coeff=self.mo_coeff)
                eri = ideriv.eri(self.reference_state, scf=self.scf,
                                            backend=self.backend, 
                                            molecule=self.molecule,
                                            natoms=self.molecule.natm,
                                            mo_coeff=self.mo_coeff)
                eri_sum_i = ideriv.eri_sum_i(self.reference_state,
                                            scf=self.scf,
                                            backend=self.backend, 
                                            molecule=self.molecule,
                                            natoms=self.molecule.natm,
                                            mo_coeff=self.mo_coeff)

            hcore.import_blocks(h_blocks)
            ovlp.import_blocks(s_blocks)
            eri.import_blocks(eri_blocks)
            eri_sum_i.import_blocks(blocks_or)

            for block in blocks_dm1:
                self.matrix += np.einsum('pq,xpq->x', d1.matrix[block],
                                         hcore.matrix[block],
                                         optimize=True).reshape(natoms,3)
                self.matrix += np.einsum('pq,xpq->x', d1.matrix[block],
                                         eri_sum_i.matrix[block],
                                         optimize=True).reshape(natoms, 3)


            for block in blocks_lr:
                # if block in ["OC", "OV", "CV"]:
                # factor 2 to account for OV+VO, OC+CO, CV+VC
                self.matrix += 2.0*np.einsum('pq,xpq->x', L.matrix[block],
                                              hcore.matrix[block],
                                              optimize=True).reshape(natoms,3)
                self.matrix += 2.0*np.einsum('pq,xpq->x', L.matrix[block],
                                              eri_sum_i.matrix[block],
                                              optimize=True).reshape(natoms,3)

            for block in blocks_or:
                if block in ["OC", "OV", "CV"]:
                    # factor 2 to account for OV+VO, OC+CO, CV+VC
                    self.matrix += 2.0*np.einsum('pq,xpq->x', O.matrix[block],
                                                  ovlp.matrix[block],
                                                optimize=True).reshape(natoms,3)
                else:
                    self.matrix += np.einsum('pq,xpq->x', O.matrix[block],
                                              ovlp.matrix[block],
                                              optimize=True).reshape(natoms,3)

                 
            for block in blocks_dm2:
                # factors account for how many blocks of 
                # each type should be non-zero in the 2PDMs
                if block in ["CCCC", "OOOO", "VVVV"]:
                    self.matrix += 0.25*np.einsum('pqrs,xpqrs->x',
                                                   D2.matrix[block],
                                                   eri.matrix[block],
                                                optimize=True).reshape(natoms,3)
                elif block in ["CCVV", "OOVV"]:
                    self.matrix += 0.50*np.einsum('pqrs,xpqrs->x',
                                                   D2.matrix[block],
                                                   eri.matrix[block],
                                                optimize=True).reshape(natoms,3)

                elif block in ["CVCV", "OCVV", "OVVV", "OVOV", "OCOC"]:
                    self.matrix += np.einsum('pqrs,xpqrs->x',
                                              D2.matrix[block],
                                              eri.matrix[block],
                                              optimize=True).reshape(natoms,3)

                elif block in ["OCCV"]:
                    self.matrix += 2.0*np.einsum('pqrs,xpqrs->x',
                                                  D2.matrix[block],
                                                  eri.matrix[block],
                                                optimize=True).reshape(natoms,3)
                else:
                    print("WARNING: You forgot ", block)

            # HF ground state energy contributions:
            for block in hf_blocks_dm1:
                self.matrix += ( np.einsum('pq,xpq->x', dm1_hf.matrix[block],
                                            hcore.matrix[block],
                                            optimize=True).reshape(natoms,3)
                                +np.einsum('pq,xpq->x', dm1_hf.matrix[block],
                                            eri_sum_i.matrix[block],
                                            optimize=True).reshape(natoms,3)
                                )
            for block in hf_blocks_DM2:
                if block in ["OOOO", "CCCC"]:
                    self.matrix +=0.25*np.einsum('pqrs,xpqrs->x',
                                                  DM2_hf.matrix[block],
                                                  eri.matrix[block],
                                                optimize=True).reshape(natoms,3)
                elif block in ["OCOC"]:
                    self.matrix += np.einsum('pqrs,xpqrs->x',
                                              DM2_hf.matrix[block],
                                              eri.matrix[block],
                                              optimize=True).reshape(natoms,3)

        else:
            # THIS imports one atom at a time... 
            # probably change to all atoms atoms at once?
            for a in self.atoms:
                # 4.2 import integral derivatives; 
                # Now a new object is created for each atom; 
                # is there a smarter way to do this? 
                # reset_block?? or delete_block to use the same object??
                # 4.2.1 Core Hamiltonian
                hcore = ideriv.core_hamiltonian(refstate = self.reference_state, 
                                                backend=self.backend, atom=a)
                for block in h_blocks:
                    hcore.import_block(block, seed)
                

                # 4.2.2 Overlap Matrix
                ovlp = ideriv.overlap(refstate = self.reference_state, 
                                      backend=self.backend, atom=a)
                for block in s_blocks:
                    ovlp.import_block(block, seed)

                # 4.2.3 ERI
                eri = ideriv.eri(refstate = self.reference_state, 
                                 backend=self.backend, atom=a)
                for block in eri_blocks:
                    eri.import_block(block, seed)

                # 4.2.4 ERI, sum over the occupied orbitals;
                # it has the same block structure as 
                # the orbital rsp. multipliers
                eri_sum_i = ideriv.eri_sum_i(refstate = self.reference_state, 
                                             backend=self.backend, atom=a)
                for block in blocks_or:
                    eri_sum_i.import_block(block, seed)

                #   4.3 compute gradient
                #       4.3.1 Orbital response - omega
                for block in O.blocks:
                    # calculate \sum_pq \omega_pq \S_pq^\x;
                    # Note: some blocks should be summed twice
                    # Better inlcude directly in the multipliers?
                    if block in ["OV", "CV"]:
                        self.matrix[a] = ( self.matrix[a] 
                                        + 2.0*np.einsum('xpq,pq->x', 
                                              ovlp.matrix[block], 
                                              O.matrix[block],
                                              optimize=True)
                                         )
                    else:
                        self.matrix[a] = ( self.matrix[a] 
                                         + np.einsum('xpq,pq->x', 
                                           ovlp.matrix[block], 
                                           O.matrix[block],
                                           optimize=True)
                                          )

                # 4.3.2 Orbital response -lambda
                for block in L.blocks:
                    # calculate \sum_pq \lambda_pq \h_pq^\x + 
                    # \lambda_pq \sum_i <pi||qi>^x 
                    if block in ["CV", "OV"]:
                        self.matrix[a] = ( self.matrix[a] 
                                       + 2.0*np.einsum('xpq,pq->x', 
                                         hcore.matrix[block], 
                                         L.matrix[block],
                                         optimize=True)
                                         )
                        self.matrix[a] = ( self.matrix[a] 
                                         + 2.0*np.einsum('xpq,pq->x', 
                                        eri_sum_i.matrix[block], 
                                        L.matrix[block],
                                        optimize=True)
                                            )
                    else:
                        self.matrix[a] = ( self.matrix[a] 
                                         + np.einsum('xpq,pq->x', 
                                           hcore.matrix[block], 
                                           L.matrix[block],
                                           optimize=True)
                                            )
                        self.matrix[a] = (self.matrix[a] 
                                        + np.einsum('xpq,pq->x', 
                                          eri_sum_i.matrix[block], 
                                          L.matrix[block],
                                          optimize=True)
                                            ) 

                # 4.3.4 1PDM:
                for block in d1.blocks:
                    # calculate \sum_pq \gamma_pq \h_pq^\x
                    self.matrix[a] = ( self.matrix[a] 
                                     + np.einsum('xpq,pq->x', 
                                       hcore.matrix[block], 
                                       d1.matrix[block],
                                       optimize=True)
                                        )

                # 4.3.5 2PDM:
                for block in D2.blocks:
                    # calculate \sum_pqrs \Gamma_pqrs \eri_pqrs^\x
                    self.matrix[a] = ( self.matrix[a] 
                                      + 0.25*np.einsum('xpqrs,pqrs->x',
                                             eri.matrix[block], 
                                             D2.matrix[block],
                                             optimize=True)
                                        )

        # 5. Import the reference state gradient 
        #      and add it to get the final result:
        # TODO: rename; hf is the contribution from
        # nuclear-nuclear repulsion; the varible name is confusing
        if hf is None:
            HF_gradient = ideriv.HF_gradient(backend=self.backend,
                                             atoms=self.atoms,
                                             scf=self.scf)
            HF_gradient.import_HF_gradient(seed)
            de_HF = HF_gradient.matrix
        else:
            de_HF = hf

        self.matrix = self.matrix + de_HF 

    def compute_ao(self, maxiter=50, hf=None, seed=None, verbose=False,
                   print_multipliers=False):
        """Computes the CVS-ADC energy gradient
        
        Parameters:
        ----------
        maxiter: maximum number of iterations used in computing 
                 the Orbital response Lagrange multipliers
        hf     : matrix of len(self.atoms) x 3; 
                 nuclear gradient; Should be renamed
        seed   : seed for the random number generator used to import random
                 integral derivatives (for testing purpose only)
        print_multipliers: prints Lagrange multipliers
        verbose: print additional data
        """

        #0. Prepare afew variables + HF DMs:
        natoms = len(self.atoms)
        dm1_hf = DMs.DM_1(self.reference_state,
                          vec=None, order="hf")
        DM2_hf = DMs.DM_2(self.reference_state,
                          vec=None, order="hf")
        if "cvs" in self.order:
            hf_blocks_dm1 = ["CC", "OO"]
            hf_blocks_DM2 = ["CCCC", "OOOO", "OCOC"]
        else:
            hf_blocks_dm1 = ["OO"]
            hf_blocks_DM2 = ["OOOO"]

        if verbose:
            print("DMs, HF reference state:")
        dm1_hf.compute_blocks(hf_blocks_dm1, verbose=verbose)
        DM2_hf.compute_blocks(hf_blocks_DM2, verbose=verbose)

        #1. Compute ADC(0) 1PDM -- required for 2PDM
        if "cvs" in self.order:
            d0 = DMs.DM_1(self.reference_state, self.vector, "cvs-adc0")
            d0.compute_block("VV")
        elif "mp" in self.order or "hf" in self.order:
            d0 = None
        else:
            d0 = DMs.DM_1(self.reference_state, self.vector, "adc0")
            d0.compute_block("VV")

        # Compute amplitude response for adc2 and higher
        if self.order in ["cvs2", "adc2", "cvs-adc2"]:
            blocks_ar = get_blocks_amplitude_response(self.order)
            tz = AR.T_Multipliers(self.reference_state, self.vector, self.order)
            for block in blocks_ar:
                tz.add_block(block)
                tz.compute_block(dm1=d0, block_type=block)
        else:
            tz = None

        #2. Compute DMs
        #   2.1 1DM:
        blocks_dm1 = get_blocks_DM1(self.order)
        d1 = DMs.DM_1(self.reference_state, self.vector, self.order)
        d1.compute_blocks(blocks=blocks_dm1, t=tz, verbose=verbose)

        #   2.2 2DM:
        blocks_dm2 = get_blocks_DM2(self.order)
        D2 = DMs.DM_2(self.reference_state, self.vector, self.order)
        if self.order not in ["adc0", "cvs-adc0", "cvs0"]:
            D2.compute_blocks(blocks=blocks_dm2, t=tz, dm0=d0, verbose=verbose)
 
        #3. Compute Orbital response
        #   3.1 Lambda
        blocks_or = get_blocks_Orsp(self.order)
        blocks_lr = get_blocks_Lrsp(self.order) 

        L = OR.L_Multipliers(self.reference_state, order=self.order,
                             vec=self.vector)
        if "cvs" in self.order:
            # in ["cvs2x", "cvs2", "cvs-adc2x", "cvs-adc2"]:
            # OV and CV are computed together, so we remove the CV
            # block from the compute list
            L.compute_blocks(blocks=blocks_lr[:-1], dm1=d1,
                             DM2=D2, max_iterations=maxiter, verbose=verbose)
        else:
            L.compute_blocks(blocks=blocks_lr, dm1=d1,
                             DM2=D2, max_iterations=maxiter, verbose=verbose)
        #   3.2 Omega
        if print_multipliers:
            print("\n Here are the Lambda Lagrange Multipliers:\n")
            for block in L.blocks:
                print(block+":\n")
                print(L.matrix[block])
                print()

        O = OR.O_Multipliers(self.reference_state, order=self.order)
        if self.order == "hf":
            O.add_block("OO")
            O.matrix["OO"] = -self.reference_state.fock("o1o1").to_ndarray()
        else:
            O.compute_blocks(blocks=blocks_or, L=L, dm1=d1, DM2=D2,
                             verbose=verbose)

        if print_multipliers:
            print("\n Here are the Omega Lagrange Multipliers:\n")
            for block in O.blocks:
                print(block+":\n")
                print(O.matrix[block])
                print()
        if self.backend == "pyscf":
            # HCORE
            self.matrix += aod.compute_pyscf_hcore([L, dm1_hf, d1],
                                                   self.molecule,
                                                   self.scf, self.mo_coeff,
                                                   verbose=verbose)

            # OVERLAP
            self.matrix += aod.compute_pyscf_ovlp(O, self.molecule,
                                                  self.mo_coeff,
                                                  verbose=verbose)

            # ERIs
            self.matrix += aod.compute_pyscf_eri([D2,DM2_hf],
                                                 [L, dm1_hf, d1],
                                                 self.molecule,
                                                 self.mo_coeff,
                                                 verbose=verbose)

        else:
            err_txt = "Only Pyscf is currently implemented."
            raise NotImplementedError(err_txt)

        # add the nuclear-nuclear contribution
        # the name HF_gradient is misleading;
        # this is only the nuclear contribution
        # TODO: change name
        if hf is None:
            HF_gradient = ideriv.HF_gradient(backend=self.backend,
                                             atoms=self.atoms,
                                             scf=self.scf)
            HF_gradient.import_HF_gradient(seed)
            de_HF = HF_gradient.matrix
        else:
            de_HF = hf

        self.matrix = self.matrix + de_HF
