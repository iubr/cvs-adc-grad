#               
#                               ADC_GRADIENT
#	  --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------

import adcc #maybe not needed??
import numpy as np

np.set_printoptions(precision=7, suppress=True)

no = []
try:
    import pyscf
    from pyscf import grad
except ImportError:
    no.append('pyscf')

try:
    import veloxchem as vlx
except ImportError:
    no.append('vlx')

try:
    import psi4
except ImportError:
    no.append('psi4')

try:
    import daltonproject as dp
except ImportError:
    no.append('dp')

if len(no)==4:
    errtxt="At least one of the following "
    errtxt += "electronic structure codes is required: \n"
    for code in no:
        errtxt = errtxt + code + ' '
    raise ImportError(errtxt)



def get_type(block,pos=0):
    return {
        "O" : "o1o1",
        "C" : "o2o2",
        "V" : "v1v1",
        }[block[pos]]

def get_shape(block, ncore, nocc, nvirt, natoms):
    return{
            "CC" : (3*natoms, ncore, ncore),
            "OC" : (3*natoms, nocc, ncore),
            "CV" : (3*natoms, ncore, nvirt),
            "OO" : (3*natoms, nocc, nocc),
            "OV" : (3*natoms, nocc, nvirt),
            "VV" : (3*natoms, nvirt, nvirt),
            }[block]

def get_shape_eri(block, ncore, nocc, nvirt, natoms):
    return{
            "CVCV" : (3*natoms, ncore, nvirt, ncore, nvirt),
            "CCVV" : (3*natoms, ncore, ncore, nvirt, nvirt),
            "OCVV" : (3*natoms, nocc, ncore, nvirt, nvirt),
            "OCCV" : (3*natoms, nocc, ncore, ncore, nvirt),
            "OOVV" : (3*natoms, nocc, nocc, nvirt, nvirt),
            "OVVV" : (3*natoms, nocc, nvirt, nvirt, nvirt),
            "OVOV" : (3*natoms, nocc, nvirt, nocc, nvirt),
            "OOOO" : (3*natoms, nocc, nocc, nocc, nocc),
            "CCCC" : (3*natoms, ncore, ncore, ncore, ncore),
            "OCOC" : (3*natoms, nocc, ncore, nocc, ncore),
            "VVVV" : (3*natoms, nvirt, nvirt, nvirt, nvirt),
            }[block]

def get_starting_index(block, ncore, nocc, nvirt, pos=0):
    return{
            "C" : 0,
            "O" : int(0.5*ncore),
            "V" : int(0.5*ncore+0.5*nocc),
            }[block[pos]]

    

class int_deriv():
    def __init__(self, refstate, scf=None, backend='random', atom=0,
                 natoms=1, molecule=None, basis=None, mo_coeff=None): 
        #replace atom with a molecule?? / xyz file??
        """Initializes an object used to import integral derivatives from one
        of the supported electronic structure codes 
        
        Parameters:
        ----------
        refstate: reference state from an adc calcualation (may not be necessary)
        backend: electronic structure code to import integral derivatives from; 
                 "random" is for testing only
        atom: atom index -- required for pyscf? PROBABLY WILL BE REMOVED
        natoms: number of atoms in the molecule
        blocks: list of blocks to be included; 
                depending on the type it can be a list of 2-dimensional 
                or 4-dimensional blocks;
        molecule: dp molecule objects; required only for daltonproject import 
        basis: dp basis object; required only for daltonproject import
        method: dp method object; required only for daltonproject import
        """
        self.reference_state = refstate
        if scf is not None: 
            self.scf = scf
        if molecule is not None:
            self.molecule = molecule
        if basis is not None:
            self.basis = basis
        if mo_coeff is not None:
            self.mo_coeff = mo_coeff
        self.backend = backend
        self.atom = atom
        self.natoms = natoms
        self.blocks = []
        self.matrix = {}

    def import_blocks(self, blocks, seed=None):
        """Adds/Imports all blocks of the Core Hamiltonian
        (in MO basis)

        Parameters:
        ----------
        blocks: list of blocks to be added; possible values are
        "OO" "CC" "VV" "OC" "OV" and "CV"
        atom: index of atom wrt which the derivatives are performed
        """

        if self.backend == "random":
            for block in blocks:
                self.add_block_random(block, seed)
        elif self.backend == "veloxchem":
            for block in blocks:
                self.add_block_veloxchem(block)
        elif self.backend == "pyscf":
            if self.reference_state.restricted:
                self.add_all_pyscf(blocks)
            else:
                self.add_all_pyscf_unrestricted(blocks)
        elif self.backend == "psi4":
            for block in blocks:
                self.add_block_psi4(block)
        elif self.backend == "dp":
            self.add_all_dp(blocks)
        else:
            raise ValueError(self.backend, "Not implemented.")

    def get_mo_space(self, block, ncore, nocc, nvirt, pos=0):
        return{
                "C" : self.reference_state.mospaces.core_orbitals[0:ncore],
                "O" : self.reference_state.mospaces.occupied_orbitals[0:nocc],
                "V" : self.reference_state.mospaces.virtual_orbitals[0:nvirt],
                }[block[pos]]

class core_hamiltonian(int_deriv):
    def import_block(self, block, seed=None):
        """Adds/Imports a specific block of the Core Hamiltonian
        (in MO basis)

        Parameters:
        ----------
        block: type of block to be added; possible values are 
        "OO" "CC" "VV" "OC" "OV" and "CV"
        atom: index of atom wrt which the derivatives are performed
        """
        if self.backend == "random":
            self.add_block_random(block, seed)
        elif self.backend == "veloxchem":
            self.add_block_veloxchem(block)
        elif self.backend == "pyscf":
            self.add_block_pyscf(block)
        elif self.backend == "psi4":
            self.add_block_psi4(block)
        elif self.backend == "dp":
            self.add_all_dp()
        else:
            raise ValueError(self.backend, "Not implemented.")

    def delete_block(self, block):
        pass
    
    def add_all_dp(self, blocks):
        h1ao = dp.lsdalton.nuclear_electron_attraction_matrix(self.molecule, 
                                           self.basis, self.scf, geo_order=1)
        kao = dp.lsdalton.kinetic_energy_matrix(self.molecule, self.basis,
                                           self.scf, geo_order=1)
        #Uncomment lines below to use the mo_coeff as the identity matrix; 
        #Still need to actually get the correct ones from somewhere
        #dalton?, Zilvinas and Olav's import routine?
        #self.mo_coeff = np.zeros((h1ao.shape[1],h1ao.shape[2])) 
        #np.fill_diagonal(self.mo_coeff, 1.0)

        #get the derivative of the core hamiltonian in MO basis:
        hcore = np.zeros(h1ao.shape)        
        hcore[:] = np.matmul(
                   np.matmul(self.mo_coeff.transpose(),h1ao[:]+kao[:]),
                   self.mo_coeff)

        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        try:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        except:
            ncore = 0

        self.blocks = blocks
        for block in blocks:
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            i1 = get_starting_index(block, ncore, nocc, nvirt, pos=0)
            i2 = get_starting_index(block, ncore, nocc, nvirt, pos=1)
            self.matrix[block][:,0:n1,0:n2] = hcore[:,i1:i1+n1,i2:i2+n2]
            self.matrix[block][:,n1:,n2:] = hcore[:,i1:i1+n1,i2:i2+n2]

    def add_all_pyscf(self, blocks):
        #get the derivative of hcore in dalton format
        gradient = grad.RHF(self.scf)
        gen_hcore = gradient.hcore_generator(self.molecule)
        hao = np.zeros((3*self.molecule.natm, self.molecule.nao, self.molecule.nao))

        #d_sao_intm = -self.molecule.intor('int1e_ipovlp', aosym='s1')
        #d_sao=np.zeros((3*self.molecule.natm, self.molecule.nao, self.molecule.nao))
        ao_slices = self.molecule.aoslice_by_atom()
        #required for the derivatives of the MO coefficients 

        for i in range(self.molecule.natm):
            hao[3*i:3*i+3] = gen_hcore(i)
            k0, k1 = ao_slices[i, 2:]
            #i0 = i*3
            #i1 = i0+3
            #d_sao[i0:i1,k0:k1] = d_sao_intm[:,k0:k1]
        #d_sao += d_sao.transpose(0,2,1) # needed because pyscf generates
        # only ( nabla m | n ), but we need ( nabla m | n ) + ( m | nabla n)   

        # get the component related to the derivatives
        # of the MO coefficients
        #sao = self.scf.get_ovlp() #overlap matrix in ao basis
        hcore_ao = self.scf.get_hcore() #core hamiltonian in ao basis
        #sao_inv = np.linalg.inv(sao)
        #d_mo_coeff = (-0.5*np.einsum('mn,xnp->xmp',sao_inv,
        #                np.einsum('xns,sp->xnp',d_sao, self.scf.mo_coeff)
        #                            )
        #             ) # see Szabo and Ostlund, p. 441
        #hcore_ao = np.zeros((2*self.molecule.nao, 2*self.molecule.nao))
        #c_mo = np.zeros((2*self.molecule.nao, 2*self.molecule.nao))
        #d_c_mo = np.zeros((3*self.molecule.natm,2*self.molecule.nao, 2*self.molecule.nao))

        ###n_alpha = self.scf.mo_coeff.shape[1]
        ###hcore_ao[:n_alpha,:n_alpha] = orig_hcore_ao
        ###hcore_ao[n_alpha:,n_alpha:] = orig_hcore_ao
        ###c_mo[:n_alpha,:n_alpha] = self.scf.mo_coeff
        ###c_mo[n_alpha:,n_alpha:] = self.scf.mo_coeff
        ###d_c_mo[:,:n_alpha,:n_alpha] = d_mo_coeff
        ###d_c_mo[:,n_alpha:,n_alpha:] = d_mo_coeff
        
        #There must be a better way to do all of the above..

        #get the derivative of the core hamiltonian in MO basis:
        hcore = np.zeros(hao.shape)        
        hcore[:] = ( np.matmul(
                   np.matmul(self.mo_coeff.transpose(),hao[:]),
                   self.mo_coeff) )
        ####hcore += (2.0*np.einsum('pm,xmq->xpq',self.scf.mo_coeff.T,
        ####             np.einsum('mn,xnq->xmq',hcore_ao, d_mo_coeff)) 
        ####            )

        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
            #core_indices=self.reference_state.mospaces.core_orbitals[0:int(ncore/2)]
            ####Re-arrange hcore[:] so that core starts at 0
            ###nc = int(0.5*ncore)
            ###no = int(0.5*nocc)
            ###c = self.reference_state.mospaces.core_orbitals[0:nc].copy()
            ###o = self.reference_state.mospaces.occupied_orbitals[0:no].copy()
            ###for i in range(nc):
            ###    for j in range(no):
            ###        if c[i] > o[j]:
            ###            hcore[:,[o[j],c[i]],:] = hcore[:,[c[i],o[j]],:]
            ###            hcore[:,:,[o[j],c[i]]] = hcore[:,:,[o[j],c[i]]]
            ###            swap = c[i]
            ###            c[i] = o[j]
            ###            o[j] = swap
            ###        else:
            ###            break
            ###for i in range(no-1):
            ###    for j in range(i+1,no):
            ###        if o[i] > o[j]:
            ###            hcore[:,[o[j],o[i]],:] = hcore[:,[o[i],o[j]],:]
            ###            hcore[:,:,[o[j],o[i]]] = hcore[:,:,[o[j],o[i]]]
            ###            swap = o[i]
            ###            o[i] = o[j]
            ###            o[j] = swap
            ###        else:
            ###            break
        else:
            ncore = 0

        self.blocks = blocks
        for block in blocks:
            #print(block)
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            #print(shape)
            #print(hcore.shape)
            self.matrix[block] = np.zeros(shape)
            space1 = self.get_mo_space(block, int(ncore/2), int(nocc/2),
                                       int(nvirt/2), pos=0)
            space2 = self.get_mo_space(block, int(ncore/2), int(nocc/2),
                                       int(nvirt/2), pos=1)
            #print(space1)
            #print(space2)
            i = 0
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            for p in space1:
                j = 0
                for q in space2:
                    #print(i,j,p,q)
                    self.matrix[block][:,i,j] = hcore[:,p,q]
                    self.matrix[block][:,i+n1,j+n2] = hcore[:,p,q]
                    j += 1
                i += 1
            #n1 = int(0.5*shape[1])
            #n2 = int(0.5*shape[2])
            #i1 = get_starting_index(block, ncore, nocc, nvirt, pos=0)
            #i2 = get_starting_index(block, ncore, nocc, nvirt, pos=1)
            #self.matrix[block][:,0:n1,0:n2] = hcore[:,i1:i1+n1,i2:i2+n2]
            #self.matrix[block][:,n1:,n2:] = hcore[:,i1:i1+n1,i2:i2+n2]

    def add_all_pyscf_unrestricted(self, blocks):
        #get the derivative of hcore in dalton format
        gradient = grad.RHF(self.scf)
        gen_hcore = gradient.hcore_generator(self.molecule)
        hao = np.zeros((3*self.molecule.natm, self.molecule.nao, 
                        self.molecule.nao))

        ao_slices = self.molecule.aoslice_by_atom()

        for i in range(self.molecule.natm):
            hao[3*i:3*i+3] = gen_hcore(i)

        #print("Shape hao ", hao.shape)
        #print("Shape mo coeffs ", self.mo_coeff.shape)
        #print(self.mo_coeff)
        n_alpha = hao.shape[1]
        ###n_alpha1 = self.reference_state.n_alpha #number of occupied alpha el
        ###n_beta = self.reference_state.n_beta 
        #number of occupied beta electrons
        ###n_orbsa = self.reference_state.n_orbs_alpha #number of alpha orbitals
        ###n_orbsb = self.reference_state.n_orbs_beta #number of beta orbitals
        ###n_orbs = self.reference_state.n_orbs 
        ###print("N alpha:", n_alpha, n_alpha1, n_orbsa, n_orbs)
        ###print("N beta:", n_beta, n_orbsb)
        hcore = np.zeros((hao.shape[0],2*n_alpha,2*n_alpha))        
        hcore[:,:n_alpha, :n_alpha] = np.einsum('pm,xmq->xpq', 
                                                self.mo_coeff[0].transpose(),
                                                np.einsum('nq,xmn->xmq',
                                                self.mo_coeff[0], hao)
                                                )
        hcore[:,n_alpha:, n_alpha:] = np.einsum('pm,xmq->xpq', 
                                                self.mo_coeff[1].transpose(),
                                                np.einsum('nq,xmn->xmq',
                                                self.mo_coeff[1], hao)
                                                )
        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        else:
            ncore = 0

        self.blocks = blocks
        for block in blocks:
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            space1 = self.get_mo_space(block, ncore, nocc, nvirt, pos=0)
            space2 = self.get_mo_space(block, ncore, nocc, nvirt, pos=1)
            i = 0
            for p in space1:
                j = 0
                for q in space2:
                    #print(i,j,p,q)
                    self.matrix[block][:,i,j] = hcore[:,p,q]
                    j += 1
                i += 1

    def add_block_random(self, block, seed=None):
        if block in ["OO","CC","VV","OC","OV","CV"]:
            self.blocks.append(block)
            n1 = self.reference_state.fock(get_type(block,0)).to_ndarray().shape[0]
            n2 = self.reference_state.fock(get_type(block,1)).to_ndarray().shape[0]
            self.matrix[block] = np.random.RandomState(seed=seed).rand(3,n1,n2) 
            #the 3 comes from the 3 coordinates of the atom (X,Y,Z)
        else:
            errtxt = block + "is not a valid block for the "
            errtxt += "core Hamiltonian integral derivatives."
            raise ValueError(errtxt)

    def add_block_veloxchem(self, block):
        raise ValueError(self.backend, "Not implemented.")

    def add_block_pyscf(self, block):
        if block in ["OO","CC","VV","OC","OV","CV"]:
            self.blocks.append(block)
            n1 = self.reference_state.fock(get_type(block,0)).to_ndarray().shape[0]
            n2 = self.reference_state.fock(get_type(block,1)).to_ndarray().shape[0]
            self.matrix[block] = np.zeros((3,n1,n2)) 
            #the 3 comes from the 3 coordinates of the atom (X,Y,Z)
            #hcore = 
            # pyscf.scf.hf.RHF.Gradients(self.scf).hcore_generator(self.scf.mol)
            #print(hcore)
        else:
            errtxt = block + "is not a valid block for the "
            errtxt += "core Hamiltonian integral derivatives."
            raise ValueError(errtxt)

    def add_block_psi4(self, block):
        raise ValueError(self.backend, "Not implemented.")

class overlap(int_deriv):
    def import_block(self, block, seed=None):
        """Adds/Imports a specific block of the Overlap matrix 
        (derivative wrt nuclear coords)
        (in MO basis)

        Parameters:
        ----------
        block: type of block to be added; possible values are 
        "OO" "CC" "VV" "OC" "OV" and "CV"
        """

        if self.backend == "random":
            self.add_block_random(block, seed)
        elif self.backend == "veloxchem":
            self.add_block_veloxchem(block)
        elif self.backend == "pyscf":
            self.add_block_pyscf(block)
        elif self.backend == "psi4":
            self.add_block_psi4(block)
        else:
            raise ValueError(self.backend, "Not implemented.")

    def add_block_random(self, block, seed=None):
        if block in ["OO","CC","VV","OC","OV","CV"]:
            self.blocks.append(block)
            n1 = self.reference_state.fock(get_type(block,0)).to_ndarray().shape[0]
            n2 = self.reference_state.fock(get_type(block,1)).to_ndarray().shape[0]
            self.matrix[block] = np.random.RandomState(seed=seed).rand(3,n1,n2)
             #The 3 comes from the 3 coordinates of the atom (X,Y,Z)
        else:
            errtxt = block + "is not a valid block for the "
            errtxt += "overlap integral derivatives."
            raise ValueError(errtxt)

    def add_all_dp(self, blocks):
        sao = dp.lsdalton.overlap_matrix(self.molecule, self.basis,
                                         self.scf, geo_order=1)
        #Uncomment the lines below to use mo_coeff as the identity matrix; 
        #Still need to actually get the correct ones from somewhere
        #dalton?, Zilvinas and Olav's import routine?
        #self.mo_coeff = np.zeros((sao.shape[1],sao.shape[2])) 
        #np.fill_diagonal(self.mo_coeff, 1.0)

        #get the derivative of the core hamiltonian in MO basis:
        s = np.zeros(sao.shape)        
        s[:] = np.matmul(np.matmul(self.mo_coeff.transpose(), sao[:]), self.mo_coeff)

        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        try:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        except:
            ncore = 0

        self.blocks = blocks
        for block in blocks:
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            #print(self.matrix[block])
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            #print(n1, n2)
            i1 = get_starting_index(block, ncore, nocc, nvirt, pos=0)
            i2 = get_starting_index(block, ncore, nocc, nvirt, pos=1)
            self.matrix[block][:,0:n1,0:n2] = s[:,i1:i1+n1,i2:i2+n2]
            self.matrix[block][:,n1:,n2:] = s[:,i1:i1+n1,i2:i2+n2]

    def add_all_pyscf(self, blocks):
        dspy = -self.molecule.intor('int1e_ipovlp', aosym='s1')
        ## "-" required by pyscf.. not sure why, but see how it's imported
        ## in, for example, rhf.py in /pyscf/grad/
        #Bring overlap matrix to dalton format
        sao = np.zeros((3*self.molecule.natm, dspy.shape[1], dspy.shape[2]))
        ao_slices = self.molecule.aoslice_by_atom()

        for i in range(self.molecule.natm):
            k0, k1 = ao_slices[i, 2:]
            sao[i*3:i*3+3,k0:k1] = dspy[:,k0:k1]
        sao += sao.transpose(0,2,1) 

        #get the derivative of the core hamiltonian in MO basis:
        s = np.zeros(sao.shape)        
        s[:] = np.matmul(np.matmul(self.mo_coeff.transpose(), sao[:]),self.mo_coeff)

        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        ###    #Re-arrange s[:] so that core starts at 0
        ###    nc = int(0.5*ncore)
        ###    no = int(0.5*nocc)
        ###    c = self.reference_state.mospaces.core_orbitals[0:nc].copy()
        ###    o = self.reference_state.mospaces.occupied_orbitals[0:no].copy()
        ###    for i in range(nc):
        ###        for j in range(no):
        ###            if c[i] > o[j]:
        ###                s[:,[o[j],c[i]],:] = s[:,[c[i],o[j]],:]
        ###                s[:,:,[o[j],c[i]]] = s[:,:,[o[j],c[i]]]
        ###                swap = c[i]
        ###                c[i] = o[j]
        ###                o[j] = swap
        ###            else:
        ###                break
        ###    for i in range(no-1):
        ###        for j in range(i+1,no):
        ###            if o[i] > o[j]:
        ###                s[:,[o[j],o[i]],:] = s[:,[o[i],o[j]],:]
        ###                s[:,:,[o[j],o[i]]] = s[:,:,[o[j],o[i]]]
        ###                swap = o[i]
        ###                o[i] = o[j]
        ###                o[j] = swap
        ###            else:
        ###                break
        else:
            ncore = 0

        
        self.blocks = blocks
        for block in blocks:
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            space1 = self.get_mo_space(block, int(ncore/2), int(nocc/2),
                                       int(nvirt/2), pos=0)
            space2 = self.get_mo_space(block, int(ncore/2), int(nocc/2),
                                       int(nvirt/2), pos=1)
            #print(space1)
            #print(space2)
            i = 0
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            for p in space1:
                j = 0
                for q in space2:
                    #print(i,j,p,q)
                    self.matrix[block][:,i,j] = s[:,p,q]
                    self.matrix[block][:,i+n1,j+n2] = s[:,p,q]
                    j += 1
                i += 1

           ### #print(self.matrix[block])
           ### n1 = int(0.5*shape[1])
           ### n2 = int(0.5*shape[2])
           ### #print(n1, n2)
           ### i1 = get_starting_index(block, ncore, nocc, nvirt, pos=0)
           ### i2 = get_starting_index(block, ncore, nocc, nvirt, pos=1)
           ### self.matrix[block][:,0:n1,0:n2] = s[:,i1:i1+n1,i2:i2+n2]
           ### self.matrix[block][:,n1:,n2:] = s[:,i1:i1+n1,i2:i2+n2]

    def add_all_pyscf_unrestricted(self, blocks):
        dspy = -self.molecule.intor('int1e_ipovlp', aosym='s1')
        ## "-" required by pyscf.. not sure why, but see how it's imported
        ## in, for example, rhf.py in /pyscf/grad/
        #Bring overlap matrix to dalton format
        sao = np.zeros((3*self.molecule.natm, dspy.shape[1], dspy.shape[2]))
        ao_slices = self.molecule.aoslice_by_atom()

        for i in range(self.molecule.natm):
            k0, k1 = ao_slices[i, 2:]
            sao[i*3:i*3+3,k0:k1] = dspy[:,k0:k1]
        sao += sao.transpose(0,2,1) 

        n_alpha = sao.shape[1]
        s = np.zeros((sao.shape[0], 2*n_alpha, 2*n_alpha))

        s[:,:n_alpha,:n_alpha] = np.einsum('pm,xmq->xpq', 
                                                self.mo_coeff[0].transpose(),
                                                np.einsum('nq,xmn->xmq',
                                                self.mo_coeff[0], sao)
                                                ) #alpha
        s[:,n_alpha:, n_alpha:] = np.einsum('pm,xmq->xpq', 
                                                self.mo_coeff[1].transpose(),
                                                np.einsum('nq,xmn->xmq',
                                                self.mo_coeff[1], sao)
                                                ) #beta
        

        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        else:
            ncore = 0

        
        self.blocks = blocks
        for block in blocks:
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            space1 = self.get_mo_space(block, ncore, nocc, nvirt, pos=0)
            space2 = self.get_mo_space(block, ncore, nocc, nvirt, pos=1)
            i = 0
            for p in space1:
                j = 0
                for q in space2:
                    self.matrix[block][:,i,j] = s[:,p,q]
                    j += 1
                i += 1

    def add_block_veloxchem(self, block):
        raise ValueError(self.backend, "Not implemented.")

    def add_block_pyscf(self, block):
        raise ValueError(self.backend, "Not implemented.")

    def add_block_psi4(self, block):
        raise ValueError(self.backend, "Not implemented.")



class eri(int_deriv):
    def import_block(self, block, seed=None):
        """Adds/Imports a specific block of the electron repulsion integrals 
        (derivatives wrt nuclear corrdinates)
        (in MO basis)

        Parameters:
        ----------
        block: type of block to be added; possible values are:
        "OVOV" "CVCV" "OCVV" "OCCV" "OOVV" "CCVV" "OVVV"; 
        "OOOO", "CCCC", "OCOC" from the HF reference state (maybe not needed?);  
        """

        if self.backend == "random":
            self.add_block_random(block, seed)
        elif self.backend == "veloxchem":
            self.add_block_veloxchem(block)
        elif self.backend == "pyscf":
            self.add_block_pyscf(block)
        elif self.backend == "psi4":
            self.add_block_psi4(block)
        else:
            raise ValueError(self.backend, "Not implemented.")

    def add_block_random(self, block, seed=None):
        if block in ["OVOV","CVCV","OCCV","OOVV", "CCVV",
                     "OVVV","OOOO","CCCC","OCOC"]:
            self.blocks.append(block)
            n1 = self.reference_state.fock(get_type(block,0)).to_ndarray().shape[0]
            n2 = self.reference_state.fock(get_type(block,1)).to_ndarray().shape[0]
            n3 = self.reference_state.fock(get_type(block,2)).to_ndarray().shape[0]
            n4 = self.reference_state.fock(get_type(block,3)).to_ndarray().shape[0]
            self.matrix[block] = np.random.RandomState(seed=seed).rand(3,n1,n2,n3,n4)
            #the 3 comes from the 3 coordinates of the atom (X, Y, Z)
        else:
            errtxt = "is not a valid bock for the "
            errtxt += "ERI integral derivatives."
            raise ValueError(block, errtxt)

    def add_all_dp(self, blocks):
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these muct be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        eriao = dp.lsdalton.eri4(self.molecule, self.basis, 
                                 self.scf, geo_order=1)

        eri_mo = np.einsum('im,xmjkl->xijkl', self.mo_coeff.transpose(),
                               np.einsum('jn,xmnkl->xmjkl',self.mo_coeff.transpose(),
                               np.einsum('rk,xmnrl->xmnkl',self.mo_coeff,
                               np.einsum('sl,xmnrs->xmnrl',self.mo_coeff,eriao))
                               ))
        phys_eri = np.zeros((eri_mo.shape[0], 2*eri_mo.shape[1], 
                             2*eri_mo.shape[2], 2*eri_mo.shape[3],
                             2*eri_mo.shape[4]))

        asym_phys_eri = np.zeros(phys_eri.shape)

        n_alpha = self.mo_coeff.shape[1]
        phys_eri[:,:n_alpha,:n_alpha,:n_alpha,:n_alpha]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,n_alpha:,n_alpha:,n_alpha:,n_alpha:]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,:n_alpha, n_alpha:,:n_alpha,n_alpha:]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,n_alpha:,:n_alpha,n_alpha:,:n_alpha]=eri_mo.transpose(0,1,3,2,4)

        asym_phys_eri = phys_eri - phys_eri.transpose(0,1,2,4,3)
        #print("SHAPE:",asym_phys_eri.shape)

        del eri_mo, phys_eri, eriao
        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        else:
            ncore = 0

        self.blocks = blocks
        for block in blocks:
            shape = get_shape_eri(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            #print("Here's another shape: ",self.matrix[block].shape)
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            n3 = int(0.5*shape[3])
            n4 = int(0.5*shape[4])
            i1 = get_starting_index(block, ncore, nocc, nvirt, pos=0)
            i2 = get_starting_index(block, ncore, nocc, nvirt, pos=1)
            i3 = get_starting_index(block, ncore, nocc, nvirt, pos=2)
            i4 = get_starting_index(block, ncore, nocc, nvirt, pos=3)
            #print(n1, n2, n3, n4, i1, i2, i3, i4, n_alpha)
            if n1 == 0 or n2 == 0 or n3 == 0 or n4 == 0:
                errtxt = "You are asking for a core block, "
                errtxt += "but you did not run a CVS calculation."
                raise ValueError(errtxt, block)
            for p in [n1, 2*n1]:
                for q in [n2, 2*n2]:
                    for r in [n3, 2*n3]:
                        for s in [n4, 2*n4]:
                            p1 = p - n1
                            q2 = q - n2
                            r3 = r - n3
                            s4 = s - n4
                            m1 = int(p1 * n_alpha / n1)
                            m2 = int(q2 * n_alpha / n2)
                            m3 = int(r3 * n_alpha / n3)
                            m4 = int(s4 * n_alpha / n4)
                            #print(p1,p,q2,q,r3,r,s4,s) 
                            self.matrix[block][:,p1:p,q2:q,r3:r,s4:s] = (
                            asym_phys_eri[:,i1+m1:i1+m1+n1,i2+m2:i2+m2+n2,
                                            i3+m3:i3+m3+n3,i4+m4:i4+m4+n4])
        del asym_phys_eri

    def add_all_pyscf(self, blocks):
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these must be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        #Import eriao from pyscf and bring it to dalton format
        deri = -self.molecule.intor('int2e_ip1', aosym='s1')
        ## "-" sign because of pyscf; not 100% sure why it's needed...,
        ## but see, for example, how the same integral derivatives are imported
        ## in pyscf, /pyscf/grad/rhf.py

        ao_slices = self.molecule.aoslice_by_atom()

        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        else:
            ncore = 0
        self.blocks = blocks
        for block in blocks:
            shape = get_shape_eri(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)

        for i in range(self.molecule.natm):
            eriao = np.zeros(deri.shape)
            k0, k1 = ao_slices[i,2:]
            i0 = i*3
            i1 = i0+3
            eriao[:,k0:k1] = deri[:,k0:k1]

        # only ( nabla m | n ), but we need ( nabla m | n ) + ( m | nabla n)

            eriao += ( eriao.transpose(0,2,1,4,3) 
                     + eriao.transpose(0,3,4,1,2)
                     + eriao.transpose(0,4,3,2,1) ) #similar as above

            n_alpha = self.mo_coeff.shape[1] #number of alpha electrons

            # get the component related to the derivatives
            # of the MO coefficients
            eri_mo = np.einsum('im,xmjkl->xijkl', self.mo_coeff.transpose(),
                                   np.einsum('jn,xmnkl->xmjkl',
                                   self.mo_coeff.transpose(),
                                   np.einsum('rk,xmnrl->xmnkl',self.mo_coeff,
                                   np.einsum('sl,xmnrs->xmnrl',
                                   self.mo_coeff,eriao))
                                   ))
            asym_phys_eri = np.zeros((eri_mo.shape[0], 2*eri_mo.shape[1], 
                                 2*eri_mo.shape[2], 2*eri_mo.shape[3],
                                 2*eri_mo.shape[4]))

            #asym_phys_eri = np.zeros(phys_eri.shape)

            asym_phys_eri[:,:n_alpha,:n_alpha,:n_alpha,:n_alpha]=(
                eri_mo.transpose(0,1,3,2,4))
            asym_phys_eri[:,n_alpha:,n_alpha:,n_alpha:,n_alpha:]=(
                eri_mo.transpose(0,1,3,2,4))
            asym_phys_eri[:,:n_alpha, n_alpha:,:n_alpha,n_alpha:]=(
                eri_mo.transpose(0,1,3,2,4))
            asym_phys_eri[:,n_alpha:,:n_alpha,n_alpha:,:n_alpha]=(
                eri_mo.transpose(0,1,3,2,4))

            del eri_mo, eriao

            asym_phys_eri -= asym_phys_eri.transpose(0,1,2,4,3) 

            for block in blocks:
                space1 = self.get_mo_space(block, ncore, nocc, nvirt, pos=0)
                space2 = self.get_mo_space(block, ncore, nocc, nvirt, pos=1) 
                space3 = self.get_mo_space(block, ncore, nocc, nvirt, pos=2)
                space4 = self.get_mo_space(block, ncore, nocc, nvirt, pos=3)
                i = 0
                for p in space1:
                    j = 0
                    for q in space2:
                        k = 0
                        for r in space3:
                            l=0
                            for s in space4:
                                self.matrix[block][i0:i1,i,j,k,l] = (
                                    asym_phys_eri[:,p,q,r,s] )
                                l += 1
                            k += 1
                        j += 1
                    i += 1

            del asym_phys_eri

    def add_all_pyscf_1(self, blocks): #Old and memory inefficient
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these must be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        #Import eriao from pyscf and bring it to dalton format
        deri = -self.molecule.intor('int2e_ip1', aosym='s1')
        ## "-" sign because of pyscf; not 100% sure why it's needed...,
        ## but see, for example, how the same integral derivatives are imported
        ## in pyscf, /pyscf/grad/rhf.py
        print(deri.shape)
        eriao = np.zeros((3*self.molecule.natm, deri.shape[1],
                          deri.shape[2], deri.shape[3], deri.shape[4]))
        ao_slices = self.molecule.aoslice_by_atom()


        d_sao_intm = -self.molecule.intor('int1e_ipovlp', aosym='s1')
        d_sao=np.zeros((3*self.molecule.natm, self.molecule.nao, self.molecule.nao))
        #required for the derivatives of the MO coefficients 

        for i in range(self.molecule.natm):
            k0, k1 = ao_slices[i,2:]
            i0 = i*3
            i1 = i0+3
            d_sao[i0:i1,k0:k1] = d_sao_intm[:,k0:k1]
            eriao[i0:i1,k0:k1] = deri[:,k0:k1]

        d_sao += d_sao.transpose(0,2,1) # needed because pyscf generates
        # only ( nabla m | n ), but we need ( nabla m | n ) + ( m | nabla n)

        eriao += ( eriao.transpose(0,2,1,4,3) 
                 + eriao.transpose(0,3,4,1,2)
                 + eriao.transpose(0,4,3,2,1) ) #similar as above

        n_alpha = self.mo_coeff.shape[1] #number of alpha electrons

        # get the component related to the derivatives
        # of the MO coefficients
        sao = self.scf.get_ovlp() #overlap matrix in ao basis
        sao_inv = np.linalg.inv(sao)
        d_mo_coeff = (-0.5*np.einsum('mn,xnp->xmp',sao_inv,
                        np.einsum('xns,sp->xnp',d_sao, self.scf.mo_coeff)
                                    )
                     ) # see Szabo and Ostlund, p. 441
        d_c_mo=np.zeros((3*self.molecule.natm,2*self.molecule.nao,
                         2*self.molecule.nao))
        c_mo=np.zeros((2*self.molecule.nao,2*self.molecule.nao))


        d_c_mo[:,:n_alpha,:n_alpha] = d_mo_coeff
        d_c_mo[:,n_alpha:,n_alpha:] = d_mo_coeff
        c_mo[:n_alpha,:n_alpha] = self.scf.mo_coeff
        c_mo[n_alpha:,n_alpha:] = self.scf.mo_coeff

        chem_eriao = self.molecule.intor('int2e', aosym='s1')
        phys_eriao = np.zeros((2*chem_eriao.shape[0],2*chem_eriao.shape[1],
                               2*chem_eriao.shape[2],2*chem_eriao.shape[3]))
        asym_phys_eriao = np.zeros(phys_eriao.shape)

        phys_eriao[:n_alpha,:n_alpha,:n_alpha,:n_alpha]=chem_eriao.transpose(0,2,1,3)
        phys_eriao[n_alpha:,n_alpha:,n_alpha:,n_alpha:]=chem_eriao.transpose(0,2,1,3)
        phys_eriao[:n_alpha,n_alpha:,:n_alpha,n_alpha:]=chem_eriao.transpose(0,2,1,3)
        phys_eriao[n_alpha:,:n_alpha,n_alpha:,:n_alpha]=chem_eriao.transpose(0,2,1,3)
        asymm_phys_eriao = phys_eriao - phys_eriao.transpose(0,1,3,2)

        #There must be a better way to do all of the above..
    
        #print("Just double checking: ", np.amin(eriao), np.amax(eriao))

        eri_mo = np.einsum('im,xmjkl->xijkl', self.mo_coeff.transpose(),
                               np.einsum('jn,xmnkl->xmjkl',self.mo_coeff.transpose(),
                               np.einsum('rk,xmnrl->xmnkl',self.mo_coeff,
                               np.einsum('sl,xmnrs->xmnrl',self.mo_coeff,eriao))
                               ))
        phys_eri = np.zeros((eri_mo.shape[0], 2*eri_mo.shape[1], 
                             2*eri_mo.shape[2], 2*eri_mo.shape[3],
                             2*eri_mo.shape[4]))

        asym_phys_eri = np.zeros(phys_eri.shape)

        phys_eri[:,:n_alpha,:n_alpha,:n_alpha,:n_alpha]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,n_alpha:,n_alpha:,n_alpha:,n_alpha:]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,:n_alpha, n_alpha:,:n_alpha,n_alpha:]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,n_alpha:,:n_alpha,n_alpha:,:n_alpha]=eri_mo.transpose(0,1,3,2,4)

        asym_phys_eri = ( phys_eri - phys_eri.transpose(0,1,2,4,3) )
                       #### + 4.0*np.einsum('pm,xmqrs->xpqrs', c_mo.T,
                       ####         np.einsum('qn,xmnrs->xmqrs', c_mo.T,
                       ####         np.einsum('tr,xmnts->xmnrs', c_mo,
                       ####         np.einsum('xls,mntl->xmnts',d_c_mo,asymm_phys_eriao)
                       ####      )
                       ####    )
                       ####  )
                        #)
        #print("SHAPE:",asym_phys_eri.shape)

        del eri_mo, phys_eri, eriao
        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
            ####Re-arrange asym_phys_eri[:] so that core starts at 0
            ###nc = int(0.5*ncore)
            ###no = int(0.5*nocc)
            ###c = self.reference_state.mospaces.core_orbitals[0:nc].copy()
            ###o = self.reference_state.mospaces.occupied_orbitals[0:no].copy()
            ###for i in range(nc):
            ###    for j in range(no):
            ###        if c[i] > o[j]:
            ###            asym_phys_eri[:,[o[j],c[i]],:,:,:] = (
            ###                            asym_phys_eri[:,[c[i],o[j]],:,:,:] )
            ###            asym_phys_eri[:,:,[o[j],c[i]],:,:] = (
            ###                            asym_phys_eri[:,:,[o[j],c[i]],:,:])
            ###            asym_phys_eri[:,:,:,[o[j],c[i]],:] = (
            ###                            asym_phys_eri[:,:,:,[c[i],o[j]],:] )
            ###            asym_phys_eri[:,:,:,:,[o[j],c[i]]] = (
            ###                            asym_phys_eri[:,:,:,:,[o[j],c[i]]])
            ###            swap = c[i]
            ###            c[i] = o[j]
            ###            o[j] = swap
            ###        else:
            ###            break
            ###for i in range(no-1):
            ###    for j in range(i+1,no):
            ###        if o[i] > o[j]:
            ###            asym_phys_eri[:,[o[j],o[i]],:,:,:] = (
            ###                            asym_phys_eri[:,[o[i],o[j]],:,:,:] )
            ###            asym_phys_eri[:,:,[o[j],o[i]],:,:] = (
            ###                            asym_phys_eri[:,:,[o[j],o[i]],:,:])
            ###            asym_phys_eri[:,:,:,[o[j],c[i]],:] = (
            ###                            asym_phys_eri[:,:,:,[o[i],o[j]],:] )
            ###            asym_phys_eri[:,:,:,:,[o[j],c[i]]] = (
            ###                            asym_phys_eri[:,:,:,:,[o[j],o[i]]])
            ###            swap = o[i]
            ###            o[i] = o[j]
            ###            o[j] = swap
            ###        else:
            ###            break
        else:
            ncore = 0

        self.blocks = blocks
        for block in blocks:
            shape = get_shape_eri(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            #print(block)
            #print("Here's another shape: ",self.matrix[block].shape)
            #print(asym_phys_eri.shape)
            #print(ncore, nocc, nvirt)
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            n3 = int(0.5*shape[3])
            n4 = int(0.5*shape[4])
            space1 = self.get_mo_space(block, ncore, nocc, nvirt, pos=0)
            space2 = self.get_mo_space(block, ncore, nocc, nvirt, pos=1) 
            space3 = self.get_mo_space(block, ncore, nocc, nvirt, pos=2)
            space4 = self.get_mo_space(block, ncore, nocc, nvirt, pos=3)
            i = 0
            for p in space1:
                j = 0
                for q in space2:
                    k = 0
                    for r in space3:
                        l=0
                        for s in space4:
                            self.matrix[block][:,i,j,k,l] = asym_phys_eri[:,p,q,r,s]
                            l += 1
                        k += 1
                    j += 1
                i += 1

        ###    i1 = get_starting_index(block, ncore, nocc, nvirt, pos=0)
        ###    i2 = get_starting_index(block, ncore, nocc, nvirt, pos=1)
        ###    i3 = get_starting_index(block, ncore, nocc, nvirt, pos=2)
        ###    i4 = get_starting_index(block, ncore, nocc, nvirt, pos=3)
        ###    #print(n1, n2, n3, n4, i1, i2, i3, i4, n_alpha)
        ###    if n1 == 0 or n2 == 0 or n3 == 0 or n4 == 0:
        ###        errtxt = "You are asking for a core block, "
        ###        errtxt += "but you did not run a CVS calculation."
        ###        raise ValueError(errtxt, block)
        ###    for p in [n1, 2*n1]:
        ###        for q in [n2, 2*n2]:
        ###            for r in [n3, 2*n3]:
        ###                for s in [n4, 2*n4]:
        ###                    p1 = p - n1
        ###                    q2 = q - n2
        ###                    r3 = r - n3
        ###                    s4 = s - n4
        ###                    m1 = int(p1 * n_alpha / n1)
        ###                    m2 = int(q2 * n_alpha / n2)
        ###                    m3 = int(r3 * n_alpha / n3)
        ###                    m4 = int(s4 * n_alpha / n4)
        ###                    #print(p1,p,q2,q,r3,r,s4,s) 
        ###                    self.matrix[block][:,p1:p,q2:q,r3:r,s4:s] = (
        ###                    asym_phys_eri[:,i1+m1:i1+m1+n1,i2+m2:i2+m2+n2,
        ###                                    i3+m3:i3+m3+n3,i4+m4:i4+m4+n4])
        ###                    print(p,q,r,s)
        ###                    print(p1,q2,r3,s4)
        ###                    print(i1,i2,i3,i4)
        ###                    print(m1,m2,m3,m4)
        ###                    print(n1,n2,n3,n4)
        del asym_phys_eri

    def add_all_pyscf_unrestricted(self, blocks):
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these must be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        #Import eriao from pyscf and bring it to dalton format
        deri = -self.molecule.intor('int2e_ip1', aosym='s1')
        ## "-" sign because of pyscf; not 100% sure why it's needed...,
        ## but see, for example, how the same integral derivatives are imported
        ## in pyscf, /pyscf/grad/rhf.py
        eriao = np.zeros((3*self.molecule.natm, deri.shape[1],
                          deri.shape[2], deri.shape[3], deri.shape[4]))
        ao_slices = self.molecule.aoslice_by_atom()


        for i in range(self.molecule.natm):
            k0, k1 = ao_slices[i,2:]
            i0 = i*3
            i1 = i0+3
            eriao[i0:i1,k0:k1] = deri[:,k0:k1]

        # needed because pyscf generates
        # only ( nabla m | n ), but we need ( nabla m | n ) + ( m | nabla n)

        eriao += ( eriao.transpose(0,2,1,4,3) 
                 + eriao.transpose(0,3,4,1,2)
                 + eriao.transpose(0,4,3,2,1) ) #similar as above

        n_alpha = eriao.shape[1] #number of alpha orbitals

        # get the component related to the derivatives
        # of the MO coefficients


        #print("Just double checking: ", np.amin(eriao), np.amax(eriao))

        eri_mo = np.zeros((eriao.shape[0], 2*n_alpha, 2*n_alpha, 
                          2*n_alpha, 2*n_alpha))
        #print("SHAPES:", eri_mo.shape, eriao.shape, self.mo_coeff.shape)

        for p in [0,1]:
            p1 = p*n_alpha
            for q in [0,1]:
                q1 = q*n_alpha
                #for r in [0,1]:
                #    r1 = r*n_alpha
                #    for s in [0,1]:
                #        s1 = s*n_alpha
                eri_mo[:,p1:p1+n_alpha, p1:p1+n_alpha,
                         q1:q1+n_alpha,q1:q1+n_alpha] = (
            np.einsum('im,xmjkl->xijkl', self.mo_coeff[p].transpose(),
            np.einsum('jn,xmnkl->xmjkl',self.mo_coeff[p].transpose(),
            np.einsum('rk,xmnrl->xmnkl',self.mo_coeff[q],
            np.einsum('sl,xmnrs->xmnrl',self.mo_coeff[q], eriao))))
            )
        phys_eri = np.zeros(eri_mo.shape)

        asym_phys_eri = np.zeros(phys_eri.shape)

        phys_eri = eri_mo.transpose(0,1,3,2,4)

        asym_phys_eri = ( phys_eri - phys_eri.transpose(0,1,2,4,3) )

        del eri_mo, phys_eri, eriao
        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        else:
            ncore = 0

        self.blocks = blocks
        for block in blocks:
            shape = get_shape_eri(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            n3 = int(0.5*shape[3])
            n4 = int(0.5*shape[4])
            space1 = self.get_mo_space(block, ncore, nocc, nvirt, pos=0)
            space2 = self.get_mo_space(block, ncore, nocc, nvirt, pos=1) 
            space3 = self.get_mo_space(block, ncore, nocc, nvirt, pos=2)
            space4 = self.get_mo_space(block, ncore, nocc, nvirt, pos=3)
            i = 0
            for p in space1:
                j = 0
                for q in space2:
                    k = 0
                    for r in space3:
                        l=0
                        for s in space4:
                            self.matrix[block][:,i,j,k,l] = (
                                asym_phys_eri[:,p,q,r,s] )
                            l += 1
                        k += 1
                    j += 1
                i += 1

        del asym_phys_eri

    def compute_pyscf_unrestricted(self, DM2):
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these must be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        #Import eriao from pyscf and bring it to dalton format
        deri = -self.molecule.intor('int2e_ip1', aosym='s1')
        ## "-" sign because of pyscf; not 100% sure why it's needed...,
        ## but see, for example, how the same integral derivatives are imported
        ## in pyscf, /pyscf/grad/rhf.py
        #eriao = np.zeros((3*self.molecule.natm, deri.shape[1],
        #                  deri.shape[2], deri.shape[3], deri.shape[4]))
        ao_slices = self.molecule.aoslice_by_atom()

        eri_dm2 = np.zeros((self.molecule.natm,3))
        coeff_alpha = self.mo_coeff[0]
        coeff_beta = self.mo_coeff[1]
        nao  = coeff_alpha.shape[0]
        dm2_KmJ = np.zeros((nao,nao,nao,nao))
        dm2_K = np.zeros((nao,nao,nao,nao))
        dm2_J = np.zeros((nao,nao,nao,nao))

        for block in DM2.blocks:
            space1 = self.get_mo_space(block, ncore, nocc, nvirt, pos=0)
            space2 = self.get_mo_space(block, ncore, nocc, nvirt, pos=1) 
            space3 = self.get_mo_space(block, ncore, nocc, nvirt, pos=2)
            space4 = self.get_mo_space(block, ncore, nocc, nvirt, pos=3)
            i = 0
            for p in space1:
                j = 0
                for q in space2:
                    k = 0
                    for r in space3:
                        l=0
                        for s in space4:
                            self.matrix[block][:,i,j,k,l] = (
                                asym_phys_eri[:,p,q,r,s] )
                            l += 1
                        k += 1
                    j += 1
                i += 1

        for i in range(self.molecule.natm):
            k0, k1 = ao_slices[i,2:]
            i0 = i*3
            i1 = i0+3
            eriao[i0:i1,k0:k1] = deri[:,k0:k1]

        # needed because pyscf generates
        # only ( nabla m | n ), but we need ( nabla m | n ) + ( m | nabla n)

        eriao += ( eriao.transpose(0,2,1,4,3) 
                 + eriao.transpose(0,3,4,1,2)
                 + eriao.transpose(0,4,3,2,1) ) #similar as above

        n_alpha = eriao.shape[1] #number of alpha orbitals

        # get the component related to the derivatives
        # of the MO coefficients


        #print("Just double checking: ", np.amin(eriao), np.amax(eriao))

        eri_mo = np.zeros((eriao.shape[0], 2*n_alpha, 2*n_alpha, 
                          2*n_alpha, 2*n_alpha))
        #print("SHAPES:", eri_mo.shape, eriao.shape, self.mo_coeff.shape)

        for p in [0,1]:
            p1 = p*n_alpha
            for q in [0,1]:
                q1 = q*n_alpha
                #for r in [0,1]:
                #    r1 = r*n_alpha
                #    for s in [0,1]:
                #        s1 = s*n_alpha
                eri_mo[:,p1:p1+n_alpha, p1:p1+n_alpha,
                         q1:q1+n_alpha,q1:q1+n_alpha] = (
            np.einsum('im,xmjkl->xijkl', self.mo_coeff[p].transpose(),
            np.einsum('jn,xmnkl->xmjkl',self.mo_coeff[p].transpose(),
            np.einsum('rk,xmnrl->xmnkl',self.mo_coeff[q],
            np.einsum('sl,xmnrs->xmnrl',self.mo_coeff[q], eriao))))
            )
        phys_eri = np.zeros(eri_mo.shape)

        asym_phys_eri = np.zeros(phys_eri.shape)

        phys_eri = eri_mo.transpose(0,1,3,2,4)

        asym_phys_eri = ( phys_eri - phys_eri.transpose(0,1,2,4,3) )

        del eri_mo, phys_eri, eriao
        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        else:
            ncore = 0

        self.blocks = blocks
        for block in blocks:
            shape = get_shape_eri(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            n3 = int(0.5*shape[3])
            n4 = int(0.5*shape[4])
            space1 = self.get_mo_space(block, ncore, nocc, nvirt, pos=0)
            space2 = self.get_mo_space(block, ncore, nocc, nvirt, pos=1) 
            space3 = self.get_mo_space(block, ncore, nocc, nvirt, pos=2)
            space4 = self.get_mo_space(block, ncore, nocc, nvirt, pos=3)
            i = 0
            for p in space1:
                j = 0
                for q in space2:
                    k = 0
                    for r in space3:
                        l=0
                        for s in space4:
                            self.matrix[block][:,i,j,k,l] = (
                                asym_phys_eri[:,p,q,r,s] )
                            l += 1
                        k += 1
                    j += 1
                i += 1

        del asym_phys_eri

    def add_block_veloxchem(self, block):
        raise ValueError(self.backend, "Not implemented.")

    def add_block_pyscf(self, block):
        raise ValueError(self.backend, "Not implemented.")

    def add_block_psi4(self, block):
        raise ValueError(self.backend, "Not implemented.")

class eri_sum_i(int_deriv):
    def import_block(self, block, seed=None):
        """Adds/Imports a specific block of the electron repulsion integrals 
        (derivatives wrt nuclear corrdinates)
        and sums over the occupied space \sum_i <pi||qi>^\csi
        (in MO basis)

        Parameters:
        ----------
        block: type of block to be added; 
        possible values are "OO" "CC" "VV" "OC" "OV" "CV"; 
        """

        if self.backend == "random":
            self.compute_block_random(block, seed)
        elif self.backend == "veloxchem":
            self.compute_block_veloxchem(block)
        elif self.backend == "pyscf":
            self.compute_block_pyscf(block)
        elif self.backend == "psi4":
            self.compute_block_psi4(block)
        else:
            raise ValueError(self.backend, "Not implemented.")

    def compute_block_random(self, block, seed=None):
        if block in ["OO","CC","VV","OC","OV","CV"]:
            self.blocks.append(block)
            n1 = self.reference_state.fock(get_type(block,0)).to_ndarray().shape[0]
            n2 = self.reference_state.fock(get_type(block,1)).to_ndarray().shape[0]
            self.matrix[block] = np.random.RandomState(seed=seed).rand(3,n1,n2)
            #The 3 comes from the 3 coordinates of the atom (X,Y,Z)
        else:
            errtxt = block + " is not a valid block for "
            errxtx += "the eri-sum-i object."
            raise ValueError(errtxt)

    def add_all_dp(self, blocks):
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these muct be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        #TODO: allow UHF import
        eriao = dp.lsdalton.eri4(self.molecule, self.basis, 
                                 self.scf, geo_order=1)

        eri_mo = np.einsum('im,xmjkl->xijkl', self.mo_coeff.transpose(),
                               np.einsum('jn,xmnkl->xmjkl',self.mo_coeff.transpose(),
                               np.einsum('rk,xmnrl->xmnkl',self.mo_coeff,
                               np.einsum('sl,xmnrs->xmnrl',self.mo_coeff,eriao))
                               ))
        phys_eri = np.zeros((eri_mo.shape[0], 2*eri_mo.shape[1], 
                             2*eri_mo.shape[2], 2*eri_mo.shape[3],
                             2*eri_mo.shape[4]))

        asym_phys_eri = np.zeros(phys_eri.shape)

        n_alpha = self.mo_coeff.shape[1]
        phys_eri[:,:n_alpha,:n_alpha,:n_alpha,:n_alpha]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,n_alpha:,n_alpha:,n_alpha:,n_alpha:]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,:n_alpha, n_alpha:,:n_alpha,n_alpha:]=eri_mo.transpose(0,1,3,2,4)
        phys_eri[:,n_alpha:,:n_alpha,n_alpha:,:n_alpha]=eri_mo.transpose(0,1,3,2,4)

        asym_phys_eri = phys_eri - phys_eri.transpose(0,1,2,4,3)
        #print("SHAPE:",asym_phys_eri.shape)
        del eri_mo, phys_eri, eriao

        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        try:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        except:
            ncore = 0

        occ = int(0.5*(nocc+ncore))
        eri_sumi = (
            np.einsum('xpiqi->xpq',asym_phys_eri[:,:,:occ,:,:occ],optimize=True)
            +np.einsum('xpiqi->xpq',
                       asym_phys_eri[:,:,n_alpha:n_alpha+occ,:,n_alpha:n_alpha+occ],
                       optimize=True)
            )
        #print(eri_sumi)


        self.blocks = blocks
        for block in blocks:
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)
            #print(self.matrix[block])
            n1 = int(0.5*shape[1])
            n2 = int(0.5*shape[2])
            #print(n1, n2)
            i1 = get_starting_index(block, ncore, nocc, nvirt, pos=0)
            i2 = get_starting_index(block, ncore, nocc, nvirt, pos=1)

            self.matrix[block][:,:n1,:n2]=eri_sumi[:,i1:i1+n1,i2:i2+n2]
            self.matrix[block][:,n1:,n2:]=eri_sumi[:,i1:i1+n1,i2:i2+n2]

        del asym_phys_eri, eri_sumi
            #np.einsum('xpiqi->xpq',
            #eri[:,i1:i1+n1,0:occ,i2:i2+n2,0:occ],optimize=True)
            #self.matrix[block][:,n1:,n2:]=np.einsum('xpiqi->xpq',
            #                                eri[:,i1:i1+n1,0:occ,i2:i2+n2,0:occ],
            #                                optimize=True)

    def add_all_pyscf(self, blocks):
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these must be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        #Import eriao in dalton format
        deri = -self.molecule.intor('int2e_ip1', aosym='s1')
        ## "-" sign because of pyscf; not 100% sure why it's needed...,
        ## but see, for example, how the same integral derivatives are imported
        ## in pyscf, /pyscf/grad/rhf.py
        eriao = np.zeros((3*self.molecule.natm, deri.shape[1],
                          deri.shape[2], deri.shape[3], deri.shape[4]))
        ao_slices = self.molecule.aoslice_by_atom()

        n_alpha = self.mo_coeff.shape[1] #number of alpha orbitlas


        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        else:
            ncore = 0
        occ = int(0.5*(nocc+ncore)) #number of occupied orbitals

        self.blocks = blocks
        for block in blocks:
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)

        for i in range(self.molecule.natm):
            eriao = np.zeros(deri.shape)
            k0, k1 = ao_slices[i,2:]
            i0 = i*3
            i1 = i0+3
            eriao[:,k0:k1] = deri[:,k0:k1]

            # only ( nabla m | n ), but we need ( nabla m | n ) + ( m | nabla n)
            eriao += ( eriao.transpose(0,2,1,4,3) 
                     + eriao.transpose(0,3,4,1,2)
                     + eriao.transpose(0,4,3,2,1) ) #similar as above


            eri_mo = np.einsum('im,xmjkl->xijkl', self.mo_coeff.transpose(),
                                   np.einsum('jn,xmnkl->xmjkl',
                                   self.mo_coeff.transpose(),
                                   np.einsum('rk,xmnrl->xmnkl',
                                   self.mo_coeff,
                                   np.einsum('sl,xmnrs->xmnrl',
                                   self.mo_coeff,eriao))
                                   ))
            phys_eri = np.zeros((eri_mo.shape[0], 2*eri_mo.shape[1], 
                                 2*eri_mo.shape[2], 2*eri_mo.shape[3],
                                 2*eri_mo.shape[4]))

            asym_phys_eri = np.zeros(phys_eri.shape)

            phys_eri[:,:n_alpha,:n_alpha,:n_alpha,:n_alpha]=(
                    eri_mo.transpose(0,1,3,2,4) )
            phys_eri[:,n_alpha:,n_alpha:,n_alpha:,n_alpha:]=(
                    eri_mo.transpose(0,1,3,2,4))
            phys_eri[:,:n_alpha, n_alpha:,:n_alpha,n_alpha:]=(
                    eri_mo.transpose(0,1,3,2,4))
            phys_eri[:,n_alpha:,:n_alpha,n_alpha:,:n_alpha]=(
                    eri_mo.transpose(0,1,3,2,4))

            asym_phys_eri = ( phys_eri - phys_eri.transpose(0,1,2,4,3) )
            del eri_mo, phys_eri, eriao

            eri_sumi = (
                np.einsum('xpiqi->xpq',asym_phys_eri[:,:,:occ,:,:occ],
                          optimize=True)
              + np.einsum('xpiqi->xpq',
                asym_phys_eri[:,:,n_alpha:n_alpha+occ,:,n_alpha:n_alpha+occ],
                optimize=True)
            )
            for block in blocks:
                shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
                n1 = int(0.5*shape[1])
                n2 = int(0.5*shape[2])
                space1 = self.get_mo_space(block, int(ncore/2), int(nocc/2),
                                           int(nvirt/2), pos=0)
                space2 = self.get_mo_space(block, int(ncore/2), int(nocc/2),
                                           int(nvirt/2), pos=1)
                i = 0
                for p in space1:
                    j = 0
                    for q in space2:
                        self.matrix[block][i0:i1,i,j] = eri_sumi[:,p,q]
                        self.matrix[block][i0:i1,i+n1,j+n2] = eri_sumi[:,p,q]
                        j += 1
                    i += 1
            del asym_phys_eri, eri_sumi


    def add_all_pyscf_unrestricted(self, blocks):
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these must be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        """
        eriao contains the derivatives of the two-electron
        integrals in ao basis and chemist's notation;
        for the adc gradient, these must be transformed
        into antisymmetrized two-electron integrals 
        in physicist's notation.
        """
        #Import eriao from pyscf and bring it to dalton format
        deri = -self.molecule.intor('int2e_ip1', aosym='s1')
        ## "-" sign because of pyscf; not 100% sure why it's needed...,
        ## but see, for example, how the same integral derivatives are imported
        ## in pyscf, /pyscf/grad/rhf.py
        eriao = np.zeros((3*self.molecule.natm, deri.shape[1],
                          deri.shape[2], deri.shape[3], deri.shape[4]))
        ao_slices = self.molecule.aoslice_by_atom()


        for i in range(self.molecule.natm):
            k0, k1 = ao_slices[i,2:]
            i0 = i*3
            i1 = i0+3
            eriao[i0:i1,k0:k1] = deri[:,k0:k1]

        # needed because pyscf generates
        # only ( nabla m | n ), but we need ( nabla m | n ) + ( m | nabla n)

        eriao += ( eriao.transpose(0,2,1,4,3) 
                 + eriao.transpose(0,3,4,1,2)
                 + eriao.transpose(0,4,3,2,1) ) #similar as above

        n_alpha = eriao.shape[1] #number of alpha orbitals

        # get the component related to the derivatives
        # of the MO coefficients


        #print("Just double checking: ", np.amin(eriao), np.amax(eriao))

        eri_mo = np.zeros((eriao.shape[0], 2*n_alpha, 2*n_alpha, 
                          2*n_alpha, 2*n_alpha))
        #print("SHAPES:", eri_mo.shape, eriao.shape, self.mo_coeff.shape)

        for p in [0,1]:
            p1 = p*n_alpha
            for q in [0,1]:
                q1 = q*n_alpha
                #for r in [0,1]:
                #    r1 = r*n_alpha
                #    for s in [0,1]:
                #        s1 = s*n_alpha
                #       if p==r
                eri_mo[:,p1:p1+n_alpha, p1:p1+n_alpha,
                         q1:q1+n_alpha,q1:q1+n_alpha] = (
            np.einsum('im,xmjkl->xijkl', self.mo_coeff[p].transpose(),
            np.einsum('jn,xmnkl->xmjkl',self.mo_coeff[p].transpose(),
            np.einsum('rk,xmnrl->xmnkl',self.mo_coeff[q],
            np.einsum('sl,xmnrs->xmnrl',self.mo_coeff[q], eriao))))
            )
        phys_eri = np.zeros(eri_mo.shape)

        asym_phys_eri = np.zeros(phys_eri.shape)

        phys_eri = eri_mo.transpose(0,1,3,2,4)

        asym_phys_eri = ( phys_eri - phys_eri.transpose(0,1,2,4,3) )

        del eri_mo, phys_eri, eriao

        #There must be a better way to get these numbers!
        nocc = self.reference_state.fock("o1o1").to_ndarray().shape[0]
        nvirt = self.reference_state.fock("v1v1").to_ndarray().shape[0]
        if self.reference_state.has_core_occupied_space:
            ncore = self.reference_state.fock("o2o2").to_ndarray().shape[0]
        else:
            ncore = 0

        occ_alpha = self.reference_state.n_alpha #number of occupied alpha el
        occ_beta = self.reference_state.n_beta #number of occupied beta electrons
        #occ = int(0.5*(nocc+ncore))
        #Needed here: number of alpha occ, beta occ
        eri_sumi = (
            np.einsum('xpiqi->xpq',asym_phys_eri[:,:,:occ_alpha,:,:occ_alpha],optimize=True)
            +np.einsum('xpiqi->xpq',
                       asym_phys_eri[:,:,n_alpha:n_alpha+occ_beta,
                                     :,n_alpha:n_alpha+occ_beta],
                       optimize=True)
            #+np.einsum('xpiqi->xpq',
            #           asym_phys_eri[:,:,:occ,
            #                         :,n_alpha:n_alpha+occ],
            #           optimize=True)
            #+np.einsum('xpiqi->xpq',
            #           asym_phys_eri[:,:,n_alpha:n_alpha+occ,
            #                         :,:occ],
            #           optimize=True)
            ) #Is this really correct??
        #print(eri_sumi)
        self.blocks = blocks
        for block in blocks:
            shape = get_shape(block, ncore, nocc, nvirt, self.natoms)
            self.matrix[block] = np.zeros(shape)

            space1 = self.get_mo_space(block, ncore, nocc, nvirt, pos=0)
            space2 = self.get_mo_space(block, ncore, nocc, nvirt, pos=1)
            i = 0
            for p in space1:
                j = 0
                for q in space2:
                    #print(i,j,p,q)
                    self.matrix[block][:,i,j] = eri_sumi[:,p,q]
                    j += 1
                i += 1

        del asym_phys_eri, eri_sumi

    def add_block_veloxchem(self, block):
        raise ValueError(self.backend, "Not implemented.")

    def add_block_pyscf(self, block):
        raise ValueError(self.backend, "Not implemented.")

    def add_block_psi4(self, block):
        raise ValueError(self.backend, "Not implemented.")

class HF_gradient():
    def __init__(self, backend='random', atoms=[], scf=None):
        """Initializes an object used to import the HF energy gradient from
        one of the supported electronic structure codes 
        
        Parameters:
        ----------
        backend: electronic structure code to import integral derivatives from;
        "random" is for testing only
        atoms: list of atoms with respect to which the HF energy gradient is imported
        """
        #self.reference_state = refstate
        self.backend = backend
        self.atoms = atoms
        self.matrix = None 
        self.scf = scf 

    def import_HF_gradient(self, seed=None):
        """Imports the HF energy gradient from the electronic structure code 
        defined by self.backend
        """

        if self.backend == "random":
            self.matrix = np.random.RandomState(seed=seed).rand(len(self.atoms),3)
        elif self.backend == "veloxchem":
            pass
        elif self.backend == "pyscf":
            gradient = pyscf.grad.RHF(self.scf)
            self.matrix = gradient.grad_nuc()

        elif self.backend == "psi4":
            pass
        else:
            raise ValueError(self.backend, "Not implemented.")
