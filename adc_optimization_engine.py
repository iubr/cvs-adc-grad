#               
#                               ADC_GRADIENT
#	  --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------

import geometric
import adcc
from numpy import linalg

class ADCOptimizationEngine(geometric.engine.Engine):
    """
    Implements an optimization engine for geomeTRIC

    Parameters:
    ----------
    
    Instance variables:
    ------------------
    """

    def __init__(self, molecule, scf_drv, adc_drv, grad_drv,
                 backend='pyscf', state=0, ao_basis=None, min_basis=None,
                 verbose=False, maxiter=300):
        """
        Initializes the optimization engine for geomeTRIC

        Parameters:
        ----------
        molecule: molecule object, depends on the backend
        scf_drv : SCF driver to compute the reference state
        adc_drv : driver for the ADC calculation
        grad_drv: driver for the gradient calculation
        backend : code uesd for the scf reference state
                  possible values: pyscf, vlx, psi4
        state   : index of the excited state to be optimized
        """
        g_molecule = geometric.molecule.Molecule()
        self.backend = backend
        self.state = state
        self.verbose = verbose
        self.maxiter = maxiter

        if backend == 'pyscf': 
            g_molecule.elem = [molecule.atom_symbol(i) for i in range(molecule.natm)]
            g_molecule.xyzs = [molecule.atom_coords() * geometric.nifty.bohr2ang]
            super().__init__(g_molecule)

            self.molecule = molecule #contains basis set info
            self.scf_drv = scf_drv #pyscf.scf.RHF object; conv_tol set
            self.adc_drv = adc_drv #ADCDriver object
                                   #adc calculation 
            self.grad_drv = grad_drv #adc_gradient object

        elif backend == 'vlx':
            g_molecule.elem = molecule.get_labels() 
            g_molecule.xyzs = [
                molecule.get_coordinates() * geometric.nifty.bohr2ang
            ]
            #TODO: finish; vlx is not finished, so not working;
        else:
            raise NotImplementedError("Only pyscf and vlx are implemented.")

        

    def calc_new(self, coords, dirname):
        """
        Implements a calc_new method for the optimization engine

        Parameters:
        ---------
        coords : array of atom coordinates?
        dirname: relative path to the directory where to save files

        Returns:
        -------
        a dictionary containing the energy and gradient
        """ 
        
        if self.backend == "pyscf":
            from pyscf import gto, scf

            moltxt = ''
            reshaped_coord = coords.reshape(-1,3) * geometric.nifty.bohr2ang
            for i in range(self.molecule.natm):
                new_coords = "  %18.12f %18.12f %18.12f\n" % (reshaped_coord[i,0], 
                                                           reshaped_coord[i,1],
                                                           reshaped_coord[i,2])
                moltxt += self.molecule.atom_symbol(i)+new_coords

            print()            
            print("     These are the coordinates:\n")
            print(moltxt)
            #print(self.molecule.ecp)    
            new_molecule = gto.M(atom=moltxt, basis=self.molecule.basis,
                                 ecp=self.molecule.ecp, 
                                 unit="Angstrom", charge=self.molecule.charge,
                                 spin=self.molecule.spin)

            # run SCF on the new coordnates
            self.scf_drv.reset(new_molecule)
            self.scf_drv.kernel()

            # run ADC on the new reference state
            self.adc_drv.reset(self.scf_drv)
            adc = self.adc_drv.compute()

            if "adc" in self.adc_drv.method or "cvs" in self.adc_drv.method:
                energy = adc.excitation_energy[self.state]
            else:
                energy = 0.0
            if self.adc_drv.method in ["cvs1", "cvs-adc1", "adc1",
                                       "cvs0", "cvs-adc0", "adc0", "hf"]:
                energy += self.scf_drv.energy_tot()

            elif self.adc_drv.method in ["cvs2", "cvs-adc2", "adc2", "gs_mp2",
                                         "cvs2x", "cvs-adc2x", "adc2x"]:
                energy += adcc.LazyMp(adc.reference_state).energy(2)

            print()
            print("     Total Energy (Hartree):", energy)
            print()
            # compute gradient for the new geometry
            self.grad_drv.reset(refstate=adc.reference_state,
                                vector=adc.excitation_vector[self.state],
                                molecule=new_molecule,
                                scf=self.scf_drv)
            if self.verbose:
                print(adc.describe())
                print()
                print("Amplitudes:")
                print(adc.describe_amplitudes())
                print()
            if self.grad_drv.gradient_type == "analytical":
                print()
                print("Computing the "+self.adc_drv.method+" gradient analytically:")
                self.grad_drv.compute_ao(maxiter=self.maxiter)
            else:
                print()
                print("Computing the "+self.adc_drv.method+" gradient numerically:")
                self.grad_drv.compute_numerical_gradient(step=1e-5,
                        core=self.adc_drv.core, singlets=self.adc_drv.singlets,
                        triplets=self.adc_drv.triplets,
                        spin_flip=self.adc_drv.spin_flip,
                        v_index=self.state)
            gradient = self.grad_drv.get_gradient()
            print()
            print(" Gradient norm (Hartree/Bohr): %12.7f\n" % linalg.norm(gradient))
            print(" Gradient (Hartree/Bohr):\n")
            print(gradient)
            print()
            print()

            return {
                'energy': energy,
                'gradient': gradient.flatten(),
            }

