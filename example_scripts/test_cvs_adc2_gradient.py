import adcc
import pyscf
import adc_gradient
import adc_driver
import time
import numpy as np

"""This script calculates the analytical
   and numerical gradient at the CVS-ADC(2) theory level
"""

np.set_printoptions(precision=7, suppress=True)

# 1. PREPARE MOLECULE
folder = 'xyz_files/'
fileName = 'h2o.xyz' 
xyz = folder+fileName
moltxt = pyscf.gto.mole.fromfile(xyz, format="xyz")
basis = "def2-SVP" 
mol = pyscf.gto.M(atom=moltxt, basis=basis, unit="Angstrom")
    
# 2. RUN SCF to get the reference state necessary for adcc
hf = pyscf.scf.RHF(mol)
hf.conv_tol=1e-12
hf.kernel()

# 3. RUN MP2 through adc_driver
method = "cvs-adc2"
singlets = 5 # number of singlet excited states to include
v = 0 #index of the excitation vector 0 = first excited state
core = [0] # O1s; 

adc_drv = adc_driver.ADCDriver(hf, method=method,
							   singlets=singlets, core=core)
adc = adc_drv.compute() 

# 4. COMPUTE gradient analytically 
vector = adc.excitation_vector[v]
reference_state = adc.reference_state

start = time.time()
    
adc_grad = adc_gradient.gradient(reference_state, vector, order=method,
                                 backend="pyscf", molecule=mol,
                                 mo_coeff=hf.mo_coeff, scf=hf)

# comput_ao -- uses AO basis for the derivative integrals
# compute -- uses MO basis for the derivative integrals
#         -- spin-flip ADC possible with compute; 
#         -- otherwise not recommended as it can use a
#			 large amount of memory and computational time.		
adc_grad.compute_ao(maxiter=20, verbose=True)
stop1 = time.time()

# 5. COMPUTE gradient numerically
adc_grad_num = adc_gradient.gradient(reference_state, vector, order = method,
                                     backend="pyscf", molecule=mol,
                                     mo_coeff=hf.mo_coeff, scf=hf)

adc_grad_num.compute_numerical_gradient(step=1e-4, core=core, singlets=singlets,
										v_index = v)
stop2 = time.time()
print()
print()
print("The "+method+" numerical gradient is:")
print(adc_grad_num.matrix)
print("Computing the numerical gradient took %10.2f s." % (stop2 - stop1))
print()
print()
print("And the "+method+" analytical gradient is:")
print(adc_grad.matrix)
print("Computing the analytical gradient took %10.2f s." % (stop1 - start))

