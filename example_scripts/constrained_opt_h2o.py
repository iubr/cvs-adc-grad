import pyscf
import optimization_driver as opt_drv
import adc_driver
import adc_gradient
import numpy

folder = 'xyz_files/'
fileName = 'h2o_1.225_0.975.xyz'

xyz = folder+fileName
moltxt = pyscf.gto.mole.fromfile(xyz, format="xyz")
basis = "6-311++G**"
molecule = pyscf.gto.M(atom=moltxt, basis=basis, unit="Angstrom")

hf = pyscf.scf.RHF(molecule)
hf.conv_tol=1e-10
hf.kernel()

adc_method = "cvs-adc2x"
core = [0]
v = 1 # O1s->2b2
singlets = 5 # include 5 singlet excited states
adc_drv = adc_driver.ADCDriver(hf, method=adc_method, singlets=singlets, core=core,
                               tol=1e-5)
adc = adc_drv.compute()
grad_drv = adc_gradient.gradient(adc.reference_state, adc.excitation_vector[v],
                                 adc_method, backend='pyscf', molecule=molecule,
                                 scf=hf)

# Perform a constrained core-excited state optimization
# using contraints from the file "constraints.txt"
# In this case, the OH bond lengths are constrained to
# the values defined in the file;
# the optimization driver is a custom optimization driver
# for the python-based program geomeTRIC

opt = opt_drv.ADCOptimizationDriver(state=v, coordsys='tric', maxiter=100,
                                    constraints="constraints.txt")
coords, energy = opt.compute(molecule, hf, adc_drv, grad_drv)


print()
print()
print("And the optimized geometry with constraints is (Angstrom):")
print()
for i in grad_drv.atoms:
    txt = "%s %15.12f %15.12f %15.12f" % (molecule.atom_symbol(i), 
                                          coords[i,0],
                                          coords[i,1], 
                                          coords[i,2])
    print(txt)
print()
