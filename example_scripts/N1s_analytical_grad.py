import pyscf
import adc_driver
import adc_gradient
import numpy
import time

folder = 'xyz_files/'
files = ['06_nitroxyl'] 
cores = [ [1] ] 

basis_sets = ['def2-SVP']
adc_methods = ['cvs-adc1', 'cvs-adc2', 'cvs-adc2x']


singlets =  10
v = 0


i = 0
for f in files:
    xyz = folder + f + '.xyz'
    core = cores[i]
    moltxt = pyscf.gto.mole.fromfile(xyz, format="xyz")
    print(core)
    print(moltxt)

    for adc_method in adc_methods:
        for basis in basis_sets:
            py_mol = pyscf.gto.M(atom=moltxt, basis=basis, unit="Angstrom")
            hf = pyscf.scf.RHF(py_mol)
            hf.conv_tol=1e-12 
            hf.kernel()

            adc_drv = adc_driver.ADCDriver(hf, method=adc_method,
                                           singlets=singlets, core=core,
                                           tol=1e-6)
            adc = adc_drv.compute()

            print()
            print(adc.describe())
            print()

            grad_drv = adc_gradient.gradient(adc.reference_state,
                                             adc.excitation_vector[v],
                                             adc_method,
                                             backend='pyscf',
                                             molecule=py_mol,
                                             scf = hf)
            start = time.time()
            grad_drv.compute_ao()
            stop = time.time()
            gradient = grad_drv.get_gradient()
            print()
            print(f+": The ANALYTICAL "+adc_method+" / "+basis+" gradient:\n")
            print(gradient)
            print("It took %12.3f s\n" % (stop-start))

    i += 1
