import adcc
import pyscf
import adc_gradient
import adc_driver
import time
import numpy as np

"""This script calculates the analytical
   and numerical gradient at the MP2 theory level
"""

np.set_printoptions(precision=7, suppress=True)

# 1. PREPARE MOLECULE
folder = 'xyz_files/'
fileName = 'h2o.xyz' 
xyz = folder+fileName
moltxt = pyscf.gto.mole.fromfile(xyz, format="xyz")
basis = "STO-3G" #"cc-pVTZ" 
mol = pyscf.gto.M(atom=moltxt, basis=basis, unit="Angstrom")
    
# 2. RUN SCF to get the reference state necessary for adcc
hf = pyscf.scf.RHF(mol)
hf.conv_tol=1e-12
hf.kernel()

# 3. RUN MP2 through adc_driver
method = "gs_mp2" 

mp2_drv = adc_driver.ADCDriver(hf, method=method)
mp2 = mp2_drv.compute() 

# 4. COMPUTE gradient analytically 
reference_state = mp2.reference_state

start = time.time()
    
mp2_grad = adc_gradient.gradient(reference_state, order=method,
                                 backend="pyscf", molecule=mol,
                                 mo_coeff=hf.mo_coeff, scf=hf)

# comput_ao -- uses AO basis for the derivative integrals
# compute -- uses MO basis for the derivative integrals
#         -- spin-flip ADC possible with compute; 
#         -- otherwise not recommended as it can use a
#			 large amount of memory and computational time.		
mp2_grad.compute_ao(maxiter=20, verbose=True)
stop1 = time.time()

# 5. COMPUTE gradient numerically
mp2_grad_num = adc_gradient.gradient(reference_state, order = method,
                                 backend="pyscf", molecule=mol,
                                 mo_coeff=hf.mo_coeff, scf=hf)

mp2_grad_num.compute_numerical_gradient(step=1e-4)
stop2 = time.time()
print()
print()
print("The "+method+" numerical gradient is:")
print(mp2_grad_num.matrix)
print("Computing the numerical gradient took %10.2f s." % (stop2 - stop1))
print()
print()
print("And the "+method+" analytical gradient is:")
print(mp2_grad.matrix)
print("Computing the analytical gradient took %10.2f s." % (stop1 - start))

