#               
#                               ADC_GRADIENT
#     --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------

# load required packages
import adcc
import DMs
from adcc.functions import empty_like, zeros_like, ones_like
import numpy as np

class T_Multipliers:
    def __init__(self, refstate, vec, order):
        """Initializes the Lagrange multipliers corresponding to the Amplitude Response
        used to calculate CVS-ADC analytical gradients
        
        Parameters:
        ----------
        refstate: reference state from an adc calculation
        vec: excitation vector corresponding to the excited state of interest
        order: order of the adc matrix ("0"/"adc0", "cvs0/cvs-adc0","1"/"adc1",
               "cvs1/cvs-adc1","cvs2"/"cvs-adc2", "cvs2-x/cvs-adc2-x","gs_mp2"); 
                "adc3" not available yet
        block_typ: only block type "OOVV" or "CCVV" possible
        """
        #print("T MULTIPLIERS", order)
        if order in ["adc0", "adc1", "0", "1", "cvs0", "cvs-adc0",
                     "cvs1", "cvs-adc1"]:
            pass
        else:
            self.reference_state = refstate
            self.vector = vec
            self.order = order
            self.blocks = []
            self.matrix = {}
        

    def add_block(self, block_type):
        if block_type in ["CCVV", "OCVV", "OOVV"]:
            if block_type not in self.blocks:
                self.blocks.append(block_type)

                if block_type =="CCVV":
                    self.matrix[block_type] = (
                adcc.zeros_like(self.reference_state.eri("o2o2v1v1")).to_ndarray()
                                                )
                elif block_type =="OCVV":
                    self.matrix[block_type] = (
                adcc.zeros_like(self.reference_state.eri("o1o2v1v1")).to_ndarray()
                                                )
                elif block_type == "OOVV":
                    self.matrix[block_type] = ( 
                adcc.zeros_like(self.reference_state.eri("o1o1v1v1")).to_ndarray()
                                                )
        else:
            errtxt = " is not a valid block type for the amplitude"
            errtxt = " response Lagrange multipliers at "
            raise ValueError(block_type, errtxt, self.order)

       
    def compute_block(self, dm1=None, block_type="OOVV"):
        """Computes Lagrange multipliers required for amplitude response
        """
        if self.order in  ["0", "1", "adc0", "adc1", "cvs-adc0",
                           "cvs-adc1", "cvs0", "cvs1"]:
            pass
        elif self.order in ["cvs-adc2", "cvs2", "cvs2-singles", 
                            "cvs2-debug", "cvs2-amplitude", "cvs2x",
                            "cvs-adc2x", "cvs2x-debug"]:
            if block_type == "OOVV":
                #Construct and Compute pre-requisites 
                t2 = adcc.LazyMp(self.reference_state).t2("o1o1v1v1").to_ndarray()

                if dm1 == None:
                    dm1 = DMs.DM_1(self.reference_state, self.vector, "cvs-adc0")
                    dm1.compute_block("VV")

                if "VV" in dm1.blocks:
                    dm0 = dm1.matrix["VV"]
                else:
                    errtxt = "1PDM does not contain the required VV block."
                    raise ValueError(errtxt, dm1.blocks)

                oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
                foo = self.reference_state.fock("o1o1").to_ndarray()
                fvv = self.reference_state.fock("v1v1").to_ndarray()
                eo = np.diagonal(foo).copy()
                ev = np.diagonal(fvv).copy()
                nvirt = fvv.shape[0]
                nocc = foo.shape[0]
                eps = np.zeros(oovv.shape)

                evv = ev.reshape(-1,1) + ev
                eoo = eo.reshape(-1,1) + eo
                eov = eo.reshape(-1,1) - ev
                eoovv = ( - eoo.reshape((nocc,nocc,1,1)) 
                          + evv.reshape((1,1,nvirt,nvirt))
                         )
                sum_c = ( np.einsum('ijcb,ca->ijab',
                                     oovv, dm0, optimize=True)
                         -np.einsum('ijca,cb->ijab',
                                     oovv, dm0, optimize=True)
                              )
                sum_c /= eoovv

                self.matrix[block_type] = -0.25*sum_c #Did I get a sign wrong?
                                        

            elif block_type == "CCVV":
                #Construct and Compute pre-requisites 
                t2 = adcc.LazyMp(self.reference_state).t2("o2o2v1v1").to_ndarray()
                self.matrix[block_type] = 0.25*t2
            elif block_type == "OCVV":
                #Construct and compute pre-requisites
                t2 = adcc.LazyMp(self.reference_state).t2("o1o2v1v1").to_ndarray()
                self.matrix[block_type] = 0.25*t2               
            else:
                raise ValueError(block_type, "Not implemented at ", self.order)
        elif self.order in ["mp2"]:
            if block_type == "OOVV":
                #Construct and Compute pre-requisites 
                t2 = adcc.LazyMp(self.reference_state).t2("o1o1v1v1").to_ndarray()
                self.matrix[block_type] = 0.25*t2
            else:
                raise NotImplementedError(block_type, self.order) 
        else:
            raise NotImplementedError(self.order)
            
    def dot(self, mat, block):
        """Computes the dot product between the t-amplitude Lagrange multipliers
        and another matrix of the same dimensions. 

        Parameters:
        ----------
        mat: matrix with the same shape as the Lagrange multiplier block
        block: block of the Lagrange multiplier matrix
        """
        if mat.shape != self.matrix[block].shape:
            raise ValueError(mat.shape, self.matrix[block].shape, ": matrices have different dimensions!")
        else:
            E = np.einsum('pqrs,pqrs->', mat, self.matrix[block], optimize=True)
            return E


