#               
#                               ADC_GRADIENT
#     --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------

# TODO: REMMOVE all commented out code
# TODO: RUN yapf and flake8
# TODO: DELETE unused code

# load required packages
import adcc
import DMs
from adcc.functions import empty_like, zeros_like, ones_like
import numpy as np
from scipy.sparse import linalg
import time

#Missleading Name?
def cphf_solver(guess, dm1_ao, DM2_ao, conv_thresh=1e-5, maxiter=50):
    """
    General solver to solve A \lambda = B
    1. Transform guess from MO-to-AO:
    lambda_{\mu}{\nu}= \sum_ia C_{\mu i} \lambda_{ia} C_{\nu a}
    2. Compute B with guess; 
    3. Compare to target B;
    4. Improve guess
    5. Repeat until convergence threshold achieved;

    Parameters:
    -----------
    guess      : guess vector for lambda (in MO basis)
    dm1_ao     : 1PDM in AO basis
    DM2_ao     : 2PDM in AO basis
    conv_thresh: desired convergence threshold
    maxiter    : maximum number of iterations
    """
    pass

class OR_Multipliers:
    def __init__(self, refstate, vec=None, order=None):
        """Initializes the Lagrange multipliers 
        corresponding to Orbital Response
        used to calculate CVS-ADC analytical gradients
        
        Parameters:
        ----------
        refstate: reference state from an adc calculation
        vec: excitation vector corresponding to the excited state of interest
        order: order of the adc matrix 
        ("0"/"adc0","1"/"adc1","2"/"adc2","cvs0"/"cvs-adc0", 
        "cvs1"/"cvs-adc1", "cvs2"/"cvs-adc2"); 
        "adc2-x" and "adc3" not available yet
        "gs_mp2": multipliers for the GS MP2 gradient
        """
        self.reference_state = refstate
        if vec != None:
            self.vector = vec
        self.order = order
        self.blocks = []
        self.matrix = {}


    def add_block(self, block_type):
        """Adds a block of type block_type to the multiplier

        Parameters:
        ----------
        block_type: type of block: "CC", "OC", "CV", "OO", "OV", "VV"
        """
        if block_type in ["CC", "OO", "VV", "OC", "CO", "OV", "VO", "CV"]:
                if block_type not in self.blocks:
                    self.blocks.append(block_type)
    
                    if block_type == "CC":
                        self.matrix["CC"] = ( adcc.zeros_like(
                                             self.reference_state.fock("o2o2")
                                             ).to_ndarray()
                                            )

                    elif block_type == "OO":
                        self.matrix["OO"] = (adcc.zeros_like(
                                             self.reference_state.fock("o1o1")
                                             ).to_ndarray())

                    elif block_type == "VV":
                        self.matrix["VV"]=(adcc.zeros_like(
                                           self.reference_state.fock("v1v1")
                                           ).to_ndarray() )

                    elif block_type == "OV":
                        self.matrix["OV"] = (adcc.zeros_like(
                                             self.reference_state.eri(
                                                "o1v1o1v1")
                                             ).to_ndarray()[0,0])
                    elif block_type == "VO":
                        no = self.reference_state.fock(
                                            "o1o1").to_ndarray().shape[0]
                        nv = self.reference_state.fock(
                                            "v1v1").to_ndarray().shape[0]
                        self.matrix["VO"] = np.zeros((nv,no))

                    elif block_type == "OC":
                        self.matrix["OC"] = (adcc.zeros_like(
                                             self.reference_state.eri(
                                                "o1o2o1o2")
                                             ).to_ndarray()[0,0])
                    elif block_type == "CO":
                        self.matrix["CO"] = (adcc.zeros_like(
                                             self.reference_state.eri(
                                                "o2o1o2o1")
                                             ).to_ndarray()[0,0])

                    elif block_type == "CV":
                        self.matrix["CV"] = (adcc.zeros_like(
                                             self.reference_state.eri(
                                                "o2v1o2v1")
                                             ).to_ndarray()[0,0])
                else:
                    pass 
        else:
            errtxt = "Not a valid block type for the "
            errtxt += "orbital response Lagrange multipliers."
            raise ValueError(errtxt, block_type)

    def remove_blocks(self):
        """
        Deletes all the blocks in the density matrix
        """
        for block in self.blocks:
            del self.matrix[block]
        self.blocks=[]

    def set_to_zero(self, block):
        if block in self.blocks:
            self.matrix[block] = np.zeros(self.matrix[block].shape)
        else:
            errtxt = "Multiplier does notcntain the requested block."
            raise ValueError(errtxt, block, self.blocks)

    def reset(self, refstate, vec):
        """
        Resets the reference state and excitation vector

        Parameters:
        ----------
        refstate: new reference state
        vec: new excitation vector
        """
        self.reference_state = refstate
        self.vector = vec

    def dot(self, mat, block_type):
        """Computes the dot product between the block_type block
        of the Lagrange multiplier
        and another matrix of the same dimensions. 

        Parameters:
        ----------
        mat: matrix with the same shape as the requested block 
        of the Lagrange multiplier
        block_type: requested block of the Lagrange multipliers 
        ("CC", "OO", "VV", "OC", "OV", "CV")
        """
        if block_type not in self.block:
            raise ValueError(block_type, " does not exist.")
        else:
            if mat.shape != self.matrix[block_type].shape:
                errtxt ="Matrices have different dimensions!" 
                raise ValueError(errtxt, mat.shape, self.matrix[block_type].shape)
            else:
                E = np.einsum('pq,pq->',mat,self.matrix[block_type],optimize=True)
                return E

# TODO: remove lev option
class L_Multipliers(OR_Multipliers):
    def compute_blocks(self, blocks, dm1, DM2, max_iterations=50, lev=False,
                       verbose=False):
        """
        Computes all blocks in array blocks

        Parameters:
        ----------
        blocks: array continingthe names of the blocks to
                be computed
        dm1: one particle density matrix
        DM2: two particle density matrix
        max_iterations: number of maximum iteration to be used
                        in the conjugate gradient algorithm
                        (only for mixed blocks, e.g. OV, CV, etc.)
        lev: use equations from Levchenko thesis
        verbose: controls the printout level
        """
        if verbose:
            start = time.time()
        if lev:
            for block in blocks:
                self.compute_block_levchenko(block, dm1, DM2, max_iterations)
        else:
            for block in blocks:
                self.compute_block(block, dm1, DM2, max_iterations)
        if verbose:
            stop = time.time()
            print("LAMBDA   took %10.5f s." % (stop-start))

    def compute_block(self, block_type, dm1, DM2, max_iterations=50):
        """Computes the lambda Lagrange multipliers
        required for orbital response
        
        Parameters:
        ----------
        block_type: the block of the Lagrange multiplier to be computed 
        ("CC", "OC", "CV", "OO", "OV", "VV")
        dm1: one-particle density matrix
        DM2: two-particle density matrix
        """

        # Check if block exists; if not, try to initialize
        if block_type not in self.blocks:
                self.add_block(block_type)

        if self.order in ["adc0", "0"]: #, "cvs-adc0", "cvs0"]:
            if block_type == "OO":
                self.compute_0b_OO(dm1, DM2)
            elif block_type == "VV":
                self.compute_0b_VV(dm1, DM2)
            elif block_type == "OV":
                self.compute_0b_OV(dm1, DM2, max_iterations)
            else:
                errtxt = self.order + ": Unrecognized block type "
                raise ValueError(errtxt, block_type)

        elif self.order in ["cvs-adc0", "cvs0"]: #, "cvs-adc0", "cvs0"]:
            if block_type == "OO":
                self.compute_cvs0_OO(dm1, DM2)
            elif block_type == "CC":
                self.compute_cvs0_CC(dm1, DM2)
            elif block_type == "VV":
                self.compute_cvs0_VV(dm1, DM2)
            elif block_type == "OC":
                self.compute_cvs0_OC(dm1, DM2)
            elif block_type == "OV" or block_type == "CV":
                if "OV" in self.blocks and "CV" in self.blocks:
                    pass # since you already calculated them once
                else:
                    if "OV" not in self.blocks:
                        self.add_block("OV")
                    if "CV" not in self.blocks:
                        self.add_block("CV")
                    self.compute_cvs0_CV_OV(dm1, DM2, max_iterations)
            else:
                errtxt = self.order + ": Unrecognized block type "
                raise ValueError(errtxt, block_type)

        elif self.order in ["cvs-adc1", "cvs1"]:
            if block_type == "CC":
                self.compute_cvs1_CC(dm1, DM2)

            elif block_type == "OO":
                self.compute_cvs1_OO(dm1, DM2)

            elif block_type == "VV":
                self.compute_cvs1_VV(dm1, DM2)

            elif block_type == "OC":
                self.compute_cvs1_OC(dm1, DM2)

            # Probably need to rethink this one...:
            elif block_type == "OV" or block_type == "CV":
                if "OV" in self.blocks and "CV" in self.blocks:
                    pass # since you already calculated them once
                else:
                    if "OV" not in self.blocks:
                        self.add_block("OV")
                    if "CV" not in self.blocks:
                        self.add_block("CV")
                    self.compute_cvs1_CV_OV(dm1, DM2, max_iterations)
            else:
                errtxt = self.order + ": Unrecognized block type "
                raise ValueError(errtxt, block_type)

        elif self.order in ["adc1", "1"]:
            if block_type == "OO":
                self.compute_1_OO(dm1, DM2)

            elif block_type == "VV":
                self.compute_1_VV(dm1, DM2)

            elif block_type == "OV":
                self.compute_1_OV(dm1, DM2, max_iterations)

            else:
                errtxt = self.order + ": Unrecognized block type."
                raise ValueError(errtxt, block_type)

        elif self.order in ["cvs-adc2", "cvs2"]:
            if block_type == "CC":
                self.compute_cvs2_CC(dm1, DM2)

            elif block_type == "OO":
                self.compute_cvs2_OO(dm1, DM2)

            elif block_type == "VV":
                self.compute_cvs2_VV(dm1, DM2)

            elif block_type == "OC":
                self.compute_cvs2_OC(dm1, DM2)

            elif block_type == "OV" or block_type == "CV":
                if "OV" not in self.blocks:
                    self.add_block("OV")
                if "CV" not in self.blocks:
                    self.add_block("CV")
                self.compute_cvs2_CV_OV(dm1, DM2, max_iterations)
            else:
                errtxt = self.order + ": Unrecognized block type."
                raise ValueError(errtxt, block_type)

        elif self.order in ["cvs-adc2x", "cvs2x"]:
            if block_type == "CC":
                self.compute_cvs2x_CC(dm1, DM2)

            elif block_type == "OO":
                self.compute_cvs2x_OO(dm1, DM2)

            elif block_type == "VV":
                self.compute_cvs2x_VV(dm1, DM2)

            elif block_type == "OC":
                self.compute_cvs2x_OC(dm1, DM2)

            elif block_type == "OV" or block_type == "CV":
                if "OV" not in self.blocks:
                    self.add_block("OV")
                if "CV" not in self.blocks:
                    self.add_block("CV")
                self.compute_cvs2x_CV_OV(dm1, DM2, max_iterations)

            else:
                errtxt = self.order + ": Unrecognized block type."
                raise ValueError(errtxt, block_type)

        elif self.order in ["gs_mp2"]:
            if block_type == "OO":
                self.compute_mp2_OO(dm1, DM2)
            elif block_type == "VV":
                self.compute_mp2_VV(dm1, DM2)
            elif block_type == "OV":
                self.compute_mp2_OV(dm1, DM2, max_iterations)
            elif block_type == "VO":
                self.compute_mp2_VO(dm1, DM2, max_iterations)
            else:
                errtxt = self.order + ": Unrecognized block type."
                raise ValueError(errtxt, block_type)
        else:
            raise ValueError(self.order, "Not implemented.")

    # TODO: delete ths routine
    def compute_block_levchenko(self, block_type, dm1, DM2, max_iterations=50):
        """Computes the lambda Lagrange multipliers
        required for orbital response;
        uses the equations derived by Levchenko -- see PhD thesis
        
        Parameters:
        ----------
        block_type: the block of the Lagrange multiplier to be computed 
        ("CC", "OC", "CV", "OO", "OV", "VV")
        dm1: one-particle density matrix
        DM2: two-particle density matrix
        """

        #Check if block exists; if not, try to initialize
        if block_type not in self.blocks:
                self.add_block(block_type)

        if self.order in ["adc0", "0", "adc1", "1"]: #, "cvs-adc0", "cvs0"]:
            if block_type == "OO":
                self.compute_levchenko_OO(dm1, DM2)
            elif block_type == "VV":
                self.compute_levchenko_VV(dm1, DM2)
            elif block_type == "OV":
                self.compute_levchenko_OV(dm1, DM2, max_iterations)
            else:
                errtxt = self.order + ": Unrecognized block type "
                raise ValueError(errtxt, block_type)

    # TODO: delete all routines where multipliers are zero!
    def compute_cvs1_CC(self, dm1, DM2):
        #1. Compute pre-requisites:
        #1.1 Density matrices:

        if "CC" in dm1.blocks:
            dm0_cc = dm1.matrix["CC"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required CC block. "
            raise ValueError(errtxt, dm1.blocks)

        #pre_fact = 0.25
        if "CVCV" in DM2.blocks:
            Dm1_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block. "
            raise ValueError(errtxt, DM2.blocks) 
        
        #1.2 CC Fock and CVCV eri matrices: 
        fcc = self.reference_state.fock("o2o2").to_ndarray() 
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()

        #2. Compute multipliers:
        ncore = cvcv.shape[0]

        self.matrix["CC"] = ( np.einsum('IbKc,JbKc->IJ',
                                        Dm1_cvcv,cvcv,optimize=True)
                            - np.einsum('JbKc,IbKc->IJ',
                                        Dm1_cvcv,cvcv,optimize=True)
                            )
       
        for I in range(ncore):
            for J in range(ncore):
                if abs(fcc[I,I]-fcc[J,J]) > 1e-10:
                    self.matrix["CC"][I,J] /= fcc[I,I]-fcc[J,J] 
                    self.matrix["CC"][I,J] += -dm0_cc[I,J] 
                else:
                    self.matrix["CC"][I,J] = 0.0



    def compute_cvs1_OO(self, dm1, DM2):
        pass
        # trivially zero. Nothing to compute.

    def compute_cvs1_VV(self, dm1, DM2):
        # 1. Compute pre-requisites:
        # 1.1 Density matrices:
        if "VV" in dm1.blocks:
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt = "the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        if "CVCV" in DM2.blocks:
            Dm1_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block. "
            raise ValueError(errtxt, DM2.blocks)
        
        # 1.2 CC Fock and CVCV eri matrices: 
        fvv = self.reference_state.fock("v1v1").to_ndarray() 
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()

        # 2. Compute multipliers:
        # ncore = cvcv.shape[0] 
        nvirt = cvcv.shape[1]

        self.matrix["VV"] = (np.einsum('JaKc,JbKc->ab',Dm1_cvcv,
                                        cvcv,optimize=True)
                            -np.einsum('JbKc,JaKc->ab',
                                        Dm1_cvcv,cvcv,optimize=True)
                            )
       
        for a in range(nvirt):
            for b in range(nvirt):
                if abs(fvv[a,a]-fvv[b,b]) > 1e-10:
                    self.matrix["VV"][a,b] /= (fvv[a,a]-fvv[b,b]) 
                    self.matrix["VV"][a,b] += -dm0_vv[a,b] 
                else:
                    self.matrix["VV"][a,b] = 0.0

    def compute_cvs1_OC(self, dm1, DM2):
        # pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()

        eoc = foo.diagonal().reshape(-1,1) - fcc.diagonal() 

        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2PDM does not contain the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        self.matrix["OC"] = -np.einsum("JbKc,ibKc->iJ",
                                       DM_cvcv, ovcv,
                                       optimize=True)

        self.matrix["OC"] /= eoc

    def compute_cvs1_CV_OV(self, dm1, DM2, max_iterations):
        # This is the correct method to compute the CV and OV blocks!
        # pre-requisites:
        fcc = self.reference_state.fock("o2o2").to_ndarray() #
        foo = self.reference_state.fock("o1o1").to_ndarray() #
        fvv = self.reference_state.fock("v1v1").to_ndarray() #

        ncore = fcc.shape[0]
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]
        nfull = ncore + nocc

        ecv = fcc.diagonal().reshape(-1,1) - fvv.diagonal()
        eov = foo.diagonal().reshape(-1,1) - fvv.diagonal()
        ecov = np.zeros((nfull, nvirt))
        ecov[:ncore,:] = ecv
        ecov[ncore:,:] = eov 

        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray() #
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray() #
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray() #
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray() #
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray() #
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray() #
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray() #
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray() #
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray() #
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray() #
        ccov = self.reference_state.eri("o2o2o1v1").to_ndarray() # 
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()            

        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required CC and VV blocks."
            raise ValueError(ertxt, dm1.blocks)

        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2PDM does not contain the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OC" in self.blocks:
            loc = self.matrix["OC"]
        else:
            print("WARNING! Computing OC")
            self.compute_block("OC", dm1, DM2)
            loc = self.matrix["OC"]

        rhs_fv = np.zeros((ncore+nocc, nvirt))

        rhs_fv[:ncore,:] = (  np.einsum("JK,KIJa->Ia", dm_cc, cccv)
                             -np.einsum("bc,Icba->Ia", dm_vv, cvvv)
                             +np.einsum("kJ,kIJa->Ia", loc, occv)
                             +np.einsum("kJ,JIka->Ia", loc, ccov)
                             +np.einsum("JaKb,IJKb->Ia", DM_cvcv, cccv)
                             +np.einsum("IbJc,Jcab->Ia", DM_cvcv, cvvv)
                            )
        rhs_fv[ncore:nfull,:] = ( -np.einsum("JK,iKJa->ia", dm_cc, occv)
                                 -np.einsum("bc,icba->ia", dm_vv, ovvv)
                                 +np.einsum("kJ,kiJa->ia", loc, oocv)
                                 -np.einsum("kJ,iJka->ia", loc, ocov)
                                 +np.einsum("JaKb,iJKb->ia", DM_cvcv, occv)
                                )

        self.iter_count = 0
        def matvec(x):
            """
            Routine to carry out the Ax matrix vector product
            to be used by the conjugate gradient solver.     
            """
            lfv = x.reshape(nfull,nvirt)
            lcv = lfv[:ncore,:]
            lov = lfv[ncore:,:]
            Alcv = (  np.einsum('Jb,IbJa->Ia', lcv, cvcv) #
                    - np.einsum('Jb,IJab->Ia', lcv, ccvv) #
                    + np.einsum('jb,jaIb->Ia', lov, ovcv) 
                    + np.einsum('jb,jIab->Ia', lov, ocvv)
                    )
                     
            Alov = (  np.einsum("jb,ibja->ia", lov, ovov) #
                    - np.einsum("jb,ijab->ia", lov, oovv) #
                    + np.einsum("Jb,ibJa->ia", lcv, ovcv)
                    - np.einsum("Jb,iJab->ia", lcv, ocvv)
                    )
            Ax = np.zeros((nfull,nvirt))
            Ax[:ncore,:] = Alcv
            Ax[ncore:,:] = Alov 
            Ax += lfv*ecov
            self.iter_count += 1
 
            return Ax.reshape(nfull*nvirt)

        def precond(x):
            """
            Routine to define the preconditioner for conjugate gradient
            """
            M = x.reshape(nfull,nvirt) / ecov
            return M.reshape(nfull*nvirt)

        A = linalg.LinearOperator((nfull*nvirt, nfull*nvirt),
                                   matvec=matvec)
        M = linalg.LinearOperator((nfull*nvirt, nfull*nvirt),
                                   matvec=precond)

        rhs = np.reshape(rhs_fv, (nfull*nvirt))
        guess = (rhs_fv / ecov).reshape(nfull*nvirt)


        lfv, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=M,
                           callback=None, atol=None)

        if w == 0:
            print("Conjugate gradient converged in %d iterations." % self.iter_count)
            l_fv = np.reshape(lfv, (nfull, nvirt))
            self.matrix["CV"] = l_fv[:ncore,:]
            self.matrix["OV"] = l_fv[ncore:nfull,:]
        else:
            raise Warning("Conjugate gradient did not converge.", w)

    #TODO: delete this routine: OV an CV must be computed togther!!
    def compute_cvs1_OV(self, dm1, DM2, max_iterations):
        # WARNING: This is an approximate method to 
        # compute the CVS-ADC(1) OV block of the lambda multipliers.
        # Introduces a non-negligible error!
        #1. Compute pre-requisites:
        #1.1 OO Fock, VV Fock and OCCV, OVOV, OOVV, OVVV eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()

        #1.2 Density matirces:
        if "VV" in dm1.blocks and "CC" in dm1.blocks:
            dm0_vv = dm1.matrix["VV"]
            dm0_cc = dm1.matrix["CC"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "required CC and VV blocks. "
            raise ValueError(errtxt, dm1.blocks)

        #pre_fact = 0.25
        if "CVCV" in DM2.blocks:
            Dm1_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block. "
            raise ValueError(errtxt, DM2.blocks)

        #1.3 Lagrange multipliers:
        #Compute them if they have not yet been computed
        if "CC" not in self.blocks:
            #print("CC: Required block not initialized. Initializing now...")
            self.add_block("CC")
            #print("CC: Computing...")
            self.compute_block("CC", dm1, DM2)
        lcc = self.matrix["CC"]
        
        if "VV" not in self.blocks:
            #print("VV: Required block not initialized. Initializing now...")
            self.add_block("VV")
            #print("VV: Computing...")
            self.compute_block("VV", dm1, DM2)
        lvv = self.matrix["VV"]

        #2.1 Prepare A matrix (solving for l, l A = rhs)
        #2.2 right hand side (rhs)
        #2.3 and guess
        ncore = dm0_cc.shape[0]
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]

        AOVOV = np.ones((nocc,nvirt,nocc,nvirt))
        rhs_ov = np.zeros((nocc,nvirt))
        guess_ov = np.zeros((nocc, nvirt))

        A = np.zeros((nocc*nvirt, nocc*nvirt))
        rhs = np.zeros((nocc*nvirt))
        guess = np.zeros((nocc*nvirt))

        AOVOV = ( np.einsum('iajb,ibja->iajb', AOVOV, ovov, optimize=True) 
                - np.einsum('iajb,ijab->iajb', AOVOV, oovv, optimize=True)
                )
        for i in range(nocc):
            for a in range(nvirt):
                AOVOV[i,a,i,a] = AOVOV[i,a,i,a] + foo[i,i]-fvv[a,a]
                #+ (ovov[i,a,i,a] - oovv[i,i,a,a]) 

        rhs_ov = ( -1.0*np.einsum('JK,iKJa->ia', dm0_cc+lcc,
                                   occv, optimize=True) 
                - np.einsum('bc,icba->ia', dm0_vv+lvv, ovvv, optimize=True) 
                + np.einsum('JaKb,iJKb->ia', Dm1_cvcv, occv, optimize=True)
                )

        guess_ov = -1.0*rhs_ov

        for i in range(nocc):
            for a in range(nvirt):
                if abs(foo[i,i]-fvv[a,a]) > 1e-10:
                    guess_ov[i,a] = guess_ov[i,a]/(foo[i,i]-fvv[a,a]) 
                


        A = np.reshape(AOVOV,(nocc*nvirt,nocc*nvirt))
        guess = np.reshape(guess_ov, (nocc*nvirt))
        rhs = np.reshape(rhs_ov, (nocc*nvirt))

        # Compute Lagrange multipliers
        # (currently using the conjugate gradient solver of scipy)
        lov, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8, 
                           maxiter=max_iterations, M=None, 
                           callback=None, atol=None)

        #4. If the solution is found, save to self.matrix
        if w == 0:
            self.matrix["OV"] = np.reshape(lov,(nocc,nvirt))
        else:
            print("Warning: conjugate gradient did not converge.")

    def compute_cvs1_CV(self, dm1, DM2, max_iterations):
        pass

    def compute_0_OO(self, dm1, DM2):
        pass

    def compute_0_VV(self, dm1, DM2):
        pass

    def compute_0_OV(self, dm1, DM2, max_iterations):
        start = time.time()
        # 1. Compute pre-requisites:
        # 1.1 OO Fock, VV Fock and OVOV eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        if "OOOO" in DM2.blocks:
            Dm2_oooo = DM2.matrix["OOOO"]
        else:
            raise ValueError("missing OOOO")
        
        # 2.1 Prepare A matrix (solving for l, l A = rhs)
        # 2.2 right hand side (rhs)
        # 2.3 guess 
        AOVOV = np.ones(ovov.shape)
        a_ovov = np.zeros(ovov.shape)
        a2_ovov = np.zeros(ovov.shape)
        rhs_ov = np.zeros(self.matrix["OV"].shape)
        b_ov = np.zeros(rhs_ov.shape)
        guess_ov = np.zeros(self.matrix["OV"].shape)
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]

        A = np.zeros((nocc*nvirt, nocc*nvirt))
        rhs = np.zeros((nocc*nvirt,1))
        guess = np.zeros((nocc*nvirt,1))

        # these are supposed to be the elements that multiply lambda_jb
        AOVOV = ( np.einsum('iakb,kaib->iakb', AOVOV, ovov, optimize=True)
                 +np.einsum('iakb,ikba->iakb', AOVOV, oovv, optimize=True) 
                ) 
        a_ovov = ovov.transpose(2,1,0,3) + oovv.transpose(0,3,1,2)
        for i in range(nocc):
            for a in range(nvirt):
                for j in range(nocc):
                    for b in range(nvirt):
                        a2_ovov[i,a,j,b]=ovov[j,a,i,b]+oovv[i,j,b,a]
                        if a==b and i==j:
                            a2_ovov[i,a,j,b] += foo[i,i] - fvv[b,b]
                
    
        diff = a2_ovov - a_ovov
        # print("Here is your difference:")
        # print(np.amax(diff),np.amin(diff))
        # print()
        #these are supposed to be the elements that multiply lambda_ia
        for i in range(nocc):
            for a in range(nvirt):
                AOVOV[i,a,i,a] += foo[i,i]-fvv[a,a]
                a_ovov[i,a,i,a] += foo[i,i]-fvv[a,a] 
        diff = a2_ovov - a_ovov
        # print("Here is your difference:")
        # print(np.amax(diff),np.amin(diff))
        # print()

        rhs_ov = -0.5*np.einsum('iklm,lmka->ia', Dm2_oooo, ooov, optimize=True)
        b_ov = np.einsum('ikka->ia',ooov, optimize=True)
        diff = rhs_ov - b_ov
        print("Here is your difference, RHS:")
        print(np.amax(diff),np.amin(diff))
        print() 

        guess_ov = rhs_ov.copy() 

        #for i in range(nocc):
        #    for a in range(nvirt):
        #        if abs(foo[i,i]-fvv[a,a]) > 1e-10:
        #            guess_ov[i,a] = guess_ov[i,a]/(foo[i,i]-fvv[a,a]) 
                
        A = np.reshape(AOVOV,(nocc*nvirt,nocc*nvirt))

        print("A before")
        print(AOVOV)
        print()
        print("A after:")
        print(A)
        print()
        print("RHS before:")
        print(rhs_ov)
        print()
        print("RHS after:")
        guess = np.reshape(guess_ov, (nocc*nvirt))
        rhs = np.reshape(rhs_ov, (nocc*nvirt))
        print(rhs)
        print()
        print(A.shape, guess.shape, rhs.shape)
        #Compute Lagrange multipliers
        #(currently using the conjugate gradient solver of scipy)
        #Think about what the tolerance here should be considering
        #the convergence tolerance used by the adc run you are using.
        l_ov, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=None,
                           callback=None, atol=None)

        sol1 = np.matmul(A,lov) - rhs
        print("CHECK1:")
        print(sol1)
        print()
        #4. If the solution is found, save to self.matrix
        if w == 0:
            self.matrix["OV"] = np.reshape(l_ov,(nocc,nvirt))
        else:
            print("Warning: conjugate gradient did not converge.")

        ##Check solution
        # TODO: del lov
        lov = self.matrix["OV"] #reshape(nocc,nvirt)
        print("Shapes:",lov.shape, AOVOV.shape)
        sov = np.zeros(lov.shape) 
        print("SOV shape:",sov.shape)
        sol = np.zeros(lov.shape)
        ones = np.ones(lov.shape)
        print(lov.shape, foo.shape, fvv.shape)
        for i in range(nocc):
            for a in range(nvirt):
                #sov[i,a]=AOVOV[i,a,i,a]*lov[i,a]
                sol[i,a] = lov[i,a]*(foo[i,i]-fvv[a,a])
        sov =  np.einsum('iajb,jb->ia',AOVOV, lov, optimize=True)
        print()
        print("CHECK2:")
        print(sov-rhs_ov)
        print()
        sol += ( np.einsum('kb,kaib->ia', lov, ovov, optimize=True)
                +np.einsum('kb,ikba->ia', lov, oovv, optimize=True)
                +0.5*np.einsum('iklm,lmka->ia', Dm2_oooo,ooov, optimize=True)
                )
        print()
        print("Checking solution...")
        print(sol)
        print()

        #5. Clean up...
        #TODO: delete del A, guess, rhs, AOVOV, guess_ov, rhs_ov
        #TODO: delete: del oovv, ooov, ovov, foo, fvv

    #### MP(2) ####
    def compute_mp2_OO(self, dm1, DM2):
        pass
        #if "OO" in dm1.blocks:
        #    dm_oo = dm1.matrix["OO"]
        #else:
        #    errtxt = "1DM does not contain the required OO block."
        #    raise ValueError(errtxt)
        #self.matrix["OO"] = dm_oo#.copy()

    def compute_mp2_VV(self, dm1, DM2):
        pass
        #if "VV" in dm1.blocks:
        #    dm_vv = dm1.matrix["VV"]
        #else:
        #    errtxt = "1DM does not contan the required VV block."
        #    raise ValueError(errtxt)
        #self.matrix["VV"] = dm_vv#.copy()        

    def compute_mp2_OV(self, dm1, DM2, max_iterations):
        #Get pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        eov = foo.diagonal().reshape(-1,1) - fvv.diagonal()
    
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()

        #DMs:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm1_oo = dm1.matrix["OO"]
            dm1_vv = dm1.matrix["VV"]
        else:
            errtxt = "1DM does not contain required"
            errtxt += " OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        if "OOVV" in DM2.blocks:
            DM2_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2DM does not contain required"
            errtxt += " OOVV block."
            raise ValueError(errtxt, DM2.blocks)
    
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]

        rhs_ov = np.zeros((nocc, nvirt))

        rhs_ov = ( np.einsum('jk,kija->ia',dm1_oo,ooov)
                  +np.einsum('bc,icab->ia',dm1_vv,ovvv)
                  -0.5*np.einsum('jkab,jkib->ia',DM2_oovv,ooov)
                  -0.5*np.einsum('ijbc,jabc->ia',DM2_oovv,ovvv)  
                    )

        self.iter_count = 0
        def matvec(x):
            """
            Function to carry out the Ax matrix vector product
            to be used by the conjugate gradient solver.     
            """
            lov = x.reshape(nocc,nvirt)
            Ax = (lov*eov + np.einsum('jb,ibja->ia', lov, ovov)
                  - np.einsum('jb,ijab->ia', lov, oovv)
                 )
            self.iter_count += 1
            return Ax.reshape(nocc*nvirt)

        def precond(x):
            """
            Function to define the preconditioner for conjugate gradient
            """
            M = x.reshape(nocc,nvirt) / eov
            return M.reshape(nocc*nvirt)
        

        A = linalg.LinearOperator((nocc * nvirt, nocc * nvirt),
                                   matvec=matvec)
        M = linalg.LinearOperator((nocc * nvirt, nocc * nvirt),
                                   matvec=precond)
        
        rhs = np.reshape(rhs_ov, (nocc*nvirt))
        guess = (rhs_ov / eov).reshape(nocc*nvirt)

        lov, w =  linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=M,
                           callback=None, atol=None)
        if w==0:
            print("MP2 CG converged in %d iterations." % self.iter_count)
            self.matrix["OV"] = np.reshape(lov, (nocc, nvirt))
        else:
            raise Warning("Conjugate gradient did not converge.", w)

    #implemented according to CPL 166 275 (1990)
    def compute_mp2_VO(self, dm1, DM2, max_iterations):
        #Get pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()

        #DMs:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm1_oo = dm1.matrix["OO"]
            dm1_vv = dm1.matrix["VV"]
        else:
            errtxt = "1DM does not contain required"
            errtxt += " OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        if "OOVV" in DM2.blocks:
            DM2_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2DM does not contain required"
            errtxt += " OOVV block."
            raise ValueError(errtxt, DM2.blocks)
    
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]
        AVOVO = np.ones((nvirt,nocc,nvirt,nocc))
        rhs_vo = np.zeros((nvirt, nocc))

        AVOVO = ( np.einsum('aibj,ijab->aibj', AVOVO, oovv, optimize=True)
                 -np.einsum('aibj,ibja->aibj', AVOVO, ovov, optimize=True) 
                )

        for a in range(nvirt):
            for i in range(nocc):
                AVOVO[a,i,a,i] += fvv[a,a]-foo[i,i]

        rhs_vo = ( np.einsum('jk,kija->ai',dm1_oo,ooov)
                  +np.einsum('bc,icab->ai',dm1_vv,ovvv)
                  -0.5*np.einsum('ijbc,jabc->ai',DM2_oovv,ovvv)
                  -0.5*np.einsum('jkab,jkib->ai',DM2_oovv,ooov)  
                    ) 
        
        A = np.reshape(AVOVO, (nocc*nvirt, nocc*nvirt))
        rhs = np.reshape(rhs_vo, (nocc*nvirt))
        guess = rhs.copy()

        lvo, w =  linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=None,
                           callback=None, atol=None)
        if w==0:
            print("VO CONVERGED", w)
            self.matrix["VO"] = np.reshape(lvo, (nvirt, nocc))
        else:
            raise Warning("Conjugate gradient did not converge.", w)

    #### CVS-ADC(0) ####
    def compute_cvs0_CC(self, dm1, DM2):
        pass

    def compute_cvs0_OO(self, dm1, DM2):
        pass

    def compute_cvs0_VV(self, dm1, DM2):
        pass

    # In CVS-ADC0 these are zero?
    def compute_cvs0_OC(self, dm1, DM2):
        pass
    

    # TODO: delee this routine: CV and OV must be computed together!
    def compute_cvs0_CV(self, dm1, DM2, max_iterations):
        # WARNING!: This is an approximate method! 
        # using this will introduce a small error to your gradient!!
        #pre-requisites:
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        cvov = self.reference_state.eri("o2v1o1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()

        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required CC and VV blocks."
            raise ValueError(errtxt)

        if "OV" in self.blocks:
            lov = self.matrix["OV"]
        else:
            print("OV block not computed yet!")
            self.add_block("OV")
            #self.compute_cvs0_OV(dm1, DM2, max_iterations)
        
        ncore = fcc.shape[0]
        nvirt = fvv.shape[0]

        A_CVCV = np.ones((ncore,nvirt,ncore,nvirt))
        rhs_cv = np.zeros((ncore,nvirt))

        A_CVCV = ( np.einsum("IaJb,Ibja->IaJb", A_CVCV, cvov)
                  +np.einsum("IaJb,jIab->IaJb", A_CVCV, ocvv)
                    )

        for I in range(ncore):
            for a in range(nvirt):
                A_CVCV[I,a,I,a] += fcc[I,I]-fvv[a,a]

        rhs_cv = ( -np.einsum("KL,IKLa->Ia", dm_cc, cccv)
                   +np.einsum("bc,Icab->Ia", dm_vv, cvvv)
                   -np.einsum("jb,Ibja->Ia", lov, cvov)
                   -np.einsum("jb,jIab->Ia", lov, ocvv)
                    )
        A = np.reshape(A_CVCV, (ncore*nvirt,ncore*nvirt))
        rhs = np.reshape(rhs_cv, (ncore*nvirt))
        guess = rhs.copy()

        lcv, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=None,
                           callback=None, atol=None)

        if w == 0:
            self.matrix["CV"] = np.reshape(lcv,(ncore,nvirt))
        else:
            raise Warning("Conjugate gradient did not converge.", w)
         

    def compute_cvs0_CV_OV(self, dm1, DM2, max_iterations):
        # This is the correct method to compute the CV and OV blocks!
        #pre-requisites:
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()

        ncore = fcc.shape[0]
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]
        nfull = ncore + nocc

        ecv = fcc.diagonal().reshape(-1,1) - fvv.diagonal()
        eov = foo.diagonal().reshape(-1,1) - fvv.diagonal()
        ecov = np.zeros((nfull,nvirt))
        ecov[:ncore,:] = ecv
        ecov[ncore:,:] = eov        

        cvov = self.reference_state.eri("o2v1o1v1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()             

        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required CC and VV blocks."
            raise ValueError(ertxt, dm1.blocks)

        rhs_fv = np.zeros((ncore+nocc, nvirt))

        rhs_fv[:ncore,:] = ( -np.einsum("KL,ILKa->Ia", dm_cc, cccv)
                             +np.einsum("bc,Icab->Ia", dm_vv, cvvv)
                            )
        rhs_fv[ncore:nfull,:] =  ( -np.einsum("KL,iLKa->ia", dm_cc, occv)
                                 +np.einsum("bc,icab->ia", dm_vv, ovvv)
                                )

        self.iter_count = 0
        def matvec(x):
            """
            Function to carry out the Ax matrix vector product
            to be used by the conjugate gradient solver.     
            """
            lfv = x.reshape(nfull,nvirt)
            lcv = lfv[:ncore,:]
            lov = lfv[ncore:,:]
            Alcv = (  np.einsum('Jb,IbJa->Ia', lcv, cvcv) #
                    - np.einsum('Jb,IJab->Ia', lcv, ccvv) #
                    + np.einsum('jb,jaIb->Ia', lov, ovcv) 
                    + np.einsum('jb,jIab->Ia', lov, ocvv)
                    )
                     
            Alov = (  np.einsum("jb,ibja->ia", lov, ovov) #
                    - np.einsum("jb,ijab->ia", lov, oovv) #
                    + np.einsum("Jb,ibJa->ia", lcv, ovcv)
                    - np.einsum("Jb,iJab->ia", lcv, ocvv)
                    )
            Ax = np.zeros((nfull,nvirt))
            Ax[:ncore,:] = Alcv
            Ax[ncore:,:] = Alov 
            Ax += lfv*ecov
            self.iter_count += 1
 
            return Ax.reshape(nfull*nvirt)

        def precond(x):
            """
            Function to define the preconditioner for conjugate gradient
            """
            M = x.reshape(nfull,nvirt) / ecov
            return M.reshape(nfull*nvirt)

        A = linalg.LinearOperator((nfull*nvirt, nfull*nvirt),
                                   matvec=matvec)
        M = linalg.LinearOperator((nfull*nvirt, nfull*nvirt),
                                   matvec=precond)

        rhs = np.reshape(rhs_fv, (nfull*nvirt))
        guess = (rhs_fv / ecov).reshape(nfull*nvirt)

        lfv, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=M,
                           callback=None, atol=None)

        if w == 0:
            print("Conjugate gradient converged in %d iterations." % self.iter_count)
            l_fv = np.reshape(lfv, (nfull, nvirt))
            self.matrix["CV"] = l_fv[:ncore,:]
            self.matrix["OV"] = l_fv[ncore:nfull,:]
        else:
            raise Warning("Conjugate gradient did not converge.", w)

    #TODO delete this routine: CV and OV must be computed together 
    def compute_cvs0_OV(self, dm1, DM2, max_iterations):
        # WARNING! This is an approximate method to compute the OV block
        # and introduces a small error
        #1. Compute pre-requisites:
        #1.1 OO Fock, VV Fock and OVOV eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
    
        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The one particle density matrix "
            errtxt += "does not contain the required CC and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #2.1 Prepare A matrix (solving for l, l A = rhs)
        #2.2 right hand side (rhs)
        #2.3 guess 
        ###TODO: delete the code bits which are commented out by "###"
        AOVOV = np.ones(ovov.shape)
        rhs_ov = np.zeros(self.matrix["OV"].shape)
        #guess_ov = np.zeros(self.matrix["OV"].shape)
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]

        A = np.zeros((nocc*nvirt, nocc*nvirt))
        rhs = np.zeros((nocc*nvirt,1))
        guess = np.zeros((nocc*nvirt,1))

        #these are supposed to be the elements that multiply lambda_jb
        AOVOV = ( np.einsum('iajb,ibja->iajb', AOVOV, ovov, optimize=True)
                 -np.einsum('iajb,ijab->iajb', AOVOV, oovv, optimize=True) 
                )
        #these are supposed to be the elements that multiply lambda_ia
        for i in range(nocc):
            for a in range(nvirt):
                AOVOV[i,a,i,a] += foo[i,i]-fvv[a,a]

        rhs_ov = ( -np.einsum('JK,iKJa->ia', dm_cc, occv, optimize=True)
                   +np.einsum('bc,icab->ia', dm_vv, ovvv, optimize=True)
                  )

        A = np.reshape(AOVOV,(nocc*nvirt,nocc*nvirt))
        rhs = np.reshape(rhs_ov, (nocc*nvirt))
        guess = rhs.copy()

        #Compute Lagrange multipliers
        #(currently using the conjugate gradient solver of scipy)
        #Think about what the tolerance here should be considering
        #the convergence tolerance used by the adc run you are using.
        lov, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=None,
                           callback=None, atol=None)

        if w == 0:
            self.matrix["OV"] = np.reshape(lov,(nocc,nvirt))
        else:
            raise Warning("Conjugate gradient did not converge.", w)

        l_ov = self.matrix["OV"] 
        sol = np.zeros(lov.shape)

        sol = (  np.einsum('jb,ibja->ia', l_ov, ovov, optimize=True)
                -np.einsum('jb,ijab->ia', l_ov, oovv, optimize=True)
                +np.einsum('JK,iKJa->ia', dm_cc, occv, optimize=True)
                +np.einsum('bc,icba->ia', dm_vv, ovvv, optimize=True)
                )
        
        for i in range(nocc):
            for a in range(nvirt):
                sol[i,a] += l_ov[i,a]*(foo[i,i]-fvv[a,a])
        print()
        print("Checking solution cvs-adc(0), OV:")
        print(np.amax(sol), np.amin(sol))
        print()

        #5. Clean up...
        #TODO delete del A, guess, rhs, AOVOV, rhs_ov
        #TODO: delete del oovv, occv, ovov, ovvv, foo, fvv

    ####ADC(0) PLAN B ####
    def compute_0b_OO(self, dm1, DM2):
        pass

    def compute_0b_VV(self, dm1, DM2):
        pass

    ##This is the one that works!!
    def compute_0b_OV(self, dm1, DM2, max_iterations):
        #1. Compute pre-requisites:
        #1.1 OO Fock, VV Fock and OVOV eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        fvv = self.reference_state.fock("v1v1").to_ndarray()

        eov = foo.diagonal().reshape(-1,1) - fvv.diagonal()

        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()

        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The one particle density matrix "
            errtxt += "does not contain the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #2.1 Prepare A matrix (solving for l, l A = rhs)
        #2.2 right hand side (rhs)
        #2.3 guess 
        rhs_ov = np.zeros(self.matrix["OV"].shape)
        guess_ov = np.zeros(self.matrix["OV"].shape)
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]

        rhs = np.zeros((nocc*nvirt,1))
        guess = np.zeros((nocc*nvirt,1))

        rhs_ov = ( np.einsum('kl,lika->ia', dm_oo, ooov, optimize=True)
                  -np.einsum('bc,icba->ia', dm_vv, ovvv, optimize=True)
                  )

        guess_ov = rhs_ov / eov 

        self.iter_count = 0
        def matvec(x):
            """
            Function to carry out the Ax matrix vector product
            to be used by the conjugate gradient solver.     
            """
            lov = x.reshape(nocc,nvirt)
            Ax = (lov*eov + np.einsum('jb,ibja->ia', lov, ovov)
                  - np.einsum('jb,ijab->ia', lov, oovv)
                 )
            self.iter_count += 1
            return Ax.reshape(nocc*nvirt)

        def precond(x):
            """
            Function to define the preconditioner for conjugate gradient
            """
            M = x.reshape(nocc,nvirt) / eov
            return M.reshape(nocc*nvirt)
        

        A = linalg.LinearOperator((nocc * nvirt, nocc * nvirt),
                                   matvec=matvec)
        M = linalg.LinearOperator((nocc * nvirt, nocc * nvirt),
                                   matvec=precond)
                
        guess = np.reshape(guess_ov, (nocc*nvirt))
        rhs = np.reshape(rhs_ov, (nocc*nvirt))

        l_ov, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=M,
                           callback=None, atol=None)

        #4. If the solution is found, save to self.matrix
        if w == 0:
            print("Conjugate gradient converged in %d iterations." % self.iter_count)
            self.matrix["OV"] = np.reshape(l_ov,(nocc,nvirt))
        else:
            raise Warning("Conjugate gradient did not converge.", w)

    ### Levchenko, thesis ###
    ## TODO: delete this routine
    def compute_levchenko_OO(self, dm1, DM2):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        if "OO" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            #np.fill_diagonal(dm_oo, 0.0)
        else:
            errtxt = "1DM does not contain the required OO block."
            raise ValueError(errtxt, dm1.blocks)
        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2DM does not contain the required OVOV block."
            raise ValuError(errtxt, DM2.blocks)
    
        self.matrix["OO"] = ( np.einsum("iakb,jakb->ij", DM_ovov, ovov)
                             -np.einsum("jakb,iakb->ij", DM_ovov, ovov)
                            )

        for i in range(foo.shape[0]):
            for j in range(foo.shape[1]):
                if abs(foo[i,i] - foo[j,j]) > 1e-5:
                    self.matrix["OO"][i,j] = (self.matrix["OO"][i,j]/(foo[i,i]-foo[j,j])
                                              -dm_oo[i,j])
                    #print(i,j)
                    #div_eps[i,j] = 1.0/(foo[i,i]-foo[j,j])
                else:
                    self.matrix["OO"][i,j] = 0.0
                    #print(i,j)
                    #self.matrix["OO"][i,j] += -dm_oo[i,j]*(foo[i,i]-foo[j,j]) 


    ### Levchenko, thesis ###
    def compute_levchenko_VV(self, dm1, DM2):
        #pre-requisites:
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
            #np.fill_diagonal(dm_oo, 0.0)
        else:
            errtxt = "1DM does not contain the required OO block."
            raise ValueError(errtxt, dm1.blocks)
        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2DM does not contain the required OVOV block."
            raise ValuError(errtxt, DM2.blocks)
    
        self.matrix["VV"] = ( np.einsum("iajc,ibjc->ab", DM_ovov, ovov)
                             -np.einsum("ibjc,iajc->ab", DM_ovov, ovov)
                            )

        for a in range(fvv.shape[0]):
            for b in range(fvv.shape[1]):
                if abs(fvv[a,a] - fvv[b,b]) > 1e-5:
                    self.matrix["VV"][a,b] = ( self.matrix["VV"][a,b]/(fvv[a,a]-fvv[b,b])
                                                -dm_vv[a,b] )
                    #print(i,j)
                    #div_eps[a,b] = 1.0/(fvv[a,a]-fvv[b,b])
                else:
                    self.matrix["VV"][a,b] = 0
                    #print(i,j)
                    #self.matrix["OO"][i,j] += -dm_oo[i,j]*(foo[i,i]-foo[j,j]) 


    def compute_levchenko_OV(self, dm1, DM2, max_iterations):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()

        #1PDM:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required OO and VV blocks."
            raise ValueError(errtxt)

        #2PDM:
        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2PDM does not contain the required OVOV block."
            raise ValueError(errtxt)

        #L blocks:
        if "OO" not in self.blocks:
            self.add_block("OO")
            self.compute_block("OO", dm1, DM2)
        loo = self.matrix["OO"]

        if "VV" not in self.blocks:
            self.add_block("VV")
            self.compute_block("VV", dm1, DM2)
        lvv = self.matrix["VV"]

        nocc = foo.shape[0]
        nvirt = fvv.shape[0]

        AOVOV = np.ones((nocc,nvirt,nocc,nvirt))
        rhs_ov = np.zeros((nocc,nvirt))
        A_mat_ovov = (  np.einsum('iajb,ijab->iajb', AOVOV, oovv)
                       -np.einsum('iajb,ibja->iajb', AOVOV, ovov)
                     )
        for i in range(nocc):
            for a in range(nvirt):
                A_mat_ovov[i,a,i,a] += fvv[a,a] - foo[i,i]
        
        rhs_ov = ( +np.einsum('jk,ikja->ia', dm_oo, ooov) 
                   -np.einsum('jk,ikja->ia', loo, ooov) #sign might be wrong
                   -np.einsum('bc,ibac->ia', dm_vv, ovvv) #here b and c might be
                   -np.einsum('bc,ibac->ia', lvv, ovvv) # swapped
                   -np.einsum('ibjc,jcab->ia', DM_ovov, ovvv)
                   -np.einsum('jakb,ijkb->ia', DM_ovov, ooov)
                 )

        A_mat = np.reshape(A_mat_ovov, (nocc*nvirt, nocc*nvirt))
        rhs = np.reshape(rhs_ov, (nocc*nvirt))
        guess = rhs.copy()

        lov, w = linalg.cg(A=A_mat, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=None,
                           callback=None, atol=None)

        #4. If the solution is found, save to self.matrix
        if w == 0:
            print("CONJUGATE GRADIENT CONVERGED!")
            self.matrix["OV"] = np.reshape(lov,(nocc,nvirt))
        else:
            raise Warning("Conjugate gradient did not converge.", w)

        #Check solution:
        sol = np.zeros((nocc,nvirt))
        
        for i in range(nocc):
            for a in range(nvirt):
                sol[i,a] = (fvv[a,a]-foo[i,i])*self.matrix["OV"][i,a]

        sol += ( np.einsum('jk,ikja->ia', loo, ooov) #sign might be wrong here
                +np.einsum('bc,ibac->ia', lvv, ovvv) #indices may be wrong here
                +np.einsum('jb,ijab->ia', self.matrix["OV"], oovv)
                -np.einsum('jb,ibja->ia', self.matrix["OV"], ovov)
                -np.einsum('jk,ikja->ia', dm_oo, ooov)
                +np.einsum('bc,ibac->ia', dm_vv, ovvv)
                +np.einsum('ibjc,jcab->ia', DM_ovov, ovvv)
                +np.einsum('jakb,ijkb->ia', DM_ovov, ooov)
                )

        print("Checking solution, levchenko OV:")
        print(np.amax(sol), np.amin(sol))

    ####ADC(1)#####
    def compute_1_OO(self, dm1, DM2):
        #1. Compute pre-requisites:
        #1.1 Density matrices:
        if "OO" in dm1.blocks:
            dm0_oo = dm1.matrix["OO"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO block. "
            raise ValueError(errtxt, dm1.blocks)
            
        if "OVOV" in DM2.blocks:
            Dm1_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVOV block. "
            raise ValueError(errtxt, DM2.blocks)
         
        #1.2 CC Fock and CVCV eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #2. Compute multipliers:
        nocc = ovov.shape[0] 
        #nvirt = ovov.shape[1]
        #consider that you have to change the sign when you move from the left
        #to the right-hand side of the equation
        #pre_fact = 0.25
        self.matrix["OO"] = ( np.einsum('iakb,jakb->ij',
                                         Dm1_ovov,ovov,optimize=True)
                           -  np.einsum('jakb,iakb->ij',
                                         Dm1_ovov,ovov,optimize=True)
                            )
        for i in range(nocc):
            for j in range(nocc):
                if abs(foo[i,i]-foo[j,j]) > 1e-5:
                    self.matrix["OO"][i,j] = ( self.matrix["OO"][i,j] /
                                               (foo[i,i]-foo[j,j]) 
                                             - dm0_oo[i,j] )
                else:
                    self.matrix["OO"][i,j] = 0.0 #- dm0_oo[i,j]#0.0

        #3. Clean up:
        #TODO: delete del foo, ovov, Dm1_ovov, dm0_oo

    def compute_1_VV(self, dm1, DM2):
        #1. Compute pre-requisites:
        #1.1 Density matrices:
        if "VV" in dm1.blocks:
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV block. "
            raise ValueError(errtxt, dm1.blocks)
    
        if "OVOV" in DM2.blocks:
            Dm1_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OO block. "
            raise ValueError(errtxt, DM2.blocks)
        
        #1.2 CC Fock and CVCV eri matrices: 
        fvv = self.reference_state.fock("v1v1").to_ndarray() 
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #2. Compute multipliers:
        #nocc = ovov.shape[0] 
        nvirt = ovov.shape[1]
        #pre_fact = 0.25
        self.matrix["VV"] = ( np.einsum('iajc,ibjc->ab',Dm1_ovov,ovov,
                                        optimize=True)
                            - np.einsum('ibjc,iajc->ab',Dm1_ovov,ovov,
                                        optimize=True)
                            )
        for a in range(nvirt):
            for b in range(nvirt):
                if abs(fvv[a,a]-fvv[b,b]) > 1e-5:
                    self.matrix["VV"][a,b] = ( self.matrix["VV"][a,b] / 
                                              (fvv[a,a]-fvv[b,b]) 
                                             - dm0_vv[a,b] )
                else:
                    self.matrix["VV"][a,b] = 0.0 #- dm0_vv[a,b] #0.0
        #3. Clean up:
        #TODO: delete del fvv, ovov, dm0_vv, Dm1_ovov

    def compute_1_OV(self, dm1, DM2, max_iterations):
        start = time.time()
        #1. Compute pre-requisites:
        #1.1 OO Fock, VV Fock and OVOV eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        eov = foo.diagonal().reshape(-1, 1) - fvv.diagonal()

        #1.2 Density Matrices
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm0_oo = dm1.matrix["OO"]
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain"
            errtxt += "the required OO and VV blocks. "
            raise ValueError(errtxt, dm1.blocks)

        if "OVOV" in DM2.blocks: #and "OOOO" in DM2.blocks:
            #if you want to include the HF ref state DMs
            #uncomment the OOOO block 
            Dm1_ovov = DM2.matrix["OVOV"]
            #Dm1_oooo = DM2.matrix["OOOO"]
        else:
            errtxt = "2 particle density matrix does not contain"
            errtxt += "the required OOOO and OVOV blocks. "
            raise ValueError(errtxt, DM2.blocks)
        
        #1.3 OO and VV lambda Lagrange Multipliers
        if "OO" not in self.blocks:
            self.add_block("OO")
            self.compute_block("OO", dm1, DM2)
        loo = self.matrix["OO"]
        
        if "VV" not in self.blocks:
            self.add_block("VV")
            self.compute_block("VV", dm1, DM2)
        lvv = self.matrix["VV"]

        rhs_ov = np.zeros(self.matrix["OV"].shape)
        guess_ov = np.zeros(self.matrix["OV"].shape)
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]

        rhs = np.zeros((nocc*nvirt))
        guess = np.zeros((nocc*nvirt))

        rhs_ov = (-np.einsum('jk,ikja->ia', dm0_oo+loo, ooov, optimize=True) 
                 - np.einsum('bc,icba->ia', dm0_vv+lvv, ovvv, optimize=True)
                 + np.einsum('jakb,ijkb->ia', 
                              Dm1_ovov, ooov, optimize=True)
                 + np.einsum('ibjc,jcab->ia',
                              Dm1_ovov, ovvv, optimize=True)
                )

        guess_ov = rhs_ov / eov 

        self.iter_count = 0
        def matvec(x):
            """
            Function to carry out the Ax matrix vector product
            to be used by the conjugate gradient solver.     
            """
            lov = x.reshape(nocc,nvirt)
            Ax = (lov*eov + np.einsum('jb,ibja->ia', lov, ovov)
                  - np.einsum('jb,ijab->ia', lov, oovv)
                 )
            self.iter_count += 1
            return Ax.reshape(nocc*nvirt)

        def precond(x):
            """
            Function to define the preconditioner for conjugate gradient
            """
            M = x.reshape(nocc,nvirt) / eov
            return M.reshape(nocc*nvirt)
        

        A = linalg.LinearOperator((nocc * nvirt, nocc * nvirt),
                                   matvec=matvec)
        M = linalg.LinearOperator((nocc * nvirt, nocc * nvirt),
                                   matvec=precond)

        guess = np.reshape(guess_ov, (nocc*nvirt))
        rhs = np.reshape(rhs_ov, (nocc*nvirt))

        l_ov, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=M,
                           callback=None, atol=None)

        #4. If the solution is found, save to self.matrix
        if w == 0:
            print("CONJUGATE GRADIENT CONVERGED in %d iterations!"% self.iter_count)
            self.matrix["OV"] = np.reshape(l_ov,(nocc,nvirt))
        else:
            raise Warning("Conjugate gradient did not converge.", w)
 
    def compute_1_VO(self, dm1, DM2, max_iterations):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks) 

        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2PDM does not contain the required OVOV block."

        nocc = foo.shape[0]
        nvirt = fvv.shape[0]
        AVOVO = np.ones((nvirt, nocc, nvirt, nocc))
        rhs_vo = np.zeros((nvirt, nocc))

        a_vovo = ( np.einsum("aibj,ijab->aibj", AVOVO, oovv)
                  -np.einsum("aibj,ibja->aibj", AVOVO, ovov)
                  )

        for a in range(nvirt):
            for i in range(nocc):
                a_vovo[a,i,a,i] += fvv[a,a] - foo[i,i]

        A = np.reshape(a_vovo, (nocc*nvirt,nocc*nvirt))
        #print(A) #-- make sure this matrix has the right elements!
        
        #pre_fact = 0.25
        rhs_vo = ( np.einsum("kl,ilka->ai", dm_oo, ooov)
                  +np.einsum("bc,icba->ai", dm_vv, ovvv)
                  -np.einsum("ibjc,jcab->ai", DM_ovov, ovvv)
                  -np.einsum("jakb,ijkb->ai", DM_ovov, ooov)
                  )

        rhs = np.reshape(rhs_vo, (nocc*nvirt))
        #print(rhs) #-- make sure the right hand side is correct!        
        guess = rhs.copy()

        lvo, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=None,
                           callback=None, atol=None)

        if w==0:
            print("ADC(1) VO, conjugate gradient converged!")
            self.matrix["VO"] = np.reshape(lvo, (nvirt, nocc))
        else:
            raise Warning("ADC(1) VO did not converged", w)

        #Check solution:
        l_vo = self.matrix["VO"]
        sol = ( np.einsum("bj,ijab->ai", l_vo, oovv)
               -np.einsum("bj,ibja->ai", l_vo, ovov)
               -np.einsum("kl,ilka->ai", dm_oo, ooov)
               -np.einsum("bc,icba->ai", dm_vv, ovvv)
               +np.einsum("ibjc,jcab->ai", DM_ovov, ovvv)
               +np.einsum("jakb,ijkb->ai", DM_ovov, ooov)
                )
        for a in range(nvirt):
            for i in range(nocc):
                sol[a,i] += l_vo[a,i]*(fvv[a,a]-foo[i,i])

        print("CHECKING solution:", np.amax(sol), np.amin(sol))
        ex_en = ( np.einsum('ij,ij->', dm_oo, foo)
                 +np.einsum('ab,ab->', dm_vv, fvv)
                 +0.25*np.einsum('iajb,iajb->', DM_ovov, ovov)
                 )
        print()
        print("CHECKING excitation energy:", ex_en)
 
    def compute_cvs2_CC(self, dm1, DM2):
        #1. Compute pre-requisites:
        #1.1 Density matrices:

        if "CC" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required CC block."
            raise ValueError(errtxt, dm1.blocks)

        if "CVCV" in DM2.blocks:
            Dm_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCCV" in DM2.blocks:
            Dm_occv = DM2.matrix["OCCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "CCVV" in DM2.blocks:
            Dm_ccvv = DM2.matrix["CCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CCVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCVV" in DM2.blocks:
            Dm_ocvv = DM2.matrix["OCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCVV block."
            raise ValueError(errtxt, DM2.blocks)
        
        #1.2 CC Fock and CVCV, OCCV eri matrices: 
        fcc = self.reference_state.fock("o2o2").to_ndarray() 
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()

        #2. Compute multipliers:
        ncore = cvcv.shape[0] 

        self.matrix["CC"] = ( np.einsum('IaKb,JaKb->IJ',
                                        Dm_cvcv, cvcv, optimize=True)
                            - np.einsum('JaKb,IaKb->IJ',
                                        Dm_cvcv, cvcv, optimize=True)
                            + 0.5*np.einsum('IKab,JKab->IJ',
                                            Dm_ccvv, ccvv, optimize=True)
                            - 0.5*np.einsum('JKab,IKab->IJ',
                                            Dm_ccvv, ccvv, optimize=True)
                            + 0.5*np.einsum('kIab,kJab->IJ',
                                            Dm_ocvv, ocvv, optimize=True)
                            - 0.5*np.einsum('kJab,kIab->IJ',
                                            Dm_ocvv, ocvv, optimize=True)
                            + np.einsum('kILa,kJLa->IJ',
                                        Dm_occv, occv, optimize=True)
                            - np.einsum('kJLa,kILa->IJ',
                                        Dm_occv, occv, optimize=True)
                            + np.einsum('kLIa,kLJa->IJ',
                                        Dm_occv, occv, optimize=True)
                            - np.einsum('kLJa,kLIa->IJ',
                                        Dm_occv, occv, optimize=True)
                            )
       
        for I in range(ncore):
            for J in range(ncore):
                if abs(fcc[I,I]-fcc[J,J]) > 1e-5: #How close is too close?
                    self.matrix["CC"][I,J] /= (fcc[I,I]-fcc[J,J])
                    #print(I,J,self.matrix["CC"][I,J], dm_cc[I,J])
                    self.matrix["CC"][I,J] += - dm_cc[I,J]
                    #print(self.matrix["CC"][I,J])
                else:
                    self.matrix["CC"][I,J] = 0.0

        #TODO: delete del fcc, occv, cvcv, Dm_occv, Dm_cvcv, dm_cc

    def compute_cvs2_OO(self, dm1, DM2):
        #1. Compute pre-requisites:
        #1.1 Density matrices:

        if "OO" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO block."
            raise ValueError(errtxt, dm1.blocks)

        if "OVVV" in DM2.blocks:
            Dm_ovvv = DM2.matrix["OVVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCCV" in DM2.blocks:
            Dm_occv = DM2.matrix["OCCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OOVV" in DM2.blocks:
            Dm_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OOVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCVV" in DM2.blocks:
            Dm_ocvv = DM2.matrix["OCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCVV block."
            raise ValueError(errtxt, DM2.blocks)

        #1.2 OO Fock and OCCV, OVVV, OOVV eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()

        #2. Compute multipliers:
        nocc = foo.shape[0]

        self.matrix["OO"] = ( np.einsum('iKLa,jKLa->ij',
                                        Dm_occv, occv, optimize=True)
                             -np.einsum('jKLa,iKLa->ij',
                                        Dm_occv, occv, optimize=True)
                             +0.5*np.einsum('iKab,jKab->ij',
                                            Dm_ocvv, ocvv, optimize=True)
                             -0.5*np.einsum('jKab,iKab->ij',
                                            Dm_ocvv, ocvv, optimize=True)
                             +0.5*np.einsum('ikab,jkab->ij',
                                            Dm_oovv, oovv, optimize=True)
                             -0.5*np.einsum('jkab,ikab->ij',
                                            Dm_oovv, oovv, optimize=True)
                             +0.5*np.einsum('iabc,jabc->ij',
                                            Dm_ovvv, ovvv, optimize=True)
                             -0.5*np.einsum('jabc,iabc->ij',
                                            Dm_ovvv, ovvv, optimize=True) 
                            )
       
        for i in range(nocc):
            for j in range(nocc):
                if abs(foo[i,i]-foo[j,j]) > 1e-5:
                    #pass
                    self.matrix["OO"][i,j] /= (foo[i,i]-foo[j,j])
                    self.matrix["OO"][i,j] -= dm_oo[i,j] 
                else:
                    #print(i,j)
                    self.matrix["OO"][i,j] = 0.0


        #TODO: delete del foo, ovvv, oovv, occv, dm_oo, Dm_ovvv, Dm_oovv, Dm_occv

    #TODO: These are zero, so don't compute them
    def compute_cvs2_VV(self, dm1, DM2):
        #1. Compute pre-requisites:
        #1.1 Density matrices:

        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        if "CVCV" in DM2.blocks:
            Dm_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCCV" in DM2.blocks:
            Dm_occv = DM2.matrix["OCCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OVVV" in DM2.blocks:
            Dm_ovvv = DM2.matrix["OVVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OOVV" in DM2.blocks:
            Dm_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OOVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCVV" in DM2.blocks:
            Dm_ocvv = DM2.matrix["OCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "CCVV" in DM2.blocks:
            Dm_ccvv = DM2.matrix["CCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CCVV block."
            raise ValueError(errtxt, DM2.blocks)
        
        #1.2 CC Fock and CVCV, OCCV eri matrices: 
        fvv = self.reference_state.fock("v1v1").to_ndarray() 
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()

        #2. Compute multipliers:
        nvirt = fvv.shape[0]

        self.matrix["VV"] = ( np.einsum("jKIa,jKIb->ab", Dm_occv, occv)
                             -np.einsum("jKIb,jKIa->ab", Dm_occv, occv)
                             +np.einsum("IaJc,IbJc->ab", Dm_cvcv, cvcv)
                             -np.einsum("IbJc,IaJc->ab", Dm_cvcv, cvcv)
                             +0.5*np.einsum("iacd,ibcd->ab", Dm_ovvv, ovvv)
                             -0.5*np.einsum("ibcd,iacd->ab", Dm_ovvv, ovvv)
                             +0.5*np.einsum("IJac,IJbc->ab", Dm_ccvv, ccvv)
                             -0.5*np.einsum("IJbc,IJac->ab", Dm_ccvv, ccvv)
                             +np.einsum("iJac,iJbc->ab", Dm_ocvv, ocvv)
                             -np.einsum("iJbc,iJac->ab", Dm_ocvv, ocvv)
                             +0.5*np.einsum("ijac,ijbc->ab", Dm_oovv, oovv)
                             -0.5*np.einsum("ijbc,ijac->ab", Dm_oovv, oovv)
                             +np.einsum("idac,idbc->ab", Dm_ovvv, ovvv)
                             -np.einsum("idbc,idac->ab", Dm_ovvv, ovvv)
                             ) 

                    ###     ( np.einsum('IaJc,IbJc->ab',
                    ###                    Dm_cvcv, cvcv, optimize=True)
                    ###        - np.einsum('IbJc,IaJc->ab',
                    ###                    Dm_cvcv, cvcv, optimize=True)
                    ###        + np.einsum('lKIa,lKIb->ab',
                    ###                    Dm_occv, occv, optimize=True) 
                    ###        - np.einsum('lKIb,lKIa->ab',
                    ###                    Dm_occv, occv, optimize=True)
                    ###        + 0.5*np.einsum('ijac,ijbc->ab',
                    ###                        Dm_oovv, oovv, optimize=True)
                    ###        - 0.5*np.einsum('ijbc,ijac->ab',
                    ###                        Dm_oovv, oovv, optimize=True)
                    ###        + 0.5*np.einsum('IJac,IJbc->ab',
                    ###                    Dm_ccvv, ccvv, optimize=True)
                    ###        - 0.5*np.einsum('IJbc,IJac->ab',
                    ###                    Dm_ccvv, ccvv, optimize=True)
                    ###        + np.einsum('iJac,iJbc->ab',
                    ###                    Dm_ocvv, ocvv, optimize=True)
                    ###        - np.einsum('iJbc, iJac->ab',
                    ###                    Dm_ocvv, ocvv, optimize=True)
                    ###        + 0.5*np.einsum('iacd,ibcd->ab',
                    ###                        Dm_ovvv, ovvv, optimize=True)
                    ###        - 0.5*np.einsum('ibcd,iacd->ab',
                    ###                        Dm_ovvv, ovvv, optimize=True)
                    ###        + np.einsum('icda,icdb->ab',
                    ###                    Dm_ovvv, ovvv, optimize=True)
                    ###        - np.einsum('icdb,icda->ab',
                    ###                    Dm_ovvv, ovvv, optimize=True)
                    ###        )
       
        for a in range(nvirt):
            for b in range(nvirt):
                if abs(fvv[a,a]-fvv[b,b]) > 1e-5:
                    self.matrix["VV"][a,b] /= (fvv[a,a]-fvv[b,b])
                    self.matrix["VV"][a,b] -= dm_vv[a,b] 
                else:
                    self.matrix["VV"][a,b] = 0.0


        #Clean unp:
        #TODO: delete del fvv, ovvv, oovv, occv, cvcv, dm_vv, Dm_ovvv
        #TODO delete del Dm_oovv, Dm_occv, Dm_cvcv

    def compute_cvs2_OC(self, dm1, DM2):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fcc = self.reference_state.fock("o2o2").to_ndarray()

        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()

        eoc = foo.diagonal().reshape(-1,1)-fcc.diagonal()

        ok = True
        if "CCVV" in DM2.blocks:
            DM2_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False

        if "OCCV" in DM2.blocks:
            DM2_occv = DM2.matrix["OCCV"]
        else:
            ok = False

        if "OCVV" in DM2.blocks:
            DM2_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False

        if "CVCV" in DM2.blocks:
            DM2_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False

        if "OOVV" in DM2.blocks:
            DM2_oovv = DM2.matrix["OOVV"]
        else:
            ok = False

        if "OVVV" in DM2.blocks:
            DM2_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required CCVV,"
            errtxt += " OCCV, OCVV, CVCV, OCVV, OOVV, OVVV blocks."
            raise ValueError(errtxt, DM2.blocks)

        nocc = foo.shape[0]
        ncore = fcc.shape[0]

        self.matrix["OC"] = ( -0.5*np.einsum('JKab,iKab->iJ',
                                              DM2_ccvv, ocvv, optimize=True)
                              +np.einsum('kJLa,ikLa->iJ',
                                          DM2_occv, oocv, optimize=True)
                              +0.5*np.einsum('kJab,ikab->iJ',
                                              DM2_ocvv, oovv, optimize=True)
                              -np.einsum('kLJa,kLia->iJ',
                                         DM2_occv, ocov, optimize=True)
                              -np.einsum('JaKb,iaKb->iJ',
                                          DM2_cvcv, ovcv, optimize=True)
                              +np.einsum('iKLa,JKLa->iJ',
                                          DM2_occv, cccv, optimize=True)
                              +0.5*np.einsum('iKab,JKab->iJ',
                                              DM2_ocvv, ccvv, optimize=True)
                              -0.5*np.einsum('ikab,kJab->iJ',
                                              DM2_oovv, ocvv, optimize=True)
                              +0.5*np.einsum('iabc,Jabc->iJ',
                                              DM2_ovvv, cvvv, optimize=True) 
                                )
        self.matrix["OC"] /= eoc

    def compute_cvs2_CV_OV(self, dm1, DM2, max_iterations):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()

        nocc = foo.shape[0]
        ncore = fcc.shape[0]
        nvirt = fvv.shape[0]
        nfull = ncore + nocc

        eov = foo.diagonal().reshape(-1,1) - fvv.diagonal()
        ecv = fcc.diagonal().reshape(-1,1) - fvv.diagonal()
        ecov = np.zeros((nfull, nvirt))
        ecov[:ncore,:] = ecv
        ecov[ncore:,:] = eov

        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray() # duplicate cvov
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        ccov = self.reference_state.eri("o2o2o1v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()
        vvvv = self.reference_state.eri("v1v1v1v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        cvov = self.reference_state.eri("o2v1o1v1").to_ndarray() #duplicate!
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()

        if "OO" in dm1.blocks and "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required"
            errtxt += " OO, CC, and VV blocks." 
            raise ValueError(errtxt, dm1.blocks)

        #Can be deleted;
        if "OO" in self.blocks and "CC" in self.blocks and "VV" in self.blocks:
            loo = self.matrix["OO"]
            lcc = self.matrix["CC"]
            lvv = self.matrix["VV"]
        #else:
        #    errtxt = "L multiplier does not contain the required"
        #    errtxt += " OO, CC, and VV blocks." 
        #    raise ValueError(errtxt, self.blocks)  

        if "OC" in self.blocks:
            loc = self.matrix["OC"]
        else:
            errtxt = "L multiplier does not contain the required OC block."
            raise ValueError(errtxt, self.blocks)

        ok = True
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
    
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False

        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False

        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False

        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False

        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required CVCV, "
            errtxt += "CCVV, OCVV, OCCV, OOVV, OVVV blocks."
            raise ValueError(errtxt, DM2.blocks)


        # A*x = rhs
        rhs_fv = np.zeros((nfull, nvirt))

        rhs_fv[:ncore,:] = ( np.einsum('JK,KIJa->Ia', dm_cc, cccv)
                                +np.einsum('jK,KIja->Ia', loc, ccov)
                                +np.einsum('jK,jIKa->Ia', loc, occv)
                                +np.einsum('jk,kIja->Ia', dm_oo, ocov)
                                -np.einsum('bc,Icba->Ia', dm_vv, cvvv)
                                +np.einsum('lKJa,lKIJ->Ia', DM_occv, occc)
                                +np.einsum('JaKb,IJKb->Ia', DM_cvcv, cccv)
                                -0.5*np.einsum('jabc,jIbc->Ia', DM_ovvv,ocvv)
                                -0.5*np.einsum('JKab,JKIb->Ia', DM_ccvv, cccv)
                                -np.einsum('jKab,jKIb->Ia', DM_ocvv, occv)
                                -0.5*np.einsum('jkab,jkIb->Ia', DM_oovv, oocv)
                                -np.einsum('jcab,jcIb->Ia', DM_ovvv, ovcv)
                                -0.5*np.einsum('IJbc,Jabc->Ia', DM_ccvv, cvvv)
                                +np.einsum('jIKb,jaKb->Ia', DM_occv, ovcv)
                                +0.5*np.einsum('jIbc,jabc->Ia', DM_ocvv, ovvv)
                                +np.einsum('kJIb,kJab->Ia', DM_occv, ocvv)
                                +np.einsum('IbJc,Jcab->Ia', DM_cvcv, cvvv)
                                )
        #ia
        rhs_fv[ncore:nfull,:] = (-np.einsum('JK,iKJa->ia', dm_cc, occv)
                                 -np.einsum('jK,iKja->ia', loc, ocov)
                                 -np.einsum('jK,ijKa->ia', loc, oocv)
                                 +np.einsum('jk,kija->ia', dm_oo, ooov)
                                 -np.einsum('bc,icba->ia', dm_vv, ovvv)
                                 +np.einsum('lKJa,lKiJ->ia', DM_occv, ococ)
                                 +np.einsum('JaKb,iJKb->ia', DM_cvcv, occv)
                                 +0.5*np.einsum('jabc,ijbc->ia',
                                                 DM_ovvv, oovv)
                                 -0.5*np.einsum('JKab,JKib->ia',
                                                 DM_ccvv, ccov)
                                 -np.einsum('jKab,jKib->ia',
                                             DM_ocvv, ocov)
                                 -0.5*np.einsum('jkab,jkib->ia',
                                                 DM_oovv, ooov)
                                 -np.einsum('jcab,ibjc->ia', DM_ovvv, ovov)
                                 -np.einsum('iJKb,JaKb->ia', DM_occv, cvcv)
                                 -0.5*np.einsum('iJbc,Jabc->ia',
                                                 DM_ocvv, cvvv)
                                 -0.5*np.einsum('ijbc,jabc->ia',
                                                 DM_oovv, ovvv)
                                 +0.5*np.einsum('ibcd,abcd->ia',
                                                 DM_ovvv, vvvv)
                                     )

        self.iter_count = 0
        def matvec(x):
            """
            Function to carry out the Ax matrix vector product
            to be used by the conjugate gradient solver.     
            """
            lfv = x.reshape(nfull,nvirt)
            lcv = lfv[:ncore,:]
            lov = lfv[ncore:,:]
            Alcv = (  np.einsum('Jb,IbJa->Ia', lcv, cvcv)
                    - np.einsum('Jb,IJab->Ia', lcv, ccvv)
                    + np.einsum('jb,jaIb->Ia', lov, ovcv)
                    + np.einsum('jb,jIab->Ia', lov, ocvv)
                    )
                     
            Alov = (  np.einsum("jb,ibja->ia", lov, ovov)
                    - np.einsum("jb,ijab->ia", lov, oovv)
                    + np.einsum("Jb,ibJa->ia", lcv, ovcv)
                    - np.einsum("Jb,iJab->ia", lcv, ocvv)
                    )
            Ax = np.zeros((nfull,nvirt))
            Ax[:ncore,:] = Alcv
            Ax[ncore:,:] = Alov 
            Ax += lfv*ecov
            self.iter_count += 1
 
            return Ax.reshape(nfull*nvirt)

        def precond(x):
            """
            Function to define the preconditioner for conjugate gradient
            """
            M = x.reshape(nfull,nvirt) / ecov
            return M.reshape(nfull*nvirt)

        A = linalg.LinearOperator((nfull*nvirt, nfull*nvirt),
                                   matvec=matvec)
        M = linalg.LinearOperator((nfull*nvirt, nfull*nvirt),
                                   matvec=precond)
        #A = np.reshape(A_fvfv, (nfull*nvirt,nfull*nvirt))
        rhs = np.reshape(rhs_fv, (nfull*nvirt))
        guess = (rhs_fv / ecov).reshape(nfull*nvirt)

        lfv, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=M,
                           callback=None, atol=None)

        if w == 0:
            print("Conjugate gradient converged in %d iterations." % self.iter_count)
            l_fv = np.reshape(lfv, (nfull, nvirt))
            self.matrix["CV"] = l_fv[:ncore,:]
            self.matrix["OV"] = l_fv[ncore:nfull,:]
        else:
            raise Warning("Conjugate gradient did not converge.", w)


    # TODO: remove routine below; CV and OV have to be computed together!
    def compute_cvs2_OV(self, dm1, DM2, max_iterations):
        #1. Compute pre-requisites:
        #1.1 OO Fock, VV Fock and OCCV, OVOV, OOVV, OVVV, 
        #OOOV, OCOC, CVCV, eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        vvvv = self.reference_state.eri("v1v1v1v1").to_ndarray()
        

        #1.2 Density matirces:
        if "VV" in dm1.blocks and "CC" in dm1.blocks and "OO" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required CC, OO, and VV block."
            raise ValueError(errtxt, dm1.blocks)

        if "CVCV" in DM2.blocks:
            Dm_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCCV" in DM2.blocks:
            Dm_occv = DM2.matrix["OCCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OOVV" in DM2.blocks:
            Dm_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OOVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OVVV" in DM2.blocks:
            Dm_ovvv = DM2.matrix["OVVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVVV block."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 Lagrange multipliers:
        #Compute them if they have not yet been computed
        if "CC" not in self.blocks:
            self.add_block("CC")
            self.compute_block("CC", dm1, DM2)
        lcc = self.matrix["CC"]

        if "OO" not in self.blocks:
            self.add_block("CC")
            self.compute_block("OO", dm1, DM2)
        loo = self.matrix["OO"]

        
        if "VV" not in self.blocks:
            self.add_block("VV")
            self.compute_block("VV", dm1, DM2)
        lvv = self.matrix["VV"]

        #2.1 Prepare A matrix (solving for l, l A = rhs)
        #2.2 right hand side (rhs)
        #2.3 and guess
        ncore = dm_cc.shape[0]
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]

        AOVOV = np.ones((nocc,nvirt,nocc,nvirt))
        rhs_ov = np.zeros((nocc,nvirt))
        guess_ov = np.zeros((nocc, nvirt))

        A = np.zeros((nocc*nvirt, nocc*nvirt))
        rhs = np.zeros((nocc*nvirt))
        guess = np.zeros((nocc*nvirt))


        AOVOV = ( np.einsum('iajb,ibja->iajb', AOVOV, ovov,
                            optimize=True) 
                - np.einsum('iajb,ijab->iajb', AOVOV, oovv,
                            optimize=True)
                )
        for i in range(nocc):
            for a in range(nvirt):
                AOVOV[i,a,i,a] = AOVOV[i,a,i,a] + (foo[i,i]-fvv[a,a])  

        rhs_ov = (-1.0*np.einsum('JK,iKJa->ia', dm_cc+lcc, occv, 
                                 optimize=True)
                    - np.einsum('jk,ikja->ia', dm_oo+loo, ooov,
                                optimize=True)
                    - np.einsum('bc,icba->ia', dm_vv+lvv, ovvv,
                                optimize=True)
                    - np.einsum('lKJa,iJlK->ia', Dm_occv, ococ,
                                optimize=True)
                    + np.einsum('JaKb,iJKb->ia', Dm_cvcv, occv,
                                optimize=True)
                    - np.einsum('iJKb,JaKb->ia', Dm_occv, cvcv,
                                optimize=True)
                    + 0.5*np.einsum('jabc,ijbc->ia', Dm_ovvv, oovv,
                                    optimize=True) 
                    - 0.5*np.einsum('ijbc,jabc->ia', Dm_oovv, ovvv, 
                                    optimize=True)
                    + 0.5*np.einsum('ibcd,abcd->ia', Dm_ovvv, vvvv,
                                    optimize=True)
                    - 0.5*np.einsum('jkab,jkib->ia', Dm_oovv, ooov,
                                    optimize=True)
                    - 0.5*np.einsum('jcab,ibjc->ia', Dm_ovvv, ovov,
                                    optimize=True)
                )

        guess_ov = -1.0*rhs_ov

        for i in range(nocc):
            for a in range(nvirt):
                if abs(foo[i,i]-fvv[a,a]) > 1e-10:
                    guess_ov[i,a] = guess_ov[i,a]/(foo[i,i]-fvv[a,a]) 
                else:
                    guess_ov[i,a] = 0.0
                
        A = np.reshape(AOVOV,(nocc*nvirt,nocc*nvirt))
        guess = np.reshape(guess_ov, (nocc*nvirt))
        rhs = np.reshape(rhs_ov, (nocc*nvirt))

        #Compute Lagrange multipliers
        #(currently using the conjugate gradient solver of scipy)
        lov, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=None,
                           callback=None, atol=None)

        #4. If the solution is found, save to self.matrix
        if w == 0:
            self.matrix["OV"] = np.reshape(lov, (nocc,nvirt))
        else:
            print("Warning: conjugate gradient did not converge.")

        #5. Clean up:
        #TODO delete del fvv, ovvv, vvvv, oovv, occv, cvcv, dm_vv, dm_oo
        #TODO delete del dm_cc, Dm_ovvv, Dm_oovv, Dm_occv, Dm_cvcv, lcc, loo, lvv

    def compute_cvs2_CV(self, dm1, DM2):
        pass
        #print(self.order + " CV block trivially zero. Nothing to compute.")

    # TODO: delete all routines where the multipliers are zero!
    # or at least set to pass
    #START CVS-ADC2x ##############################
    def compute_cvs2x_CC(self, dm1, DM2):
        #1. Compute pre-requisites:
        #1.1 Density matrices:

        if "CC" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required CC block."
            raise ValueError(errtxt, dm1.blocks)

        if "CVCV" in DM2.blocks:
            Dm_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCCV" in DM2.blocks:
            Dm_occv = DM2.matrix["OCCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "CCVV" in DM2.blocks:
            Dm_ccvv = DM2.matrix["CCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CCVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCVV" in DM2.blocks:
            Dm_ocvv = DM2.matrix["OCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCOC" in DM2.blocks:
            Dm_ococ = DM2.matrix["OCOC"]
        else:
            errtxt = "The 2PDM does not contain"
            errtxt += " the required OCOC block."
            raise ValueError(errtxt, DM2.blocks)

        ####if "OVOV" in DM2.blocks:
        ####    DM_ovov = DM2.matrix["OVOV"]
        ####else:
        ####    errtxt = "The 2PDM does not contain the required OVOV block."
        ####    raise ValueError(errtxt, DM2.blocks)

        ####if "VVVV" in DM2.blocks:
        ####    DM_vvvv = DM2.matrix["VVVV"]
        ####else:
        ####    errtxt = "The 2PDM does not contain the required VVVV block."
        ####    raise ValueError(errtxt, DM2.blocks)

        #1.2 CC Fock and CVCV, OCCV eri matrices: 
        fcc = self.reference_state.fock("o2o2").to_ndarray() 
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()

        #2. Compute multipliers:
        ncore = cvcv.shape[0] 

        self.matrix["CC"] = ( np.einsum('IaKb,JaKb->IJ',
                                        Dm_cvcv, cvcv, optimize=True)
                            - np.einsum('JaKb,IaKb->IJ',
                                        Dm_cvcv, cvcv, optimize=True)
                            + 0.5*np.einsum('IKab,JKab->IJ',
                                            Dm_ccvv, ccvv, optimize=True)
                            - 0.5*np.einsum('JKab,IKab->IJ',
                                            Dm_ccvv, ccvv, optimize=True)
                            + 0.5*np.einsum('kIab,kJab->IJ',
                                            Dm_ocvv, ocvv, optimize=True)
                            - 0.5*np.einsum('kJab,kIab->IJ',
                                            Dm_ocvv, ocvv, optimize=True)
                            + np.einsum('kILa,kJLa->IJ',
                                        Dm_occv, occv, optimize=True)
                            - np.einsum('kJLa,kILa->IJ',
                                        Dm_occv, occv, optimize=True)
                            + np.einsum('kLIa,kLJa->IJ',
                                        Dm_occv, occv, optimize=True)
                            - np.einsum('kLJa,kLIa->IJ',
                                        Dm_occv, occv, optimize=True)
                            + np.einsum('kImL,kJmL->IJ', 
                                        Dm_ococ, ococ, optimize=True)
                            - np.einsum('kJmL,kImL->IJ',
                                        Dm_ococ, ococ, optimize=True)
                            )
       
        for I in range(ncore):
            for J in range(ncore):
                if abs(fcc[I,I]-fcc[J,J]) > 1e-5: #How close is too close?
                    self.matrix["CC"][I,J] /= (fcc[I,I]-fcc[J,J])
                    #print(I,J,self.matrix["CC"][I,J], dm_cc[I,J])
                    self.matrix["CC"][I,J] += - dm_cc[I,J]
                    #print(self.matrix["CC"][I,J])
                else:
                    self.matrix["CC"][I,J] = 0.0

        #TODO: delete del fcc, occv, cvcv, Dm_occv, Dm_cvcv, dm_cc

    def compute_cvs2x_OO(self, dm1, DM2):
        #1. Compute pre-requisites:
        #1.1 Density matrices:

        if "OO" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO block."
            raise ValueError(errtxt, dm1.blocks)

        if "OVVV" in DM2.blocks:
            Dm_ovvv = DM2.matrix["OVVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCCV" in DM2.blocks:
            Dm_occv = DM2.matrix["OCCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OOVV" in DM2.blocks:
            Dm_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OOVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCVV" in DM2.blocks:
            Dm_ocvv = DM2.matrix["OCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCOC" in DM2.blocks:
            Dm_ococ = DM2.matrix["OCOC"]
        else:
            errtxt = "The 2PDM does not contain"
            errtxt += " the required OCOC block."
            raise ValueError(errtxt, DM2.blocks)

        if "OVOV" in DM2.blocks:
            Dm_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "The 2PDM does not contain the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)

        ####if "VVVV" in DM2.blocks:
        ####    DM_vvvv = DM2.matrix["VVVV"]
        ####else:
        ####    errtxt = "The 2PDM does not contain the required VVVV block."
        ####    raise ValueError(errtxt, DM2.blocks)

        #1.2 OO Fock and OCCV, OVVV, OOVV eri matrices: 
        foo = self.reference_state.fock("o1o1").to_ndarray() 
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #2. Compute multipliers:
        nocc = foo.shape[0]

        self.matrix["OO"] = ( np.einsum('iKLa,jKLa->ij',
                                        Dm_occv, occv, optimize=True)
                             -np.einsum('jKLa,iKLa->ij',
                                        Dm_occv, occv, optimize=True)
                             +0.5*np.einsum('iKab,jKab->ij',
                                            Dm_ocvv, ocvv, optimize=True)
                             -0.5*np.einsum('jKab,iKab->ij',
                                            Dm_ocvv, ocvv, optimize=True)
                             +0.5*np.einsum('ikab,jkab->ij',
                                            Dm_oovv, oovv, optimize=True)
                             -0.5*np.einsum('jkab,ikab->ij',
                                            Dm_oovv, oovv, optimize=True)
                             +0.5*np.einsum('iabc,jabc->ij',
                                            Dm_ovvv, ovvv, optimize=True)
                             -0.5*np.einsum('jabc,iabc->ij',
                                            Dm_ovvv, ovvv, optimize=True)
                            +np.einsum('iKlM,jKlM->ij',
                                        Dm_ococ, ococ, optimize=True)
                            -np.einsum('jKlM,iKlM->ij',
                                        Dm_ococ, ococ, optimize=True)
                            +np.einsum('iakb,jakb->ij',
                                        Dm_ovov, ovov, optimize=True)
                            -np.einsum('jakb,iakb->ij',
                                        Dm_ovov, ovov, optimize=True) 
                            )
       
        for i in range(nocc):
            for j in range(nocc):
                if abs(foo[i,i]-foo[j,j]) > 1e-5:
                    #pass
                    self.matrix["OO"][i,j] /= (foo[i,i]-foo[j,j])
                    self.matrix["OO"][i,j] -= dm_oo[i,j] 
                else:
                    #print(i,j)
                    self.matrix["OO"][i,j] = 0.0

    def compute_cvs2x_VV(self, dm1, DM2):
        #pass
        #1. Compute pre-requisites:
        #1.1 Density matrices:

        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        if "CVCV" in DM2.blocks:
            Dm_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCCV" in DM2.blocks:
            Dm_occv = DM2.matrix["OCCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCCV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OVVV" in DM2.blocks:
            Dm_ovvv = DM2.matrix["OVVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OOVV" in DM2.blocks:
            Dm_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OOVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OCVV" in DM2.blocks:
            Dm_ocvv = DM2.matrix["OCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OCVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "CCVV" in DM2.blocks:
            Dm_ccvv = DM2.matrix["CCVV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CCVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OVOV" in DM2.blocks:
            Dm_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "The 2PDM does not contain the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)

        if "VVVV" in DM2.blocks:
            Dm_vvvv = DM2.matrix["VVVV"]
        else:
            errtxt = "The 2PDM does not contain the required VVVV block."
            raise ValueError(errtxt, DM2.blocks)
        
        # 1.2 CC Fock and CVCV, OCCV eri matrices: 
        fvv = self.reference_state.fock("v1v1").to_ndarray() 
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        vvvv = self.reference_state.eri("v1v1v1v1").to_ndarray()

        #2. Compute multipliers:
        nvirt = fvv.shape[0]

        self.matrix["VV"] = ( np.einsum("jKIa,jKIb->ab", Dm_occv, occv)
                             -np.einsum("jKIb,jKIa->ab", Dm_occv, occv)
                             +np.einsum("IaJc,IbJc->ab", Dm_cvcv, cvcv)
                             -np.einsum("IbJc,IaJc->ab", Dm_cvcv, cvcv)
                             +0.5*np.einsum("iacd,ibcd->ab", Dm_ovvv, ovvv)
                             -0.5*np.einsum("ibcd,iacd->ab", Dm_ovvv, ovvv)
                             +0.5*np.einsum("IJac,IJbc->ab", Dm_ccvv, ccvv)
                             -0.5*np.einsum("IJbc,IJac->ab", Dm_ccvv, ccvv)
                             +np.einsum("iJac,iJbc->ab", Dm_ocvv, ocvv)
                             -np.einsum("iJbc,iJac->ab", Dm_ocvv, ocvv)
                             +0.5*np.einsum("ijac,ijbc->ab", Dm_oovv, oovv)
                             -0.5*np.einsum("ijbc,ijac->ab", Dm_oovv, oovv)
                             +np.einsum("idac,idbc->ab", Dm_ovvv, ovvv)
                             -np.einsum("idbc,idac->ab", Dm_ovvv, ovvv)
                             +np.einsum("iajc,ibjc->ab", Dm_ovov, ovov)
                             -np.einsum("ibjc,iajc->ab", Dm_ovov, ovov)
                             +0.5*np.einsum("acde,bcde->ab", Dm_vvvv, vvvv)
                             -0.5*np.einsum("bcde,acde->ab", Dm_vvvv, vvvv)
                             ) 

       
        for a in range(nvirt):
            for b in range(nvirt):
                if abs(fvv[a,a]-fvv[b,b]) > 1e-5:
                    self.matrix["VV"][a,b] /= (fvv[a,a]-fvv[b,b])
                    self.matrix["VV"][a,b] -= dm_vv[a,b] 
                else:
                    self.matrix["VV"][a,b] = 0.0


    def compute_cvs2x_OC(self, dm1, DM2):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fcc = self.reference_state.fock("o2o2").to_ndarray()

        eoc = foo.diagonal().reshape(-1,1) - fcc.diagonal()

        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()
        oooc = self.reference_state.eri("o1o1o1o2").to_ndarray()
        ccco = self.reference_state.eri("o2o2o2o1").to_ndarray()

        ok = True
        if "CCVV" in DM2.blocks:
            DM2_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False

        if "OCCV" in DM2.blocks:
            DM2_occv = DM2.matrix["OCCV"]
        else:
            ok = False

        if "OCVV" in DM2.blocks:
            DM2_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False

        if "CVCV" in DM2.blocks:
            DM2_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False

        if "OOVV" in DM2.blocks:
            DM2_oovv = DM2.matrix["OOVV"]
        else:
            ok = False

        if "OVVV" in DM2.blocks:
            DM2_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False

        if "OCOC" in DM2.blocks:
            DM2_ococ = DM2.matrix["OCOC"]
        else:
            ok = False

        if "OVOV" in DM2.blocks:
            DM2_ovov = DM2.matrix["OVOV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required CCVV, OCCV, OCVV, "
            errtxt += "CVCV, OCVV, OOVV, OVVV, OCOC, and OVOV blocks."
            raise ValueError(errtxt, DM2.blocks)

        nocc = foo.shape[0]
        ncore = fcc.shape[0]

        self.matrix["OC"] = ( -0.5*np.einsum('JKab,iKab->iJ',
                                              DM2_ccvv, ocvv, optimize=True)
                              +np.einsum('kJLa,ikLa->iJ',
                                          DM2_occv, oocv, optimize=True)
                              +0.5*np.einsum('kJab,ikab->iJ',
                                              DM2_ocvv, oovv, optimize=True)
                              -np.einsum('kLJa,kLia->iJ',
                                         DM2_occv, ocov, optimize=True)
                              -np.einsum('JaKb,iaKb->iJ',
                                          DM2_cvcv, ovcv, optimize=True)
                              +np.einsum('iKLa,JKLa->iJ',
                                          DM2_occv, cccv, optimize=True)
                              +0.5*np.einsum('iKab,JKab->iJ',
                                              DM2_ocvv, ccvv, optimize=True)
                              -0.5*np.einsum('ikab,kJab->iJ',
                                              DM2_oovv, ocvv, optimize=True)
                              +0.5*np.einsum('iabc,Jabc->iJ',
                                              DM2_ovvv, cvvv, optimize=True)
                              +np.einsum('kJmL,ikmL->iJ',
                                          DM2_ococ, oooc, optimize=True)
                              -np.einsum('iKlM,JKMl->iJ',
                                          DM2_ococ, ccco, optimize=True)
                              +np.einsum('iakb,kbJa->iJ',
                                          DM2_ovov, ovcv, optimize=True) 
                            )

        self.matrix["OC"] /= eoc


    def compute_cvs2x_CV_OV(self, dm1, DM2, max_iterations):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()

        nocc = foo.shape[0]
        ncore = fcc.shape[0]
        nvirt = fvv.shape[0]

        nfull = ncore + nocc

        ecv = fcc.diagonal().reshape(-1,1) - fvv.diagonal()
        eov = foo.diagonal().reshape(-1,1) - fvv.diagonal()
        ecov = np.zeros((nfull, nvirt))
        ecov[:ncore,:] = ecv
        ecov[ncore:,:] = eov

        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray() #duplicat cvov
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        ccov = self.reference_state.eri("o2o2o1v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()
        vvvv = self.reference_state.eri("v1v1v1v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        cvov = self.reference_state.eri("o2v1o1v1").to_ndarray() #duplicate!
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()


        if "OO" in dm1.blocks and "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required"
            errtxt += " OO, CC, and VV blocks." 
            raise ValueError(errtxt, dm1.blocks)

        if "OC" in self.blocks:
            loc = self.matrix["OC"]
        else:
            errtxt = "L multiplier does not contain the required OC block."
            raise ValueError(errtxt, self.blocks)

        ok = True
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
    
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False

        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False

        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False

        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False

        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False

        if "OCOC" in DM2.blocks:
            DM_ococ = DM2.matrix["OCOC"]
        else:
            ok = False

        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            ok = False

        if "VVVV" in DM2.blocks:
            DM_vvvv = DM2.matrix["VVVV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required CVCV, CCVV, OCVV, "
            errtxt += "OCCV, OOVV, OVVV, OCOC, OVOV, and VVVV blocks."
            raise ValueError(errtxt, DM2.blocks)

        rhs_fv = np.zeros((nfull, nvirt))

        rhs_fv[:ncore,:] = ( np.einsum('JK,KIJa->Ia', dm_cc, cccv)
                                +np.einsum('jK,KIja->Ia', loc, ccov)
                                +np.einsum('jK,jIKa->Ia', loc, occv)
                                +np.einsum('jk,kIja->Ia', dm_oo, ocov)
                                -np.einsum('bc,Icba->Ia', dm_vv, cvvv)
                                +np.einsum('lKJa,lKIJ->Ia', DM_occv, occc)
                                +np.einsum('JaKb,IJKb->Ia', DM_cvcv, cccv)
                                -0.5*np.einsum('jabc,jIbc->Ia', DM_ovvv,ocvv)
                                -0.5*np.einsum('JKab,JKIb->Ia', DM_ccvv, cccv)
                                -np.einsum('jKab,jKIb->Ia', DM_ocvv, occv)
                                -0.5*np.einsum('jkab,jkIb->Ia', DM_oovv, oocv)
                                -np.einsum('jcab,jcIb->Ia', DM_ovvv, ovcv)
                                -0.5*np.einsum('IJbc,Jabc->Ia', DM_ccvv, cvvv)
                                +np.einsum('jIKb,jaKb->Ia', DM_occv, ovcv)
                                +0.5*np.einsum('jIbc,jabc->Ia', DM_ocvv, ovvv)
                                +np.einsum('kJIb,kJab->Ia', DM_occv, ocvv)
                                +np.einsum('IbJc,Jcab->Ia', DM_cvcv, cvvv)
                                -0.5*np.einsum('abcd,Ibcd->Ia', DM_vvvv, cvvv) #x
                                -np.einsum('jakb,jIkb->Ia', DM_ovov, ocov) #x
                                +np.einsum('jIlK,lKja->Ia', DM_ococ, ocov) #x
                                )
        #ia
        rhs_fv[ncore:nfull,:] = (-np.einsum('JK,iKJa->ia', dm_cc, occv)
                                 -np.einsum('jK,iKja->ia', loc, ocov)
                                 -np.einsum('jK,ijKa->ia', loc, oocv)
                                 +np.einsum('jk,kija->ia', dm_oo, ooov)
                                 -np.einsum('bc,icba->ia', dm_vv, ovvv)
                                 +np.einsum('lKJa,lKiJ->ia', DM_occv, ococ)
                                 +np.einsum('JaKb,iJKb->ia', DM_cvcv, occv)
                                 +0.5*np.einsum('jabc,ijbc->ia',
                                                 DM_ovvv, oovv)
                                 -0.5*np.einsum('JKab,JKib->ia',
                                                 DM_ccvv, ccov)
                                 -np.einsum('jKab,jKib->ia',
                                             DM_ocvv, ocov)
                                 -0.5*np.einsum('jkab,jkib->ia',
                                                 DM_oovv, ooov)
                                 -np.einsum('jcab,ibjc->ia', DM_ovvv, ovov)
                                 -np.einsum('iJKb,JaKb->ia', DM_occv, cvcv)
                                 -0.5*np.einsum('iJbc,Jabc->ia',
                                                 DM_ocvv, cvvv)
                                 -0.5*np.einsum('ijbc,jabc->ia',
                                                 DM_oovv, ovvv)
                                 +0.5*np.einsum('ibcd,abcd->ia',
                                                 DM_ovvv, vvvv)
                                 -0.5*np.einsum('abcd,ibcd->ia',
                                                 DM_vvvv, ovvv)
                                 +np.einsum('jakb,ijkb->ia', DM_ovov, ooov)
                                 +np.einsum('ibjc,jcab->ia', DM_ovov, ovvv)
                                 -np.einsum('iJkL,kLJa->ia', DM_ococ, occv)
                                     )

        self.iter_count = 0
        def matvec(x):
            """
            Function to carry out the Ax matrix vector product
            to be used by the conjugate gradient solver.     
            """
            lfv = x.reshape(nfull,nvirt)
            lcv = lfv[:ncore,:]
            lov = lfv[ncore:,:]
            Alcv = (  np.einsum('Jb,IbJa->Ia', lcv, cvcv)
                    - np.einsum('Jb,IJab->Ia', lcv, ccvv)
                    + np.einsum('jb,jaIb->Ia', lov, ovcv)
                    + np.einsum('jb,jIab->Ia', lov, ocvv)
                    )
                     
            Alov = (  np.einsum("jb,ibja->ia", lov, ovov)
                    - np.einsum("jb,ijab->ia", lov, oovv)
                    + np.einsum("Jb,ibJa->ia", lcv, ovcv)
                    - np.einsum("Jb,iJab->ia", lcv, ocvv)
                    )
            Ax = np.zeros((nfull,nvirt))
            Ax[:ncore,:] = Alcv
            Ax[ncore:,:] = Alov 
            Ax += lfv*ecov
            self.iter_count += 1
 
            return Ax.reshape(nfull*nvirt)


        def precond(x):
            """
            Function to define the preconditioner for conjugate gradient
            """
            M = x.reshape(nfull,nvirt) / ecov
            return M.reshape(nfull*nvirt)

        A = linalg.LinearOperator((nfull*nvirt, nfull*nvirt),
                                   matvec=matvec)
        M = linalg.LinearOperator((nfull*nvirt, nfull*nvirt),
                                   matvec=precond)

        rhs = np.reshape(rhs_fv, (nfull*nvirt))
        guess = (rhs_fv / ecov).reshape(nfull*nvirt)

        lfv, w = linalg.cg(A=A, b=rhs, x0=guess, tol=1e-8,
                           maxiter=max_iterations, M=M,
                           callback=None, atol=None)

        if w == 0:
            print("Conjugate gradient converged in %d iterations." % self.iter_count)
            l_fv = np.reshape(lfv, (nfull, nvirt))
            self.matrix["CV"] = l_fv[:ncore,:]
            self.matrix["OV"] = l_fv[ncore:nfull,:]
        else:
            raise Warning("Conjugate gradient did not converge.", w)
    #END CVS-ADC2x ################################

class O_Multipliers(OR_Multipliers):
    def compute_blocks(self, blocks, L, dm1, DM2, lev=False, verbose=False):
        """
        Computes all blocks in array blocks

        Parameters:
        ----------
        blocks: array continingthe names of the blocks to
                be computed
        L: L-type Lagrange Multiplier (L_Multiplier)
        dm1: one particle density matrix
        DM2: two particle density matrix
        """
        #print("Computing the Omega multipliers... ", blocks)
        start = time.time()
        if lev:
            for block in blocks:
                self.compute_block_levchenko(block, L, dm1, DM2)
        else:
            for block in blocks:
                self.compute_block(block, L, dm1, DM2)
        stop = time.time()
        if verbose:
            print("OMEGA    took %10.5f s." % (stop-start))

    def compute_block(self, block_type, L, dm1, DM2):
        """Computes the omega Lagrange multipliers required
        for orbital response
        
        Parameters:
        ----------
        block_type: the block of the Lagrange multiplier to be computed
                    ("CC", "OC", "CV", "OO", "OV", "VV")
        L: Lambda Lagrange multipliers; pre-requisite for computing Omega.
        """
        #Check if block exists; if not, try to initialize
        if block_type not in self.blocks:
                self.add_block(block_type)
        
        if self.order in ["cvs0", "cvs-adc0"]:
            if block_type == "CC":
                self.compute_cvs0_CC(L, dm1, DM2)
            elif block_type == "OO":
                self.compute_cvs0_OO(L, dm1, DM2)
            elif block_type == "VV":
                self.compute_cvs0_VV(L, dm1, DM2)
            elif block_type == "OC":
                self.compute_cvs0_OC(L, dm1, DM2)
            elif block_type == "CV":
                self.compute_cvs0_CV(L, dm1, DM2)
            elif block_type == "OV":
                self.compute_cvs0_OV(L, dm1, DM2)
            else:
                errtxt = ": Unrecognized block type, "
                raise ValueError(self.order, errtxt, block_type)

        elif self.order in ["0", "adc0"]:
            if block_type == "OO":
                self.compute_0d_OO(L, dm1, DM2)
            elif block_type == "VV":
                self.compute_0d_VV(L, dm1, DM2)
            elif block_type == "OV":
                self.compute_0d_OV(L, dm1, DM2)
            else:
                errtxt = ": Unrecognized block type, "
                raise ValueError(self.order, errtxt, block_type)
        
        elif self.order in ["cvs-adc1", "cvs1"]:
            if block_type == "CC":
                self.compute_cvs1_CC(L, dm1, DM2)
        
            elif block_type == "OO":
                self.compute_cvs1_OO(L, dm1, DM2)
        
            elif block_type == "VV":
                self.compute_cvs1_VV(L, dm1, DM2)
        
            elif block_type == "OC":
                self.compute_cvs1_OC(L, dm1, DM2)
        
            elif block_type == "OV":
                self.compute_cvs1_OV(L, dm1, DM2)
        
            elif block_type == "CV":
                self.compute_cvs1_CV(L, dm1, DM2)
        
            else:
                errtxt = ": Unrecognized block type, "
                raise ValueError(self.order, errtxt, block_type)
        
        elif self.order in ["adc1", "1"]:
            if block_type == "OO":
                self.compute_1_OO(L, dm1, DM2)
        
            elif block_type == "VV":
                self.compute_1_VV(L, dm1, DM2)
        
            elif block_type == "OV":
                self.compute_1_OV(L, dm1, DM2)

            elif block_type == "VO":
                self.compute_1_VO(L, dm1, DM2)
        
            else:
                errtxt = ": Unrecognized block type, "
                raise ValueError(self.order, errtxt, block_type)
        
        elif self.order in ["cvs-adc2", "cvs2"]:
            if block_type == "CC":
                self.compute_cvs2_CC(L, dm1, DM2)
        
            elif block_type == "OO":
                self.compute_cvs2_OO(L, dm1, DM2)
        
            elif block_type == "VV":
                self.compute_cvs2_VV(L, dm1, DM2)
        
            elif block_type == "OC":
                self.compute_cvs2_OC(L, dm1, DM2)
        
            elif block_type == "OV":
                self.compute_cvs2_OV(L, dm1, DM2)
        
            elif block_type == "CV":
                self.compute_cvs2_CV(L, dm1, DM2)
            else:
                errtxt = ": Unrecognized block type, "
                raise ValueError(self.order, errtxt, block_type)

        elif self.order in ["cvs-adc2x", "cvs2x"]:
            if block_type == "CC":
                self.compute_cvs2x_CC(L, dm1, DM2)
        
            elif block_type == "OO":
                self.compute_cvs2x_OO(L, dm1, DM2)
        
            elif block_type == "VV":
                self.compute_cvs2x_VV(L, dm1, DM2)
        
            elif block_type == "OC":
                self.compute_cvs2x_OC(L, dm1, DM2)
        
            elif block_type == "OV":
                self.compute_cvs2x_OV(L, dm1, DM2)
        
            elif block_type == "CV":
                self.compute_cvs2x_CV(L, dm1, DM2)
            else:
                errtxt = ": Unrecognized block type, "
                raise ValueError(self.order, errtxt, block_type)


        elif self.order in ['gs_mp2']:
            if block_type == "OO":
                self.compute_mp2_OO(L, dm1, DM2)
            elif block_type == "VV":
                self.compute_mp2_VV(L, dm1, DM2)
            elif block_type == "OV":
                self.compute_mp2_OV(L, dm1, DM2)
            elif block_type == "VO":
                self.compute_mp2_VO(L, dm1, DM2)
            else:
                errtxt = ": Unrecognized block type, "
                raise ValueError(self.order, errtxt, block_type) 
        
        else:
            raise ValueError(self.order, "Not implemented.")

    def compute_block_levchenko(self, block_type, L, dm1, DM2):
        """Computes the omega Lagrange multipliers required
        for orbital response;
        uses the equations derived by Levchenko -- see PhD thesis
        
        Parameters:
        ----------
        block_type: the block of the Lagrange multiplier to be computed
                    ("CC", "OC", "CV", "OO", "OV", "VV")
        L: Lambda Lagrange multipliers; pre-requisite for computing Omega.
        """
        #Check if block exists; if not, try to initialize
        if block_type not in self.blocks:
                self.add_block(block_type)
        
        if self.order in ["0", "adc0", "1", "adc1"]:
            if block_type == "OO":
                self.compute_levchenko_OO(L, dm1, DM2)
            elif block_type == "VV":
                self.compute_levchenko_VV(L, dm1, DM2)
            elif block_type == "OV":
                self.compute_levchenko_OV(L, dm1, DM2)
            else:
                errtxt = ": Unrecognized block type, "
                raise ValueError(self.order, errtxt, block_type)
        else:
            errtxt = "Levchenko equations not implemented at this order."
            raise ValueError(self.order, errtxt)


    ##########################
    ####### CVS-ADC(1) #######
    #######            #######
    def compute_cvs1_CC(self, L, dm1, DM2):
        #Computes Omega_CC, CVS-ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        cccc = self.reference_state.eri("o2o2o2o2").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()


        #1.2 Density matrices:
        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required CC and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #pre_fact = 0.25
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L multiplier blocks
        if "OC" in L.blocks and "OV" in L.blocks and "CV" in L.blocks:
            loc = L.matrix["OC"]
            lov = L.matrix["OV"]
            lcv = L.matrix["CV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OC, OV and CV blocks."
            raise ValueError(errtxt, L.blocks)

        #2. Compute CC multipliers
        ncore = fcc.shape[0]
        self.matrix["CC"] = -1.0*fcc #- dm_cc*fcc

        for I in range(ncore):
            self.matrix["CC"][I,:] += -dm_cc[I,:]*fcc[I,I]

        self.matrix["CC"] += ( -np.einsum("KL,KILJ->IJ", dm_cc, cccc)
                               -np.einsum("kL,kILJ->IJ", loc, occc)
                               -np.einsum("kL,kJLI->IJ", loc, occc)
                               +np.einsum("Kb,KIJb->IJ", lcv, cccv)
                               +np.einsum("Kb,KJIb->IJ", lcv, cccv)
                               +np.einsum("kb,kIJb->IJ", lov, occv)
                               +np.einsum("kb,kJIb->IJ", lov, occv)
                               -np.einsum("bc,IbJc->IJ", dm_vv, cvcv)
                               -np.einsum("JbKc,IbKc->IJ", DM_cvcv, cvcv)
                                )
        

    def compute_cvs1_OO(self, L, dm1, DM2): #DM2 not required here
        #Computes Omega_OO CVS-ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        oooc = self.reference_state.eri("o1o1o1o2").to_ndarray()
        coov = self.reference_state.eri("o2o1o1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required CC and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #1.3 L multiplier blocks:
        if "OC" in L.blocks and "OV" in L.blocks and "CV" in L.blocks:
            loc = L.matrix["OC"]
            lov = L.matrix["OV"]
            lcv = L.matrix["CV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OC, OV and CV blocks."
            raise ValueError(errtxt, L.blocks)

        #2. Compute:    

        self.matrix["OO"] = -1.0*foo

        self.matrix["OO"] += ( -np.einsum("KL,iKjL->ij", dm_cc, ococ)
                               +np.einsum("kL,kijL->ij", loc, oooc)
                               +np.einsum("kL,kjiL->ij", loc, oooc)
                               +np.einsum("Kb,Kijb->ij", lcv, coov)
                               +np.einsum("Kb,Kjib->ij", lcv, coov)
                               +np.einsum("kb,kijb->ij", lov, ooov)
                               +np.einsum("kb,kjib->ij", lov, ooov)
                               -np.einsum("bc,ibjc->ij", dm_vv, ovov) 
                                ) 

    def compute_cvs1_VV(self, L, dm1, DM2):
        #Computes Omega_VV, CVS-ADC(1)
        #1. Pre-requisites
        #1.1 Fock and Eri matrices
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        #1.2 Density matrices:
        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #pre_fact = 0.25
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        #2. Compute:
        nvirt = fvv.shape[0]

        for a in range(nvirt):
            self.matrix["VV"][a,:] = -dm_vv[a,:]*fvv[a,a]

        self.matrix["VV"] += -np.einsum('JbKc,JaKc->ab', DM_cvcv,
                                          cvcv, optimize=True) 

    def compute_cvs1_OC(self, L, dm1, DM2):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ccco = self.reference_state.eri("o2o2o2o1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        oocc = self.reference_state.eri("o1o1o2o2").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        ccov = self.reference_state.eri("o2o2o1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()

        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required CC amd VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #pre_fact = 0.25
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2PDM does not contain the required CVCV blocks."
            raise ValueError(errtxt, DM2.blocks)

        if "OC" in L.blocks and "OV" in L.blocks and "CV" in L.blocks:
            loc = L.matrix["OC"]
            lov = L.matrix["OV"]
            lcv = L.matrix["CV"]
        else:
            errtxt = "L multiplier does not contain the required"
            errtxt += " OC, OV, and CV blocks."
            raise ValueError(errtxt, L.blocks)

        nocc = foo.shape[0]

        for i in range(nocc):
            self.matrix["OC"][i,:] = -loc[i,:]*foo[i,i]

        self.matrix["OC"] += ( -np.einsum("KL,LJKi->iJ", dm_cc, ccco)
                               -np.einsum("bc,ibJc->iJ", dm_vv, ovcv)
                               -np.einsum("kL,kiLJ->iJ", loc, oocc)
                               +np.einsum("kL,iLkJ->iJ", loc, ococ)
                               +np.einsum("Kb,KJib->iJ", lcv, ccov)
                               -np.einsum("Kb,iKJb->iJ", lcv, occv)
                               +np.einsum("kb,kiJb->iJ", lov, oocv)
                               +np.einsum("kb,kJib->iJ", lov, ocov)
                               -np.einsum("JbKc,ibKc->iJ", DM_cvcv, ovcv)
                                )

    def compute_cvs1_OV(self, L, dm1, DM2):
        #dm1 and DM2 not required here
        #Computes Omega_OV, CVS-ADC(1)
        #1. Pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()

        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        #pre_fact = 0.25
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2PDM does not contain the required CVCV block."
            raise ValueError(errtxt, DM2.blocks)

        nocc = foo.shape[0] 

        for i in range(nocc):
            self.matrix["OV"][i,:] = -foo[i,i] * lov[i,:]

        self.matrix["OV"] += np.einsum("JaKc,iJKc->ia", DM_cvcv, occv)

    def compute_cvs1_CV(self, L, dm1, DM2):
        #pre-requisites:
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()

        if "CV" in L.blocks:
            lcv = L.matrix["CV"]
        else:
            errtxt = "L multiplier does not contain the required CV block."
            raise ValueError(errtxt, L.blocks)

        #pre_fact=0.25
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            errtxt = "2PDM does not contain the required CVCV blocks."
            raise ValueError(errtxt, DM2.blocks)

        ncore = fcc.shape[0]

        for I in range(ncore):
            self.matrix["CV"][I,:] = -lcv[I,:]*fcc[I,I]

        self.matrix["CV"] += np.einsum("JaKc,IJKc->Ia", DM_cvcv, cccv)  
        

    ######################
    ####### ADC(0) #######
    #######        ####### 
    def compute_0_OO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm0_oo = dm1.matrix["OO"]
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        if "OOOO" in DM2.blocks:
            Dm1_oooo = DM2.matrix["OOOO"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OOOO block."
            raise ValueError(errtxt, DM2.blocks)
        
        #2. Compute Omega OO block:
        nocc = foo.shape[0]
        nvirt = ovov.shape[1]

        for i in range(nocc):
            self.matrix["OO"][i,:] = -foo[i,i]*(dm0_oo[i,:])
            #for j in range(nocc):
            #self.matrix["OO"][i,j] = -foo[i,i]*(dm0_oo[i,j] + loo[i,j])

        self.matrix["OO"] += ( - np.einsum('kl,kilj->ij', dm0_oo,
                                            oooo, 
                                            optimize=True)
                               - np.einsum('ab,iajb->ij', dm0_vv,
                                            ovov,
                                            optimize=True)
                               - 0.5*np.einsum('jklm,iklm->ij',
                                                Dm1_oooo, oooo,
                                                optimize=True)
                            )
    def compute_0_VV(self, L, dm1, DM2):
        #Computes Omega_VV, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fvv = self.reference_state.fock("v1v1").to_ndarray()

        #1.2 Density matrices:
        if "VV" in dm1.blocks:
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        #2. Compute Omega VV block:
        nvirt = fvv.shape[0]

        for a in range(nvirt):
            self.matrix["VV"][a,:] = -fvv[a,a]*(dm0_vv[a,:])

    def compute_0_OV(self, L, dm1, DM2):
        pass

    ######################
    ####### ADC(0) #######
    ####### PLAN B ####### 
    def compute_0b_OO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        dhf_oo = np.zeros(foo.shape)
        np.fill_diagonal(dhf_oo, 1.0)
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm0_oo = dm1.matrix["OO"]
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #2. Compute Omega OO block:
        nocc = foo.shape[0]
        nvirt = ovov.shape[1]

        for i in range(nocc):
            self.matrix["OO"][i,:] = -foo[i,i]*(dm0_oo[i,:]+dhf_oo[i,:])

        self.matrix["OO"] += ( - np.einsum('kl,kilj->ij', dm0_oo, oooo, 
                                            optimize=True)
                               - np.einsum('ab,iajb->ij', dm0_vv, ovov,
                                            optimize=True)
                            )

    def compute_0b_VV(self, L, dm1, DM2):
        #Computes Omega_VV, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fvv = self.reference_state.fock("v1v1").to_ndarray()

        #1.2 Density matrices:
        if "VV" in dm1.blocks:
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        #2. Compute Omega VV block:
        nvirt = fvv.shape[0]

        for a in range(nvirt):
            self.matrix["VV"][a,:] = -fvv[a,a]*(dm0_vv[a,:])

    def compute_0b_OV(self, L, dm1, DM2):
        pass

    def compute_0b_VO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()

        #1.2 Density matrices:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm0_oo = dm1.matrix["OO"]
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        self.matrix["VO"] = (   np.einsum('bc,icba->ai', dm0_vv, ovvv, 
                                            optimize=True)
                               - np.einsum('kl,lika->ai', dm0_oo, ooov,
                                            optimize=True)
                             )

    ######################
    ####### ADC(0) #######
    ####### PLAN C ####### 
    def compute_0c_OO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        #print(lov.shape, ooov.shape)
        #2. Compute Omega OO block:
        self.matrix["OO"] = ( np.einsum('ikjk->ij', oooo, optimize=True) 
                            - np.einsum('kb,ikjb->ij', lov, ooov, optimize=True)
                            - np.einsum('kb,jkib->ij', lov, ooov, optimize=True)
                            )
        #Clean up:
        #TODO: del oooo, ooov, lov

    def compute_0c_VV(self, L, dm1, DM2):
        pass

    def compute_0c_OV(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()

        #1.2 Density matrices:
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        nocc = foo.shape[0]
        for i in range(nocc):
            self.matrix["OV"][i,:] = -foo[i,i]*lov[i,:] 
    

    def compute_0c_VO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()

        #1.2 Density matrices:
        if "OV" in L.blocks:
            lvo = L.matrix["OV"].T
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        nvirt = fvv.shape[0]
        for a in range(nvirt):
            self.matrix["VO"][a,:] = -fvv[a,a]*lvo[a,:]

        self.matrix["VO"] += (-np.einsum('ikka->ai', ooov, optimize=True)
                              +np.einsum('bk,kaib->ai', lvo,
                                          ovov, optimize=True)
                              +np.einsum('bk,ikba->ai', lvo,
                                          oovv, optimize=True)
                             )
        #TODO:del oovv, ovov, ooov, fvv, lvo

    ######################
    ####### ADC(0) #######
    ####### PLAN D ####### 
    def compute_0d_OO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        dhf_oo = np.zeros(foo.shape)
        np.fill_diagonal(dhf_oo, 1.0)
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm0_oo = dm1.matrix["OO"]
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega OO block:
        nocc = foo.shape[0]
        nvirt = ovov.shape[1]

        for i in range(nocc):
            self.matrix["OO"][i,:] = -foo[i,i]*(dm0_oo[i,:]+dhf_oo[i,:])

        self.matrix["OO"] += ( - np.einsum('kl,kilj->ij', dm0_oo, oooo, 
                                            optimize=True)
                               - np.einsum('ab,iajb->ij', dm0_vv, ovov,
                                            optimize=True)
                               - np.einsum('kb,ikjb->ij', lov, ooov,
                                            optimize=True)
                               - np.einsum('kb,jkib->ij', lov, ooov,
                                            optimize=True)
                            )

    def compute_0d_VV(self, L, dm1, DM2):
        #Computes Omega_VV, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fvv = self.reference_state.fock("v1v1").to_ndarray()

        #1.2 Density matrices:
        if "VV" in dm1.blocks:
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        #2. Compute Omega VV block:
        nvirt = fvv.shape[0]

        for a in range(nvirt):
            self.matrix["VV"][a,:] = -fvv[a,a]*(dm0_vv[a,:])

    def compute_0d_OV(self, L, dm1, DM2):
        #Pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain the"
            errtxt += " required OV block."
            raise ValueError(errtxt, L.blocks)
        self.matrix["OV"] = -np.einsum('ii,ia->ia', foo, lov)

    def compute_0d_VO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(0)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()

        #1.2 Density matrices:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm0_oo = dm1.matrix["OO"]
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain the"
            errtxt += " required OV block."
            raise ValueError(errtxt, L.blocks)


        self.matrix["VO"]=(-np.einsum('aa,ia->ai',fvv,lov,optimize=True)
                           -np.einsum('kl,lika->ai',dm0_oo,ooov,optimize=True)
                           +np.einsum('bc,icba->ai',dm0_vv,ovvv,optimize=True)
                           +np.einsum('kb,kaib->ai',lov,ovov,optimize=True)
                           -np.einsum('kb,kiba->ai',lov,oovv,optimize=True)
                         )
        

    #### MP(2) ####
    def compute_mp2_OO(self, L, dm1, DM2):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()

        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm1_oo = dm1.matrix["OO"]
            dm1_vv = dm1.matrix["VV"]
        else:
            errtxt = "1DM does not contain the required "
            errtxt += "OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #if "OO" in L.blocks and "VV" in L.blocks:
        #    loo = L.matrix["OO"]
        #    lvv = L.matrix["VV"]
        #else:
        #    errtxt = "Lagrange multiplier does not contain the required "
        #    errtxt += "OO and VV blocks."
        #    raise ValueError(errtxt, L.blocks)

        if "OOVV" in DM2.blocks:
            DM2_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2DM does not contain the required "
            errtxt += "OOVV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L Multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        nocc = foo.shape[0]
 
        #Compute:
        #-\delta_ij\epsilon_i
        #np.fill_diagonal(self.matrix["OO"],-np.diag(foo))
        
        #-\gamma^a_ij \epsilon_i

        self.matrix["OO"] = -foo.copy()     
        for i in range(nocc):
            #for j in range(nocc):
            self.matrix["OO"][i,:] += -foo[i,i]*dm1_oo[i,:]

        #The rest:
        self.matrix["OO"] += ( -np.einsum('kl,kilj->ij', dm1_oo, oooo)
                               -np.einsum('bc,ibjc->ij', dm1_vv, ovov)
                               -np.einsum('kb,ikjb->ij', lov, ooov)
                               -np.einsum('kb,jkib->ij', lov, ooov)
                               -0.5*np.einsum('jkab,ikab->ij', DM2_oovv, oovv)
                                )
        # print("\nThe 2PDM contribution, OO block\n")
        # print(-0.5*np.einsum('jkab,ikab->ij', DM2_oovv, oovv))
        # print()

    def compute_mp2_VV(self, L, dm1, DM2):
        #pre-requisites
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()

        if "VV" in dm1.blocks:
            dm1_vv = dm1.matrix["VV"]
        else:
            errtxt = "1DM does not contain the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        if "OOVV" in DM2.blocks:
            DM2_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2DM does not contain the required OOVV block."
            raise ValueError(errtxt, DM2.blocks)

        #if "VV" in L.blocks:
        #    lvv = L.matrix["VV"]
        #else:
        #    errtxt = "Lagrange multiplier does not contain the required "
        #    errtxt += "VV block."
        #    raise ValueError(errtxt, L.blocks)

        nvirt = fvv.shape[0]
        for a in range(nvirt):
        #    for b in range(nvirt):
            self.matrix["VV"][a,:] = -fvv[a,a]*dm1_vv[a,:]
        
        self.matrix["VV"] += -0.5*np.einsum('ijbc,ijac->ab', DM2_oovv, oovv)
        
    def compute_mp2_OV(self, L, dm1, DM2):
        #Get pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()

        if "OOVV" in DM2.blocks:
            DM2_oovv = DM2.matrix["OOVV"]
        else:
            errtxt = "2DM does not contain required"
            errtxt += " OOVV block."
            raise ValueError(errtxt, DM2.blocks)
    
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L Multipier does not contain the "
            errtxt += "required OV block."
            raise ValueError(errtxt, L.blocks)
    
        nocc = foo.shape[0]
        nvirt = fvv.shape[0]
        for i in range(nocc):
            self.matrix["OV"][i,:] = -foo[i,i]*lov[i,:]

        self.matrix["OV"] += -0.5*np.einsum('jkab,jkib->ia', DM2_oovv, ooov)

    def compute_mp2_VO(self, L, dm1, DM2):
        pass

    def compute_levchenko_OO(self, L, dm1, DM2):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required OO and VV blocks."
            raise ValueError(errtxt, dm1.block)

        if "OO" in L.blocks and "VV" in L.blocks and "OV" in L.blocks:
            loo = L.matrix["OO"]
            lvv = L.matrix["VV"]
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain the required "
            errtxt += "OO, VV and OV blocks."
            raise ValueError(errtxt, L.blocks)

        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2PDM does not contain the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)

        #compute multipliers:
        eps_ij = -foo.copy()
        for i in range(foo.shape[0]):
            for j in range(foo.shape[1]):
                eps_ij[i,j] += -foo[i,i]*(dm_oo[i,j]+loo[i,j])
 
        self.matrix["OO"] = (  eps_ij
                              -np.einsum('jakb,iakb->ij', DM_ovov, ovov)
                              -np.einsum('ka,ikja->ij', lov, ooov)
                              -np.einsum('ka,jkia->ij', lov, ooov)
                              -np.einsum('kl,ikjl->ij', loo+dm_oo, oooo)
                              -np.einsum('ab,iajb->ij', lvv+dm_vv, ovov)
                            )

    def compute_levchenko_VV(self, L, dm1, DM2):
        #pre-requisites:
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
       
        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required VV block."
            raise ValueError(errtxt)

        if "VV" in L.blocks:
            lvv = L.matrix["VV"]
        else:
            errtxt = "L multiplier does not contain the required VV block."
            raise ValueError(errtxt)

        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2PDM does not contain the required OVOV block."
            raise ValueError(errtxt)

        #compute:
        self.matrix["VV"] = ( -np.einsum('aa,ab->ab', fvv, dm_vv + lvv)
                              -np.einsum('ibjc,iajc->ab', DM_ovov, ovov)
                            )

    def compute_levchenko_OV(self, L, dm1, DM2):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain the required OV block."
            raise ValueError(errtxt)

        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2PDM does not contain the required OVOV block."
            raise ValueError(errtxt)

        #Compute:
        self.matrix["OV"] = ( -np.einsum('ii,ia->ia',foo,lov)
                              +np.einsum('jakb,ijkb->ia', DM_ovov, ooov)
                            ) 
 
            
    ######################
    ##### CVS-ADC(0) #####
    #####            #####
    def compute_cvs0_CC(self, L, dm1, DM2):
        #computes the CC block of omega, CVS-ADC(1)
        #pre-requisites:
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        cccc = self.reference_state.eri("o2o2o2o2").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        
        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required CC and VV bloks."
            raise ValueError(errtxt, dm1.blocks)

        if "OV" in L.blocks and "CV" in L.blocks:
            lov = L.matrix["OV"]
            lcv = L.matrix["CV"]
        else:
            errtxt = "L multiplier does not contain the required OV"
            errtxt += "and CV blocks."
            raise ValueError(errtxt, L.blocks)

        ncore = fcc.shape[0]

        #compute omega:
        self.matrix["CC"] = -fcc.copy()
        for I in range(ncore):
            self.matrix["CC"][I,:] += -fcc[I,I]*dm_cc[I,:]

        self.matrix["CC"] += ( -np.einsum("ab,IaJb->IJ", dm_vv, cvcv)
                               -np.einsum("KL,IKJL->IJ", dm_cc, cccc)
                               +np.einsum("ka,kJIa->IJ", lov, occv)
                               +np.einsum("ka,kIJa->IJ", lov, occv)
                               +np.einsum("Ka,KJIa->IJ", lcv, cccv)
                               +np.einsum("Ka,KIJa->IJ", lcv, cccv)
                              )

    def compute_cvs0_OO(self, L, dm1, DM2):
        #compute the OO block of omega, CVS-ADC(1)
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        
        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required CC and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        if "OV" in L.blocks and "CV" in L.blocks:
            lov = L.matrix["OV"]
            lcv = L.matrix["CV"]
        else:
            errtxt = "L Multiplier does not contain the required OV "
            errtxt += "and CV blocks."
            raise ValueError(errtxt, L.blocks)

        nocc = foo.shape[0]

        #compute
        self.matrix["OO"] = -foo.copy()
        self.matrix["OO"] += ( -np.einsum("KL,iKjL->ij", dm_cc, ococ)
                               -np.einsum("ab,iajb->ij", dm_vv, ovov)
                               -np.einsum("ka,ikja->ij", lov, ooov)
                               -np.einsum("ka,jkia->ij", lov, ooov)
                               -np.einsum("Ka,iKja->ij", lcv, ocov)
                               -np.einsum("Ka,jKia->ij", lcv, ocov)
                             )       

    def compute_cvs0_VV(self, L, dm1, DM2):
        #computes the VV block of the omega Lagrange multiplier
        #pre-requisites
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required VV block."
            raise ValueError(errtxt)

        nvirt = fvv.shape[0]
        #compute:
        for a in range(nvirt):
            self.matrix["VV"][a,:] = -fvv[a,a]*dm_vv[a,:]

    def compute_cvs0_OC(self, L, dm1, DM2):
        #pre-requisites:
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ccov = self.reference_state.eri("o2o2o1v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        coov = self.reference_state.eri("o2o1o1v1").to_ndarray()

        #oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        #ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        #cvov = self.reference_state.eri("o2v1o1v1").to_ndarray()
        #occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        #ccov = self.reference_state.eri("o2o2o1v1").to_ndarray()

        if "CC" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1PDM does not contain the required CC and VV blocks."
            raise ValueError(errtxt)

        if "OV" in L.blocks and "CV" in L.blocks:
            lov = L.matrix["OV"]
            lcv = L.matrix["CV"]
        else:
            errtxt = "L multiplier does not contain the required OV"
            errtxt += " and CV blocks."
            raise ValueError(errtxt)

        self.matrix["OC"] = ( np.einsum("KL,iKLJ->iJ", dm_cc, occc)
                             -np.einsum("ab,iaJb->iJ", dm_vv, ovcv)
                             -np.einsum("Ka,iKJa->iJ", lcv, occv)
                             -np.einsum("Ka,JKia->iJ", lcv, ccov)
                             -np.einsum("ka,ikJa->iJ", lov, oocv)
                             -np.einsum("ka,Jkia->iJ", lov, coov)
                             #np.einsum("KL,iKLJ->iJ", dm_cc, occc)
                             #-np.einsum("bc,Jcib->iJ", dm_vv, cvov)
                             #-np.einsum("kb,ikJB->iJ", lov, oocv)
                             #+np.einsum("kb,kJib->iJ", lov, ocov)
                             #-np.einsum("Kb,iKJB->iJ", lcv, occv)
                             #+np.einsum("Kb,KJib->iJ", lcv, ccov)
                            )

    def compute_cvs0_CV(self, L, dm1, DM2):
        #pre-requisites
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        if "CV" in L.blocks:
            lcv = L.matrix["CV"]
        else:
            errtxt = "L Multiplier does not contain the required CV block."
            raise ValueError(errtxt)

        ncore = fcc.shape[0]
        for I in range(ncore):
            self.matrix["CV"][I,:] = -fcc[I,I]*lcv[I,:]


    def compute_cvs0_OV(self, L, dm1, DM2):
        #pre-requisites:
        foo = self.reference_state.fock("o1o1").to_ndarray()

        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L Multiplier does not contain the required OV block."
            raise ValueError(errtxt)

        nocc = foo.shape[0]
        for i in range(nocc):
            self.matrix["OV"][i,:] = -foo[i,i]*lov[i,:]

    ######################
    ####### ADC(1) #######
    #######        ####### 
    def compute_1_OO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        #print()
        #print("OVOV:")
        #print(ovov)
        #print()

        #1.2 Density matrices:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm0_oo = dm1.matrix["OO"]
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #pre_fact = 0.25
        if "OVOV" in DM2.blocks:
            Dm1_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)

        #if "OOOO" in DM2.blocks:
        #    Dm1_oooo = DM2.matrix["OOOO"]
        #else:
        #    errtxt = "2 particle density matrix does not contain "
        #    errtxt += "the required OOOO block."
        #    raise ValueError(errtxt, DM2.blocks)
        
        #1.3 L blocks:
        if "OO" in L.blocks and "OV" in L.blocks and "VV" in L.blocks:
            loo = L.matrix["OO"]
            lov = L.matrix["OV"]
            lvv = L.matrix["VV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OO, VV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega OO block:
        nocc = foo.shape[0]
        nvirt = ovov.shape[1]

        self.matrix["OO"] = -foo.copy()
        for i in range(nocc):
            self.matrix["OO"][i,:] += -foo[i,i]*(dm0_oo[i,:] )#+ loo[i,:])

        # print("\nepsilon_i*gamma_ij")
        # print(self.matrix["OO"])
        # print()
        # print("1PDM")
        # print(dm0_oo)
        

        self.matrix["OO"] += ( - np.einsum('kl,kilj->ij', dm0_oo+loo,
                                            oooo, 
                                            optimize=True)
                               - np.einsum('ab,iajb->ij', dm0_vv+lvv,
                                            ovov,
                                            optimize=True)
                               + np.einsum('ka,kija->ij', lov, ooov,
                                            optimize=True)
                               + np.einsum('ka,kjia->ij', lov, ooov,
                                            optimize=True)
                               #- 0.5*np.einsum('jklm,iklm->ij',
                               #                 Dm1_oooo, oooo,
                               #                 optimize=True)
                               - np.einsum('jbkc,ibkc->ij', Dm1_ovov,
                                            ovov, optimize=True)
                            )
        # print("\n-epsilon_i*gamma_ij-2PDM contribution-1PDM contribution-lambda")
        # print(self.matrix["OO"])
        # print()
        #print("This is it!")
        #print(self.matrix["OO"])
        #print()

    
    def compute_1_VV(self, L, dm1, DM2):
        #Computes Omega_VV, ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "VV" in dm1.blocks:
            dm0_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        #pre_fact = 0.25
        if "OVOV" in DM2.blocks:
            Dm1_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L blocks:
        if "VV" in L.blocks:
            lvv = L.matrix["VV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required VV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega_VV block:
        nvirt = fvv.shape[0]
        nocc = ovov.shape[0]

        for a in range(nvirt):
            self.matrix["VV"][a,:] = -fvv[a,a]*(dm0_vv[a,:] + lvv[a,:])

        # print("\nepsilon_a*gamma_ab")
        # print(self.matrix["VV"])
        # print()
        # print("\nDensity matrix")
        # print(dm0_vv)
        # print("\nFock:")
        # print(fvv)
        self.matrix["VV"] -= np.einsum('ibjc,iajc->ab', 
                                         Dm1_ovov, ovov,
                                         optimize=True)

    def compute_1_OV(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()

        #pre_fact = 0.25
        #1.2 Density matrices:
        if "OVOV" in DM2.blocks:
            Dm1_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)
    
        #1.3 L blocks:
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega_OV block:
        nocc = foo.shape[0]
        #nvirt = ooov.shape[3]

        for i in range(nocc):
            self.matrix["OV"][i,:] = -foo[i,i]*lov[i,:]
            #for a in range(nvirt):
                #self.matrix["OV"][i,a] = -foo[i,i]*lov[i,a]

        # print("\nepsilon_i*lambda_ia")
        # print(self.matrix["OV"])
        # print()
        #print("\nFock:")
        #print(foo)


        self.matrix["OV"] += np.einsum('kalc,iklc->ia', Dm1_ovov, ooov, 
                                        optimize=True)

        #3. Clean up:
        #del foo, Dm1_ovov, ooov, lov

    ##Just to check that I get the same result as OV
    def compute_1_VO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "OVOV" in DM2.blocks:
            Dm1_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)
    
        #1.3 L blocks:
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega_OV block:
        nocc = foo.shape[0]
        #nvirt = ooov.shape[3]

        for i in range(nocc):
            self.matrix["OV"][i,:] = -foo[i,i]*lov[i,:]
            #for a in range(nvirt):
                #self.matrix["OV"][i,a] = -foo[i,i]*lov[i,a]
        self.matrix["OV"] += np.einsum('jakb,ijkb->ia', Dm1_ovov, ooov, 
                                        optimize=True)

        #3. Clean up:
        #TODO:del foo, Dm1_ovov, ooov, lov

    ######################
    ####### ADC(1) #######
    ####### PLAN B for H2 / STO-3G ####### 
    def compute_1h2_OO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        if "OVOV" in DM2.blocks:
            Dm_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OOOO" in DM2.blocks:
            Dm_oooo = DM2.matrix["OOOO"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OOOO block."
            raise ValueError(errtxt, DM2.blocks)
        
        #1.3 L blocks:
        if "OO" in L.blocks and "OV" in L.blocks and "VV" in L.blocks:
            loo = L.matrix["OO"]
            lov = L.matrix["OV"]
            lvv = L.matrix["VV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OO, VV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega OO block:
        nocc = foo.shape[0]
        nvirt = ovov.shape[1]

        for i in range(nocc):
            for j in range(nocc):
                self.matrix["OO"][i,j] = -foo[i,i]*dm_oo[i,j]

        self.matrix["OO"] += ( -np.einsum('kl,kilj->ij',dm_oo,oooo)
                            -np.einsum('bc,ibjc->ij',dm_vv,ovov)
                            -0.5*np.einsum('jklm,iklm->ij',Dm_oooo,oooo)
                            -np.einsum('jakb,iakb->ij',Dm_ovov,ovov)
                                )
    
    def compute_1h2_VV(self, L, dm1, DM2):
        #Computes Omega_VV, ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "1 particle density matrix does not contain "
            errtxt += "the required VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        if "OVOV" in DM2.blocks:
            Dm_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L blocks:
        if "VV" in L.blocks:
            lvv = L.matrix["VV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required VV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega_VV block:
        nvirt = fvv.shape[0]
        for a in range(nvirt):
            for b in range(nvirt):
                self.matrix["VV"][a,b] = -fvv[a,a]*dm_vv[a,b]

        self.matrix["VV"] += -np.einsum('ibjc,iajc',Dm_ovov,ovov) 


    def compute_1h2_OV(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "OVOV" in DM2.blocks:
            Dm_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "2 particle density matrix does not contain "
            errtxt += "the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)
    
        #1.3 L blocks:
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega_OV block:
        #nocc = foo.shape[0]
        #nvirt = ooov.shape[3]
        self.matrix["OV"] = np.einsum('jakc,ijkc->ia',Dm_ovov,ooov)

    ##Just to check that I get the same result as OV
    def compute_1h2_VO(self, L, dm1, DM2):
        #Computes Omega_OO, ADC(1)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()

        #1.2 Density matrices:
        if "OVOV" in DM2.blocks:
            Dm_ovov = DM2.matrix["OVOV"]
        else:
            errtxt = "The 2 particle density matrix does not contain "
            errtxt += "the required OVOV block."
            raise ValueError(errtxt, DM2.blocks)

        if "OOOO" in DM2.blocks:
            Dm_oooo = DM2.matrix["OOOO"]
        else:
            errtxt = "The 2 particle density matrix does not contain "
            errtxt += "the required OOOO block"
            raise ValueError(errtxt, DM2.blocks)

        if "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1 particle density matrix does not contain "
            errtxt += "the required OO and VV blocks."
            raise ValueError(errtxt, dm1.blocks)
    
        #1.3 L blocks:
        if "OV" in L.blocks:
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain "
            errtxt += "the required OV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute Omega_OV block:
        #nocc = foo.shape[0]
        #nvirt = ooov.shape[3]
        self.matrix["VO"] = ( -np.einsum('kl,lika->ai',dm_oo,ooov)
                              -np.einsum('bc,icba->ai',dm_vv,ovvv)
                              -0.5*np.einsum('iklm,lmka',Dm_oooo,ooov)
                              -np.einsum('ibjc,jcab->ai',Dm_ovov,ovvv)  
                                )

        #3. Clean up:
        #del foo, Dm1_ovov, ooov, lov

    ##########################
    ####### CVS-ADC(2) #######
    #######            ####### 
    def compute_cvs2_CC(self, L, dm1, DM2):
        #Computes Omega_CC, CVS-ADC(2)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        cccc = self.reference_state.eri("o2o2o2o2").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
    
        #1.2 Density matrices:
        if "CC" in dm1.blocks and "VV" in dm1.blocks and "OO" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required "
            errtxt += "CC, OO, and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        ok = True
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False

        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False

        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False

        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False

        if not ok:
            errtxt = "2PDM does not contain the required CCVV, "
            errtxt += "OCCV, OCVV, and CVCV blocks."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L multiplier blocks
        ok = True
        ###if "CC" in L.blocks and "OO" in L.blocks and "VV" in L.blocks:
        ###    lcc = L.matrix["CC"]
        ###    lvv = L.matrix["VV"]
        ###    loo = L.matrix["OO"]
        ###else:
        ###    ok = False

        if "OC" in L.blocks and "CV" in L.blocks and "OV" in L.blocks:
            loc = L.matrix["OC"]
            lov = L.matrix["OV"]
            lcv = L.matrix["CV"]
        else:
            ok = False

        if not ok:
            errtxt = "The L multiplier does not contain the required "
            errtxt += "OC, CV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        #2. Compute CC multipliers
        #TODO: consider the situation where the core does not start at 0
        #TODO: remove commented out code
        ncore = fcc.shape[0] 

        self.matrix["CC"] = -fcc.copy()
        self.matrix["CC"] += np.einsum("II,IJ->IJ", fcc, -dm_cc)#-lcc)
        #for I in range(ncore):
        #    self.matrix["CC"][I,:] -= fcc[I,I]*(dm_cc[I,:]+lcc[I,:]) 

        self.matrix["CC"] += ( -np.einsum("KL,KILJ->IJ", dm_cc, cccc)
                               -np.einsum("kl,kIlJ->IJ", dm_oo, ococ)
                               -np.einsum("ab,IaJb->IJ", dm_vv, cvcv)
                               -np.einsum("kL,kILJ->IJ", loc, occc)
                               -np.einsum("kL,kJLI->IJ", loc, occc)
                               +np.einsum("Kb,KIJb->IJ", lcv, cccv)
                               +np.einsum("Kb,KJIb->IJ", lcv, cccv)
                               +np.einsum("kb,kIJb->IJ", lov, occv)
                               +np.einsum("kb,kJIb->IJ", lov, occv)
                               -0.5*np.einsum("JKab,IKab->IJ", DM_ccvv, ccvv)
                               -np.einsum("kJLa,kILa->IJ", DM_occv, occv)
                               -0.5*np.einsum("kJab,kIab->IJ", DM_ocvv, ocvv)
                               -np.einsum("kLJa,kLIa->IJ", DM_occv, occv)
                               -np.einsum("JaKb,IaKb->IJ", DM_cvcv, cvcv)
                                )
        #3 Clean up: TODO delete all below check if this makes any difference and 
        # clean up everything;
        #del lov, occv, ococ, cvcv,
        #del dm_vv, dm_cc, dm_oo, DM_cvcv, DM_occv

    def compute_cvs2_OO(self, L, dm1, DM2):
        #Computes Omega_OO, CVS-ADC(2)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        oooc = self.reference_state.eri("o1o1o1o2").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()

        #1.2 Density matrices:
        if "CC" in dm1.blocks and "VV" in dm1.blocks and "OO" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain required CC, OO, and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        ok = True
        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required OOVV, "
            errtxt += "OCCV, OVVV, and OCVV blocks."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L multiplier blocks
        ok = True
        ###if "CC" in L.blocks and "OO" in L.blocks and "VV" in L.blocks:
        ###    lcc = L.matrix["CC"]
        ###    loo = L.matrix["OO"]
        ###    lvv = L.matrix["VV"]
        ###else:
        ###    ok = False

        if "OC" in L.blocks and "CV" in L.blocks and "OV" in L.blocks:
            loc = L.matrix["OC"]
            lcv = L.matrix["CV"]
            lov = L.matrix["OV"]
        else:
            ok = False

        if not ok:
            errtxt = "The L multiplier does not contain the required "
            errtxt += "OC, CV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        #2. Compute OO multipliers
        nocc = foo.shape[0]

        self.matrix["OO"] = -1.0*foo
        self.matrix["OO"] += np.einsum("ii,ij->ij", foo, -dm_oo)#-loo) 
        #for i in range(nocc):
        #    self.matrix["OO"][i,:] -= foo[i,i]*(dm_oo[i,:]+loo[i,:]) 

        #print(self.matrix["OO"])
        self.matrix["OO"] += ( -np.einsum("KL,iKjL->ij", dm_cc, ococ)
                               -np.einsum("kl,ikjl->ij", dm_oo, oooo)
                               -np.einsum("ab,iajb->ij", dm_vv, ovov)
                               +np.einsum("kL,kijL->ij", loc, oooc)
                               +np.einsum("kL,kjiL->ij", loc, oooc)
                               -np.einsum("Kb,iKjb->ij", lcv, ocov)
                               -np.einsum("Kb,jKib->ij", lcv, ocov)
                               +np.einsum("kb,kijb->ij", lov, ooov)
                               +np.einsum("kb,kjib->ij", lov, ooov)
                               -np.einsum("jKLa,iKLa->ij", DM_occv, occv)
                               -0.5*np.einsum("jKab,iKab->ij", DM_ocvv, ocvv)
                               -0.5*np.einsum("jkab,ikab->ij", DM_oovv, oovv)
                               -0.5*np.einsum("jabc,iabc->ij", DM_ovvv, ovvv)
                                )
                             ###( -np.einsum('KL,iKjL->ij', dm_cc+lcc, ococ)
                             ###  -np.einsum('kl,kilj->ij', dm_oo+loo, oooo)
                             ###  +np.einsum('kL,kijL->ij', loc, oooc)
                             ###  +np.einsum('kL,kjiL->ij', loc, oooc)
                             ###  -np.einsum('Kb,iKjb->ij', lcv, ocov)
                             ###  -np.einsum('Kb,jKib->ij', lcv, ocov)
                             ###  +np.einsum('kb,kijb->ij', lov, ooov)
                             ###  +np.einsum('kb,kjib->ij', lov, ooov)
                             ###  -np.einsum('ab,iajb->ij', dm_vv+lvv, ovov)
                             ### -np.einsum('jKLa,iKLa->ij', DM_occv, occv)
                             ### -0.5*np.einsum('jKab,iKab->ij', DM_ocvv, ocvv)
                             ### -0.5*np.einsum('jkab,ikab->ij', DM_oovv, oovv)
                             ### -0.5*np.einsum('jabc,iabc->ij', DM_ovvv, ovvv)
                             ### )
        #3 Clean up:
        #TODO: delete del lov, occv, ococ, oovv, ovvv, oooo, ovov, ooov
        #TODO delete del dm_vv, dm_cc, dm_oo, DM_oovv, DM_occv, DM_ovvv

    def compute_cvs2_VV(self, L, dm1, DM2):
        #Computes Omega_VV, CVS-ADC(2)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()

        #1.2 Density matrices:
        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        ok = True
        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if not ok:
            errtxt = "The 2PDM does not contain the required "
            errtxt += "OCCV, CVCV, OVVV, CCVV, OCVV, and OOVV blocks."
            raise ValueError(errtxt, DM2.blocks) 

        #1.3 L multiplier blocks
        #if "VV" in L.blocks:
        #    lvv = L.matrix["VV"]
        #else:
        #    errtxt = "L multiplier does not contain the required VV block."
        #    raise ValueError(errtxt, L.blocks)

        #2. Compute VV multipliers
        nvirt = dm_vv.shape[0]

        self.matrix["VV"] = -np.einsum("aa,ab->ab", fvv, dm_vv)#+lvv)
        #for a in range(nvirt):
        #    self.matrix["VV"][a,:] = -fvv[a,a]*(dm_vv[a,:]+lvv[a,:]) 

        self.matrix["VV"] += ( -np.einsum("jKIb,jKIa->ab", DM_occv, occv)
                               -np.einsum("IbJc,IaJc->ab", DM_cvcv, cvcv)
                               -0.5*np.einsum("ibcd,iacd->ab", DM_ovvv, ovvv)
                               -0.5*np.einsum("IJbc,IJac->ab", DM_ccvv, ccvv)
                               -np.einsum("iJbc,iJac->ab", DM_ocvv, ocvv)
                               -0.5*np.einsum("ijbc,ijac->ab", DM_oovv, oovv)
                               -np.einsum("idbc,idac->ab", DM_ovvv, ovvv)
                                )
                             ###( -np.einsum('kJIb,kJIa->ab', DM_occv, occv,
                             ###              optimize=True)
                             ###  -np.einsum('IbJc,IaJc->ab', DM_cvcv, cvcv,
                             ###              optimize=True)
                             ###  -0.5*np.einsum('ibcd,iacd->ab', DM_ovvv, ovvv,
                             ###                  optimize=True)
                             ###  -0.5*np.einsum('IJbc,IJac->ab', DM_ccvv, ccvv,
                             ###                  optimize=True)
                             ###  -np.einsum('iJbc,iJac->ab', DM_ocvv, ocvv,
                             ###              optimize=True)
                             ###  -0.5*np.einsum('ijbc,ijac->ab', DM_oovv, oovv,
                             ###                  optimize=True)
                             ###  -np.einsum('idbc,idac->ab', DM_ovvv, ovvv,
                             ###              optimize=True)
                             ###)
        #3 Clean up:
        #TODO: del fvv, occv, oovv, ovvv, cvcv
        #TODO:del dm_vv, DM_oovv, DM_occv, DM_ovvv, DM_cvcv


    def compute_cvs2_OC(self, L, dm1, DM2):
        foo = self.reference_state.fock("o1o1").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        oocc = self.reference_state.eri("o1o1o2o2").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray() #
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ovcc = self.reference_state.eri("o1v1o2o2").to_ndarray() #
        oooc = self.reference_state.eri("o1o1o1o2").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()

        if "CC" in dm1.blocks and "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required "
            errtxt += "CC, OO, and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        ok = True

        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required "
            errtxt += "CCVV, OCCV, OCVV, and CVCV blocks."
            raise ValueError(errtxt, DM2.blocks)

        ok = True
       ### if "CC" in L.blocks and "OO" in L.blocks and "VV" in L.blocks:
       ###     lcc = L.matrix["CC"]
       ###     loo = L.matrix["OO"]
       ###     lvv = L.matrix["VV"]
       ### else:
       ###     ok = False

        if "OC" in L.blocks and "CV" in L.blocks and "OV" in L.blocks:
            loc = L.matrix["OC"]
            lcv = L.matrix["CV"]
            lov = L.matrix["OV"]
        else:
            ok = False

        if not ok:
            errtxt = "L multiplier does not contain he required "
            errtxt += "OC, CV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        nocc = foo.shape[0]

        self.matrix["OC"] = -np.einsum("ii,iJ->iJ", foo, loc)

        #for i in range(nocc):
        #    self.matrix["OC"][i,:] = -foo[i,i]*loc[i,:]

        ##TODO: !!! = Double Check!
        self.matrix["OC"] += ( np.einsum("KL,iKLJ->iJ", dm_cc, occc)
                              -np.einsum("kl,kilJ->iJ", dm_oo, oooc)
                              -np.einsum("ab,iaJb->iJ", dm_vv, ovcv)
                              +np.einsum("kL,iLkJ->iJ", loc, ococ)
                              -np.einsum("kL,kiLJ->iJ", loc, oocc)
                              +np.einsum("Kb,ibKJ->iJ", lcv, ovcc)
                              -np.einsum("Kb,iKJb->iJ", lcv, occv)
                              +np.einsum("kb,kiJb->iJ", lov, oocv)
                              +np.einsum("kb,kJib->iJ", lov, ocov)
                              -0.5*np.einsum("JKab,iKab->iJ", DM_ccvv, ocvv)
                              +np.einsum("kJLa,ikLa->iJ", DM_occv, oocv) 
                              +0.5*np.einsum("kJab,ikab->iJ", DM_ocvv, oovv)
                              -np.einsum("kLJa,kLia->iJ", DM_occv,ocov) #!!!
                              -np.einsum("JaKb,iaKb->iJ", DM_cvcv, ovcv)
                                )

    def compute_cvs2_CO(self, L, dm1, DM2):
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        oooc = self.reference_state.eri("o1o1o1o2").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray() 
        oocc = self.reference_state.eri("o1o1o2o2").to_ndarray()
        ovcc = self.reference_state.eri("o1v1o2o2").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()

        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()

        if "CC" in dm1.blocks and "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required "
            errtxt += "CC, OO, and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        ok = True

        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required "
            errtxt += "CCVV, OCCV, OCVV, and CVCV blocks."
            raise ValueError(errtxt, DM2.blocks)

        ok = True
       ### if "CC" in L.blocks and "OO" in L.blocks and "VV" in L.blocks:
       ###     lcc = L.matrix["CC"]
       ###     loo = L.matrix["OO"]
       ###     lvv = L.matrix["VV"]
       ### else:
       ###     ok = False

        if "OC" in L.blocks and "CV" in L.blocks and "OV" in L.blocks:
            loc = L.matrix["OC"]
            lcv = L.matrix["CV"]
            lov = L.matrix["OV"]
        else:
            ok = False

        if not ok:
            errtxt = "L multiplier does not contain he required"
            errtxt += "OC, CV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        ncore = fcc.shape[0]

        self.matrix["CO"] = -np.einsum("JJ,iJ->Ji", fcc, loc)

        #for i in range(nocc):
        #    self.matrix["OC"][i,:] = -foo[i,i]*loc[i,:]

        self.matrix["CO"] += ( np.einsum("KL,iLKJ->Ji", dm_cc, occc)
                              -np.einsum("kl,likJ->Ji", dm_oo, oooc)
                              -np.einsum("ab,ibJa->Ji", dm_vv, ovcv)
                              +np.einsum("kL,kJiL->Ji", loc, ococ)
                              -np.einsum("kL,kiLJ->Ji", loc, oocc)
                              +np.einsum("Kb,ibKJ->Ji", lcv, ovcc)
                              -np.einsum("Kb,iKJb->Ji", lcv, occv)
                              +np.einsum("kb,kJib->Ji", lov, ocov)
                              +np.einsum("kb,kiJb->Ji", lov, oocv)
                              -np.einsum("iKLa,JKLa->Ji", DM_occv, cccv)
                              -0.5*np.einsum("iKab,JKab->Ji", DM_ocvv, ccvv)
                              +0.5*np.einsum("ikab,kJab->Ji", DM_oovv, ocvv)
                              -0.5*np.einsum("iabc,Jabc->Ji", DM_ovvv, cvvv)
                                )
        

    def compute_cvs2_OV(self, L, dm1, DM2):
        #Computes Omega_OV, CVS-ADC(2)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ccov = self.reference_state.eri("o2o2o1v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        ok = True
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required OCCV, "
            errtxt += "CVCV, OVVV, CCVV, OCVV, OOVV, and OVVV blocks."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L multiplier blocks
        if "OV" in L.blocks: 
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain the required OV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute OV multipliers
        nocc = foo.shape[0]

        #TODO: use np.einsum('ii,ia->ia')
        self.matrix["OV"] = -np.einsum("ii,ia->ia", foo, lov)
        #for i in range(nocc):
        #    self.matrix["OV"][i,:] = -foo[i,i]*lov[i,:] 

        self.matrix["OV"] += ( np.einsum("kLJa,iJkL->ia", DM_occv, ococ)
                              +np.einsum("JaKb,iJKb->ia", DM_cvcv, occv)
                              +0.5*np.einsum("jabc,ijbc->ia", DM_ovvv, oovv)
                              -0.5*np.einsum("JKab,JKib->ia", DM_ccvv, ccov)
                              -np.einsum("jKab,jKib->ia", DM_ocvv, ocov)
                              -0.5*np.einsum("jkab,jkib->ia", DM_oovv, ooov)
                              -np.einsum("jcab,ibjc->ia", DM_ovvv, ovov)
                                ) 
                           ### ( np.einsum('kLJa,iJkL->ia', DM_occv, ococ,
                           ###               optimize=True)
                           ###   +np.einsum('JaKb, iJKb->ia', DM_cvcv, occv,
                           ###               optimize=True)
                           ###   +0.5*np.einsum('jabc,ijbc->ia', DM_ovvv, oovv,
                           ###                   optimize=True)
                           ###   -0.5*np.einsum('JKab,JKib->ia', DM_ccvv, ccov,
                           ###                   optimize=True)
                           ###   -np.einsum('jKab,jKib->ia', DM_ocvv, ocov,
                           ###               optimize=True)
                           ###   -0.5*np.einsum('jkab,jkib->ia', DM_oovv, ooov,
                           ###                   optimize=True)
                           ###   -np.einsum('jcab,ibjc->ia', DM_ovvv, ovov,
                           ###               optimize=True)  
                           ###   )
        #3 Clean up:
        #TODO:del lov, ovov
        #TODO:del DM_oovv, DM_occv, DM_ovvv


    def compute_cvs2_CV(self, L, dm1, DM2):
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()

        ok = True

        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required OCCV, "
            errtxt += "CVCV, OVVV, CCVV, OCVV, and OOVV blocks."
            raise ValueError(errtxt, DM2.blocks)

        if "CV" in L.blocks:
            lcv = L.matrix["CV"]
        else:
            errtxt = "L multiplier does not contain the required CV block."
            raise ValueError(errtxt, L.blocks)

        #ncore = fcc.shape[0]
        self.matrix["CV"] = -np.einsum("II,Ia->Ia", fcc, lcv)
        #for I in range(ncore):
        #    self.matrix["CV"][I,:] = -fcc[I,I]*lcv[I,:]

        self.matrix["CV"] += ( np.einsum("lKJa,lKIJ->Ia", DM_occv, occc)
                              +np.einsum("JaKb,IJKb->Ia", DM_cvcv, cccv)
                              -0.5*np.einsum("jabc,jIbc->Ia", DM_ovvv, ocvv)
                              -0.5*np.einsum("JKab,JKIb->Ia", DM_ccvv, cccv)
                              -np.einsum("kJab,kJIb->Ia", DM_ocvv, occv)
                              -0.5*np.einsum("jkab,jkIb->Ia", DM_oovv, oocv)
                              -np.einsum("jcab,jcIb->Ia", DM_ovvv, ovcv)
                                ) 
                            ### ( np.einsum('kLJa,kLIJ->Ia', DM_occv, occc,
                            ###              optimize=True)
                            ###  +np.einsum('JaKb,IJKb->Ia', DM_cvcv, cccv,
                            ###              optimize=True)
                            ###  -0.5*np.einsum('jabc,jIbc->Ia', DM_ovvv, ocvv,
                            ###                  optimize=True)
                            ###  -0.5*np.einsum('JKab,JKIb->Ia', DM_ccvv, cccv,
                            ###                  optimize=True)
                            ###  -np.einsum('jKab,jKIb->Ia', DM_ocvv, occv,
                            ###              optimize=True)
                            ###  -0.5*np.einsum('jkab,jkIb->Ia', DM_oovv, oocv,
                            ###                  optimize=True)
                            ###  -np.einsum('jcab,jcIb->Ia', DM_ovvv, ovcv,
                            ###              optimize=True)
                            ###  )

    #START CVS-ADC2x ############
    ############################
    ####### CVS-ADC(2)-x #######
    #######              ####### 
    def compute_cvs2x_CC(self, L, dm1, DM2):
        #Computes Omega_CC, CVS-ADC(2)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        cccc = self.reference_state.eri("o2o2o2o2").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
    
        #1.2 Density matrices:
        if "CC" in dm1.blocks and "VV" in dm1.blocks and "OO" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required "
            errtxt += "CC, OO, and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        ok = True
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False

        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False

        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False

        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False

        if "OCOC" in DM2.blocks:
            DM_ococ = DM2.matrix["OCOC"]
        else:
            ok = False

        if not ok:
            errtxt = "2PDM does not contain the required CCVV, "
            errtxt += "OCCV, OCVV, CVCV, and OCOC blocks."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L multiplier blocks
        ok = True
        ###if "CC" in L.blocks and "OO" in L.blocks and "VV" in L.blocks:
        ###    lcc = L.matrix["CC"]
        ###    lvv = L.matrix["VV"]
        ###    loo = L.matrix["OO"]
        ###else:
        ###    ok = False

        if "OC" in L.blocks and "CV" in L.blocks and "OV" in L.blocks:
            loc = L.matrix["OC"]
            lov = L.matrix["OV"]
            lcv = L.matrix["CV"]
        else:
            ok = False

        if not ok:
            errtxt = "The L multiplier does not contain the required "
            errtxt += "CC, OO, VV, OC, CV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        #2. Compute CC multipliers
        #TODO: consider the situation where the core does not start at 0
        #TODO: remove commented out code
        ncore = fcc.shape[0] 

        self.matrix["CC"] = -fcc.copy()
        self.matrix["CC"] += np.einsum("II,IJ->IJ", fcc, -dm_cc)#-lcc)
        #for I in range(ncore):
        #    self.matrix["CC"][I,:] -= fcc[I,I]*(dm_cc[I,:]+lcc[I,:]) 

        self.matrix["CC"] += ( -np.einsum("KL,KILJ->IJ", dm_cc, cccc)
                               -np.einsum("kl,kIlJ->IJ", dm_oo, ococ)
                               -np.einsum("ab,IaJb->IJ", dm_vv, cvcv)
                               -np.einsum("kL,kILJ->IJ", loc, occc)
                               -np.einsum("kL,kJLI->IJ", loc, occc)
                               +np.einsum("Kb,KIJb->IJ", lcv, cccv)
                               +np.einsum("Kb,KJIb->IJ", lcv, cccv)
                               +np.einsum("kb,kIJb->IJ", lov, occv)
                               +np.einsum("kb,kJIb->IJ", lov, occv)
                               -0.5*np.einsum("JKab,IKab->IJ", DM_ccvv, ccvv)
                               -np.einsum("kJLa,kILa->IJ", DM_occv, occv)
                               -0.5*np.einsum("kJab,kIab->IJ", DM_ocvv, ocvv)
                               -np.einsum("kLJa,kLIa->IJ", DM_occv, occv)
                               -np.einsum("JaKb,IaKb->IJ", DM_cvcv, cvcv)
                               -np.einsum("kJmL,kImL->IJ", DM_ococ, ococ)
                                )
        #3 Clean up: TODO check if this makes any difference and 
        # clean up everything;
        #TODO: del lov, loc, lcv, occv, ococ, cvcv,
        #TODO del dm_vv, dm_cc, dm_oo, DM_cvcv, DM_occv

    def compute_cvs2x_OO(self, L, dm1, DM2):
        #Computes Omega_OO, CVS-ADC(2)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        oooo = self.reference_state.eri("o1o1o1o1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        oooc = self.reference_state.eri("o1o1o1o2").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()

        #1.2 Density matrices:
        if "CC" in dm1.blocks and "VV" in dm1.blocks and "OO" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain required CC, OO, and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        ok = True
        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "OCOC" in DM2.blocks:
            DM_ococ = DM2.matrix["OCOC"]
        else:
            ok = False
        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required OOVV, "
            errtxt += "OCCV, OVVV, OCVV, OCOC, and OVOV blocks."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L multiplier blocks
        ok = True
        ###if "CC" in L.blocks and "OO" in L.blocks and "VV" in L.blocks:
        ###    lcc = L.matrix["CC"]
        ###    loo = L.matrix["OO"]
        ###    lvv = L.matrix["VV"]
        ###else:
        ###    ok = False

        if "OC" in L.blocks and "CV" in L.blocks and "OV" in L.blocks:
            loc = L.matrix["OC"]
            lcv = L.matrix["CV"]
            lov = L.matrix["OV"]
        else:
            ok = False

        if not ok:
            errtxt = "The L multiplier does not contain the required "
            errtxt += "OC, CV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        #2. Compute OO multipliers
        nocc = foo.shape[0]

        self.matrix["OO"] = -1.0*foo
        self.matrix["OO"] += np.einsum("ii,ij->ij", foo, -dm_oo) #-loo) 
        #for i in range(nocc):
        #    self.matrix["OO"][i,:] -= foo[i,i]*(dm_oo[i,:]+loo[i,:]) 

        #print(self.matrix["OO"])
        self.matrix["OO"] += ( -np.einsum("KL,iKjL->ij", dm_cc, ococ)
                               -np.einsum("kl,ikjl->ij", dm_oo, oooo)
                               -np.einsum("ab,iajb->ij", dm_vv, ovov)
                               +np.einsum("kL,kijL->ij", loc, oooc)
                               +np.einsum("kL,kjiL->ij", loc, oooc)
                               -np.einsum("Kb,iKjb->ij", lcv, ocov)
                               -np.einsum("Kb,jKib->ij", lcv, ocov)
                               +np.einsum("kb,kijb->ij", lov, ooov)
                               +np.einsum("kb,kjib->ij", lov, ooov)
                               -np.einsum("jKLa,iKLa->ij", DM_occv, occv)
                               -0.5*np.einsum("jKab,iKab->ij", DM_ocvv, ocvv)
                               -0.5*np.einsum("jkab,ikab->ij", DM_oovv, oovv)
                               -0.5*np.einsum("jabc,iabc->ij", DM_ovvv, ovvv)
                               -np.einsum("jKlM,iKlM->ij", DM_ococ, ococ)
                               -np.einsum("jakb,iakb->ij", DM_ovov, ovov)
                                )
        #3 Clean up:
        #TODO: del lov, occv, ococ, oovv, ovvv, oooo, ovov, ooov
        #TODO: del dm_vv, dm_cc, dm_oo, DM_oovv, DM_occv, DM_ovvv

    def compute_cvs2x_VV(self, L, dm1, DM2):
        #Computes Omega_VV, CVS-ADC(2)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        fvv = self.reference_state.fock("v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        cvcv = self.reference_state.eri("o2v1o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        vvvv = self.reference_state.eri("v1v1v1v1").to_ndarray()

        #1.2 Density matrices:
        if "VV" in dm1.blocks:
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required VV block."
            raise ValueError(errtxt, dm1.blocks)

        ok = True
        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            ok = False
        if "VVVV" in DM2.blocks:
            DM_vvvv = DM2.matrix["VVVV"]
        else:
            ok = False
        if not ok:
            errtxt = "The 2PDM does not contain the required "
            errtxt += "OCCV, CVCV, OVVV, CCVV, OCVV, and OOVV blocks."
            raise ValueError(errtxt, DM2.blocks) 

        #1.3 L multiplier blocks
        #if "VV" in L.blocks:
        #    lvv = L.matrix["VV"]
        #else:
        #    errtxt = "L multiplier does not contain the required VV block."
        #    raise ValueError(errtxt, L.blocks)

        #2. Compute VV multipliers
        nvirt = dm_vv.shape[0]

        self.matrix["VV"] = -np.einsum("aa,ab->ab", fvv, dm_vv)#+lvv)
        #for a in range(nvirt):
        #    self.matrix["VV"][a,:] = -fvv[a,a]*(dm_vv[a,:]+lvv[a,:]) 

        self.matrix["VV"] += ( -np.einsum("jKIb,jKIa->ab", DM_occv, occv)
                               -np.einsum("IbJc,IaJc->ab", DM_cvcv, cvcv)
                               -0.5*np.einsum("ibcd,iacd->ab", DM_ovvv, ovvv)
                               -0.5*np.einsum("IJbc,IJac->ab", DM_ccvv, ccvv)
                               -np.einsum("iJbc,iJac->ab", DM_ocvv, ocvv)
                               -0.5*np.einsum("ijbc,ijac->ab", DM_oovv, oovv)
                               -np.einsum("idbc,idac->ab", DM_ovvv, ovvv)
                               -np.einsum("ibjc,iajc->ab", DM_ovov, ovov)
                               -0.5*np.einsum("bcde,acde->ab", DM_vvvv, vvvv)
                                )
        #3 Clean up:
        #TODO: del fvv, occv, oovv, ovvv, cvcv
        #TODO: del dm_vv, DM_oovv, DM_occv, DM_ovvv, DM_cvcv


    def compute_cvs2x_OC(self, L, dm1, DM2):
        foo = self.reference_state.fock("o1o1").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        oocc = self.reference_state.eri("o1o1o2o2").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray() #
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ovcc = self.reference_state.eri("o1v1o2o2").to_ndarray() #
        oooc = self.reference_state.eri("o1o1o1o2").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()

        if "CC" in dm1.blocks and "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required "
            errtxt += "CC, OO, and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        ok = True

        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
        if "OCOC" in DM2.blocks:
            DM_ococ = DM2.matrix["OCOC"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required "
            errtxt += "CCVV, OCCV, OCVV, CVCV, and OCOC blocks."
            raise ValueError(errtxt, DM2.blocks)

        ok = True
        ###if "CC" in L.blocks and "OO" in L.blocks and "VV" in L.blocks:
        ###    lcc = L.matrix["CC"]
        ###    loo = L.matrix["OO"]
        ###    lvv = L.matrix["VV"]
        ###else:
        ###    ok = False

        if "OC" in L.blocks and "CV" in L.blocks and "OV" in L.blocks:
            loc = L.matrix["OC"]
            lcv = L.matrix["CV"]
            lov = L.matrix["OV"]

        if not ok:
            errtxt = "L multiplier does not contain he required CC, "
            errtxt += "OC, CV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        nocc = foo.shape[0]

        self.matrix["OC"] = -np.einsum("ii,iJ->iJ", foo, loc)

        #for i in range(nocc):
        #    self.matrix["OC"][i,:] = -foo[i,i]*loc[i,:]

        ##TODO: !!! = Double Check!
        self.matrix["OC"] += ( np.einsum("KL,iKLJ->iJ", dm_cc, occc)
                              -np.einsum("kl,kilJ->iJ", dm_oo, oooc)
                              -np.einsum("ab,iaJb->iJ", dm_vv, ovcv)
                              +np.einsum("kL,iLkJ->iJ", loc, ococ)
                              -np.einsum("kL,kiLJ->iJ", loc, oocc)
                              +np.einsum("Kb,ibKJ->iJ", lcv, ovcc)
                              -np.einsum("Kb,iKJb->iJ", lcv, occv)
                              +np.einsum("kb,kiJb->iJ", lov, oocv)
                              +np.einsum("kb,kJib->iJ", lov, ocov)
                              -0.5*np.einsum("JKab,iKab->iJ", DM_ccvv, ocvv)
                              +np.einsum("kJLa,ikLa->iJ", DM_occv, oocv) 
                              +0.5*np.einsum("kJab,ikab->iJ", DM_ocvv, oovv)
                              -np.einsum("kLJa,kLia->iJ", DM_occv,ocov) #!!!
                              -np.einsum("JaKb,iaKb->iJ", DM_cvcv, ovcv)
                              +np.einsum("kJmL,ikmL->iJ", DM_ococ, oooc)
                                )

    #TODO: add cvs-adc2x 2PDMs here
    def compute_cvs2x_CO(self, L, dm1, DM2):
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        oooc = self.reference_state.eri("o1o1o1o2").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray() 
        oocc = self.reference_state.eri("o1o1o2o2").to_ndarray()
        ovcc = self.reference_state.eri("o1v1o2o2").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()

        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        ccvv = self.reference_state.eri("o2o2v1v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()

        if "CC" in dm1.blocks and "OO" in dm1.blocks and "VV" in dm1.blocks:
            dm_cc = dm1.matrix["CC"]
            dm_oo = dm1.matrix["OO"]
            dm_vv = dm1.matrix["VV"]
        else:
            errtxt = "The 1PDM does not contain the required "
            errtxt += "CC, OO, and VV blocks."
            raise ValueError(errtxt, dm1.blocks)

        ok = True

        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required "
            errtxt += "CCVV, OCCV, OCVV, and CVCV blocks."
            raise ValueError(errtxt, DM2.blocks)

        ok = True
        ###if "CC" in L.blocks and "OO" in L.blocks and "VV" in L.blocks:
        ###    lcc = L.matrix["CC"]
        ###    loo = L.matrix["OO"]
        ###    lvv = L.matrix["VV"]
        ###else:
        ###    ok = False

        if "OC" in L.blocks and "CV" in L.blocks and "OV" in L.blocks:
            loc = L.matrix["OC"]
            lcv = L.matrix["CV"]
            lov = L.matrix["OV"]
        else:
            ok = False

        if not ok:
            errtxt = "L multiplier does not contain he required CC, "
            errtxt += "OC, CV, and OV blocks."
            raise ValueError(errtxt, L.blocks)

        ncore = fcc.shape[0]

        self.matrix["CO"] = -np.einsum("JJ,iJ->Ji", fcc, loc)

        #for i in range(nocc):
        #    self.matrix["OC"][i,:] = -foo[i,i]*loc[i,:]

        self.matrix["CO"] += ( np.einsum("KL,iLKJ->Ji", dm_cc, occc)
                              -np.einsum("kl,likJ->Ji", dm_oo, oooc)
                              -np.einsum("ab,ibJa->Ji", dm_vv, ovcv)
                              +np.einsum("kL,kJiL->Ji", loc, ococ)
                              -np.einsum("kL,kiLJ->Ji", loc, oocc)
                              +np.einsum("Kb,ibKJ->Ji", lcv, ovcc)
                              -np.einsum("Kb,iKJb->Ji", lcv, occv)
                              +np.einsum("kb,kJib->Ji", lov, ocov)
                              +np.einsum("kb,kiJb->Ji", lov, oocv)
                              -np.einsum("iKLa,JKLa->Ji", DM_occv, cccv)
                              -0.5*np.einsum("iKab,JKab->Ji", DM_ocvv, ccvv)
                              +0.5*np.einsum("ikab,kJab->Ji", DM_oovv, ocvv)
                              -0.5*np.einsum("iabc,Jabc->Ji", DM_ovvv, cvvv)
                                )
        

    def compute_cvs2x_OV(self, L, dm1, DM2):
        #Computes Omega_OV, CVS-ADC(2)
        #1. Pre-requisites:
        #1.1 Fock and ERI matrix blocks:
        foo = self.reference_state.fock("o1o1").to_ndarray()
        ococ = self.reference_state.eri("o1o2o1o2").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oovv = self.reference_state.eri("o1o1v1v1").to_ndarray()
        ccov = self.reference_state.eri("o2o2o1v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()
        ooov = self.reference_state.eri("o1o1o1v1").to_ndarray()
        ovov = self.reference_state.eri("o1v1o1v1").to_ndarray()
        ovvv = self.reference_state.eri("o1v1v1v1").to_ndarray()

        ok = True
        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "VVVV" in DM2.blocks:
            DM_vvvv = DM2.matrix["VVVV"]
        else:
            ok = False
        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required OCCV, "
            errtxt += "CVCV, OVVV, CCVV, OCVV, OOVV, and OVVV blocks."
            raise ValueError(errtxt, DM2.blocks)

        #1.3 L multiplier blocks
        if "OV" in L.blocks: 
            lov = L.matrix["OV"]
        else:
            errtxt = "L multiplier does not contain the required OV block."
            raise ValueError(errtxt, L.blocks)

        #2. Compute OV multipliers
        nocc = foo.shape[0]

        #TODO: use np.einsum('ii,ia->ia') everywhere
        self.matrix["OV"] = -np.einsum("ii,ia->ia", foo, lov)
        #for i in range(nocc):
        #    self.matrix["OV"][i,:] = -foo[i,i]*lov[i,:] 

        self.matrix["OV"] += ( np.einsum("kLJa,iJkL->ia", DM_occv, ococ)
                              +np.einsum("JaKb,iJKb->ia", DM_cvcv, occv)
                              +0.5*np.einsum("jabc,ijbc->ia", DM_ovvv, oovv)
                              -0.5*np.einsum("JKab,JKib->ia", DM_ccvv, ccov)
                              -np.einsum("jKab,jKib->ia", DM_ocvv, ocov)
                              -0.5*np.einsum("jkab,jkib->ia", DM_oovv, ooov)
                              -np.einsum("jcab,ibjc->ia", DM_ovvv, ovov)
                              -0.5*np.einsum("abcd,ibcd->ia", DM_vvvv, ovvv)
                              +np.einsum("jakb,ijkb->ia", DM_ovov, ooov)
                                ) 
        #3 Clean up:
        #TODO: del lov, ovov
        #TODO: del DM_oovv, DM_occv, DM_ovvv


    def compute_cvs2x_CV(self, L, dm1, DM2):
        fcc = self.reference_state.fock("o2o2").to_ndarray()
        occc = self.reference_state.eri("o1o2o2o2").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        ocvv = self.reference_state.eri("o1o2v1v1").to_ndarray()
        cccv = self.reference_state.eri("o2o2o2v1").to_ndarray()
        occv = self.reference_state.eri("o1o2o2v1").to_ndarray()
        oocv = self.reference_state.eri("o1o1o2v1").to_ndarray()
        ovcv = self.reference_state.eri("o1v1o2v1").to_ndarray()
        cvvv = self.reference_state.eri("o2v1v1v1").to_ndarray()
        ocov = self.reference_state.eri("o1o2o1v1").to_ndarray()

        ok = True

        if "OCCV" in DM2.blocks:
            DM_occv = DM2.matrix["OCCV"]
        else:
            ok = False
        if "CVCV" in DM2.blocks:
            DM_cvcv = DM2.matrix["CVCV"]
        else:
            ok = False
        if "OVVV" in DM2.blocks:
            DM_ovvv = DM2.matrix["OVVV"]
        else:
            ok = False
        if "CCVV" in DM2.blocks:
            DM_ccvv = DM2.matrix["CCVV"]
        else:
            ok = False
        if "OCVV" in DM2.blocks:
            DM_ocvv = DM2.matrix["OCVV"]
        else:
            ok = False
        if "OOVV" in DM2.blocks:
            DM_oovv = DM2.matrix["OOVV"]
        else:
            ok = False
        if "VVVV" in DM2.blocks:
            DM_vvvv = DM2.matrix["VVVV"]
        else:
            ok = False
        if "OVOV" in DM2.blocks:
            DM_ovov = DM2.matrix["OVOV"]
        else:
            ok = False

        if not ok:
            errtxt = "The 2PDM does not contain the required OCCV, "
            errtxt += "CVCV, OVVV, CCVV, OCVV, and OOVV blocks."
            raise ValueError(errtxt, DM2.blocks)

        if "CV" in L.blocks:
            lcv = L.matrix["CV"]
        else:
            errtxt = "L multiplier does not contain the required CV block."
            raise ValueError(errtxt, L.blocks)

        #ncore = fcc.shape[0]
        self.matrix["CV"] = -np.einsum("II,Ia->Ia", fcc, lcv)
        #for I in range(ncore):
        #    self.matrix["CV"][I,:] = -fcc[I,I]*lcv[I,:]

        self.matrix["CV"] += ( np.einsum("lKJa,lKIJ->Ia", DM_occv, occc)
                              +np.einsum("JaKb,IJKb->Ia", DM_cvcv, cccv)
                              -0.5*np.einsum("jabc,jIbc->Ia", DM_ovvv, ocvv)
                              -0.5*np.einsum("JKab,JKIb->Ia", DM_ccvv, cccv)
                              -np.einsum("kJab,kJIb->Ia", DM_ocvv, occv)
                              -0.5*np.einsum("jkab,jkIb->Ia", DM_oovv, oocv)
                              -np.einsum("jcab,jcIb->Ia", DM_ovvv, ovcv)
                              -0.5*np.einsum("abcd,Ibcd->Ia", DM_vvvv, cvvv)
                              -np.einsum("jakb,jIkb->Ia", DM_ovov, ocov)
                                ) 
