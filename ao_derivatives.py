#               
#                               ADC_GRADIENT
#     --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------

# TODO: delete commentd out code
# TODO: RUN trhough yapf and flake8
# TODO: Double check that the OV and VO blocks are
# contracted correctly (using transpose!)
# TODO: Check spin-flip and enable unrestricted 
import numpy as np
import time

np.set_printoptions(precision=7, suppress=True)

no = []
try:
    import pyscf
    from pyscf import grad
except ImportError:
    no.append('pyscf')

try:
    import veloxchem as vlx
except ImportError:
    no.append('vlx')

try:
    import psi4
except ImportError:
    no.append('psi4')

try:
    import daltonproject as dp
except ImportError:
    no.append('dp')

if len(no)==4:
    errtxt="At least one of the following "
    errtxt += "electronic structure codes is required: \n"
    for code in no:
        errtxt = errtxt + code + ' '
    raise ImportError(errtxt)

def get_type(block,pos=0):
    return {
        "O" : "o1o1",
        "C" : "o2o2",
        "V" : "v1v1",
        }[block[pos]]

def get_shape(block, ncore, nocc, nvirt, natoms):
    return{
            "CC" : (3*natoms, ncore, ncore),
            "OC" : (3*natoms, nocc, ncore),
            "CV" : (3*natoms, ncore, nvirt),
            "OO" : (3*natoms, nocc, nocc),
            "OV" : (3*natoms, nocc, nvirt),
            "VV" : (3*natoms, nvirt, nvirt),
            }[block]

def get_shape_eri(block, ncore, nocc, nvirt, natoms):
    return{
            "CVCV" : (3*natoms, ncore, nvirt, ncore, nvirt),
            "CCVV" : (3*natoms, ncore, ncore, nvirt, nvirt),
            "OCVV" : (3*natoms, nocc, ncore, nvirt, nvirt),
            "OCCV" : (3*natoms, nocc, ncore, ncore, nvirt),
            "OOVV" : (3*natoms, nocc, nocc, nvirt, nvirt),
            "OVVV" : (3*natoms, nocc, nvirt, nvirt, nvirt),
            "OVOV" : (3*natoms, nocc, nvirt, nocc, nvirt),
            "OOOO" : (3*natoms, nocc, nocc, nocc, nocc),
            "CCCC" : (3*natoms, ncore, ncore, ncore, ncore),
            "OCOC" : (3*natoms, nocc, ncore, nocc, ncore),
            "VVVV" : (3*natoms, nvirt, nvirt, nvirt, nvirt),
            }[block]

def get_mo_space(D, block, ncore, nocc, nvirt, pos=0):
    return{
        "C" : D.reference_state.mospaces.core_orbitals[0:ncore],
        "O" : D.reference_state.mospaces.occupied_orbitals[0:nocc],
        "V" : D.reference_state.mospaces.virtual_orbitals[0:nvirt],
        }[block[pos]]

def get_factor(block):
    return{
        "OOOO" : 0.25,
        "CCCC" : 0.25,
        "VVVV" : 0.25,
        "CCVV" : 0.50,
        "OOVV" : 0.50,
        "OCOC" : 1.00,
        "CVCV" : 1.00,
        "OCVV" : 1.00,
        "OVVV" : 1.00,
        "OVOV" : 1.00,
        "OCCV" : 2.00,
        "OO" : 1.00,
        "CC" : 1.00,
        "VV" : 1.00,
        "OC" : 2.00,
        "OV" : 2.00,
        "CV" : 2.00,
        }[block]

def compute_pyscf_eri(DM2_list, dl_list, molecule, mo_coeff, verbose=False,
                      omit_2pdm=False):
    """Computes the contribution of the ERI derivatives to the gradient

    Parameters:
    ----------
    DM2     : list of 2-particle density matrices
    dl_list : list of 1pdms and lambda multipliers 
    molecule: pyscf molecule object 
    """

    start = time.time()
    # compute ( nabla m n | t p ) AO basis
    # "-" required because of how pyscf works
    # see /pyscf/grad/rhf.py 
    deri = -molecule.intor('int2e_ip1', aosym='s1')
    ao_slices = molecule.aoslice_by_atom()

    stop0p2 = time.time()
    if verbose:
        print("ERI from pyscf%10.5f s." % (stop0p2-start))
    grad_2pdm = np.zeros((molecule.natm,3))

    nocc = DM2_list[0].reference_state.fock("o1o1").to_ndarray().shape[0]
    nvirt = DM2_list[0].reference_state.fock("v1v1").to_ndarray().shape[0]
    if DM2_list[0].reference_state.has_core_occupied_space:
        ncore = DM2_list[0].reference_state.fock("o2o2").to_ndarray().shape[0]
    else:
        ncore = 0

    nao = mo_coeff.shape[0]

    DM_ao1 = np.zeros((nao,nao,nao,nao))
    DM_ao2 = np.zeros((nao, nao, nao, nao))

    dl_ao = np.zeros((nao,nao))
    occ = int(0.5*(ncore+nocc))
    mo_occ = mo_coeff[:,:occ]
    P = np.einsum('ni,fi->nf', mo_occ, mo_occ)

    for dl in dl_list:
        for block in dl.blocks:
            time1 = time.time()
            space1 = get_mo_space(dl, block, int(ncore/2), int(nocc/2),
                                  int(nvirt/2), pos=0)
            space2 = get_mo_space(dl, block, int(ncore/2), int(nocc/2),
                                  int(nvirt/2), pos=1) 
                
            mo_1 = mo_coeff[:,space1]
            mo_2 = mo_coeff[:,space2]
    
            factor = get_factor(block)
    
            if block in ["OV", "CV", "OC"]:
                dl_ao += ( np.linalg.multi_dot([mo_1,
                            dl.matrix[block][:len(space1),:len(space2)], mo_2.T])
                          + np.linalg.multi_dot([mo_2,
                            dl.matrix[block][:len(space1),:len(space2)].T, mo_1.T])
                         )
            else:
                dl_ao += ( np.linalg.multi_dot([mo_1,
                            dl.matrix[block][:len(space1),:len(space2)], mo_2.T])
                         )
            time2 = time.time()
            if verbose:
                print(block+"       took %10.5f s." % (time2-time1)) 

    for DM2 in DM2_list:   
        for block in DM2.blocks:
            #time1=time.time()
            space1 = get_mo_space(DM2, block, int(ncore/2), int(nocc/2),
                                              int(nvirt/2), pos=0)
            space2 = get_mo_space(DM2, block, int(ncore/2), int(nocc/2),
                                              int(nvirt/2), pos=1) 
            space3 = get_mo_space(DM2, block, int(ncore/2), int(nocc/2),
                                              int(nvirt/2), pos=2)
            space4 = get_mo_space(DM2, block, int(ncore/2), int(nocc/2),
                                              int(nvirt/2), pos=3)    
    
            mo_1 = mo_coeff[:,space1]
            mo_2 = mo_coeff[:,space2]
            mo_3 = mo_coeff[:,space3]
            mo_4 = mo_coeff[:,space4]
            factor = get_factor(block)
    
            time1=time.time()
    
    
            DM_ao1 += 2.0*factor*(( np.einsum('mp,pntf->mntf', mo_1,
                            np.einsum('nq,pqtf->pntf', mo_2,
                            np.einsum('tr,pqrf->pqtf', mo_3,
                            np.einsum('fs,pqrs->pqrf', mo_4, 
                            DM2.matrix[block][:len(space1),:len(space2),
                                              :len(space3),:len(space4)]))))
                           ) #aaaa+bbbb
                    + ( np.einsum('mp,pntf->mntf', mo_1,
                             np.einsum('nq,pqtf->pntf', mo_2,
                             np.einsum('tr,pqrf->pqtf', mo_3,
                             np.einsum('fs,pqrs->pqrf', mo_4, 
                             DM2.matrix[block][:len(space1),len(space2):,
                                               :len(space3),len(space4):]))))
                         ) #abab+baba
                    )
    
            DM_ao2 += 2.0*factor*( ( np.einsum('mp,pntf->mntf', mo_1,
                            np.einsum('nq,pqtf->pntf', mo_2,
                            np.einsum('tr,pqrf->pqtf', mo_3,
                            np.einsum('fs,pqrs->pqrf', mo_4, 
                            DM2.matrix[block][:len(space1),:len(space2),
                                              :len(space3),:len(space4)]))))
                           ) #aaaa+bbbb
                    +  ( np.einsum('mp,pntf->mntf', mo_1,
                             np.einsum('nq,pqtf->pntf', mo_2,
                             np.einsum('tr,pqrf->pqtf', mo_3,
                             np.einsum('fs,pqrs->pqrf', mo_4, 
                             DM2.matrix[block][:len(space1),len(space2):,
                                               len(space3):,:len(space4)]))))
                         ) #abba+baab
                    )
            time2=time.time()
            if verbose:
                print(block+"     took %10.5f s." % (time2-time1)) 

    stop0p5 = time.time()
    if verbose:
        print("MO-to-AO total%10.5f s." % (stop0p5-stop0p2))

    for i in range(molecule.natm):
        eriao = np.zeros(deri.shape)
        k0, k1 = ao_slices[i,2:]
        eriao[:,k0:k1] = deri[:,k0:k1]

        # we need (nabla m n | t p) + (m nabla n| t p) 
        #       + (m n | nabla t p) + (m n | t nabla p)
        eriao += ( eriao.transpose(0,2,1,4,3) 
                 + eriao.transpose(0,3,4,1,2)
                 + eriao.transpose(0,4,3,2,1) )

        # MISLEADING NAME! TODO: change
        grad_2pdm[i] += ( 4.0*np.einsum('xmtnf,mntf->x', eriao,
                                         np.einsum('mt,nf->mntf', dl_ao, P)
                                                  )
                          -2.0*np.einsum('xmfnt,mntf->x', eriao,
                                        np.einsum('mt,nf->mntf', dl_ao, P)
                                                  ) 
                                    )

        if omit_2pdm:
            pass
        else:
            grad_2pdm[i] += ( np.einsum('xmtnf,mntf->x', eriao, DM_ao1)
                         -np.einsum('xmfnt,mntf->x', eriao, DM_ao2)
                         )
    stop = time.time()
    if verbose:
        print("EINSUM erix   %10.5f s." % (stop-stop0p5))
        print("ERI tot.      %10.5f s." % (stop-start))
    return grad_2pdm

# TODO: remove; this is not used anymore
def compute_pyscf_eri_sumi(dl_list, molecule, mo_coeff):
    """Computes the contribution of a 2PDM to the gradient

    Parameters:
    ----------
    dl_list : list with 1-particle density matrices and lambda multiplier
    molecule: pyscf molecule object 
    """

    #print("Computing the eri contribution to the gradient, 1PDM...")
    start = time.time()
    # compute ( nabla m n | t p ) AO basis
    # "-" required because of how pyscf works
    # see /pyscf/grad/rhf.py 
    deri = -molecule.intor('int2e_ip1', aosym='s1')
    ao_slices = molecule.aoslice_by_atom()

    grad_2pdm = np.zeros((molecule.natm,3))

    nocc = dl_list[0].reference_state.fock("o1o1").to_ndarray().shape[0]
    nvirt = dl_list[0].reference_state.fock("v1v1").to_ndarray().shape[0]
    if dl_list[0].reference_state.has_core_occupied_space:
        ncore = dl_list[0].reference_state.fock("o2o2").to_ndarray().shape[0]
    else:
        ncore = 0

    nao = mo_coeff.shape[0]
    dl_ao = np.zeros((nao,nao))
    occ = int(0.5*(ncore+nocc)) #total number of occupied orbitals
    mo_occ = mo_coeff[:,:occ]
    P = np.einsum('ni,fi->nf', mo_occ, mo_occ)

    for dl in dl_list:
        for block in dl.blocks:
            space1 = get_mo_space(dl, block, int(ncore/2), int(nocc/2),
                                  int(nvirt/2), pos=0)
            space2 = get_mo_space(dl, block, int(ncore/2), int(nocc/2),
                                  int(nvirt/2), pos=1) 
    
                
            mo_1 = mo_coeff[:,space1]
            mo_2 = mo_coeff[:,space2]
    
            factor = get_factor(block)
    
            if block in ["OV", "CV", "OC"]:
                dl_ao += ( np.linalg.multi_dot([mo_1,
                            dl.matrix[block][:len(space1),:len(space2)], mo_2.T])
                          + np.linalg.multi_dot([mo_2,
                            dl.matrix[block][:len(space1),:len(space2)].T, mo_1.T])
                         )
            else:
                dl_ao += ( np.linalg.multi_dot([mo_1,
                            dl.matrix[block][:len(space1),:len(space2)], mo_2.T])
                         ) #aaaa

    for i in range(molecule.natm):
        eriao = np.zeros(deri.shape)
        k0, k1 = ao_slices[i,2:]
        eriao[:,k0:k1] = deri[:,k0:k1]

        # we need (nabla m n | t p) + (m nabla n| t p) 
        #       + (m n | nabla t p) + (m n | t nabla p)
        eriao += ( eriao.transpose(0,2,1,4,3) 
                 + eriao.transpose(0,3,4,1,2)
                 + eriao.transpose(0,4,3,2,1) )


        grad_2pdm[i] += ( 4.0*np.einsum('xmtnf,mntf->x', eriao,
                                         np.einsum('mt,nf->mntf', dl_ao, P)
                                                  )
                          -2.0*np.einsum('xmfnt,mntf->x', eriao,
                                        np.einsum('mt,nf->mntf', dl_ao, P)
                                                  ) 
                                    )
    stop = time.time()
    print("ERI 1PDM took %10.5f s." % (stop-start))
    return grad_2pdm

def compute_pyscf_hcore(dl_list, molecule, scf, mo_coeff, verbose=False):
    """Computes the contribution of a 2PDM to the gradient

    Parameters:
    ----------
    dl_list : list with 1-particle density matrices and lambda multipliers
    molecule: pyscf molecule object
    scf     : pyscf scf reference state
    mo_coeff: mo coefficients 
    """

    #print("Computing the core Hamiltonian contribution to the gradient...")
    start = time.time()

    gradient = grad.RHF(scf)
    gen_hcore = gradient.hcore_generator(molecule)

    grad_2pdm = np.zeros((molecule.natm,3))
    nao = mo_coeff.shape[0]

    dl_ao = np.zeros((nao,nao))

    nocc = dl_list[0].reference_state.fock("o1o1").to_ndarray().shape[0]
    nvirt = dl_list[0].reference_state.fock("v1v1").to_ndarray().shape[0]
    if dl_list[0].reference_state.has_core_occupied_space:
        ncore = dl_list[0].reference_state.fock("o2o2").to_ndarray().shape[0]
    else:
        ncore = 0

    for dl in dl_list:
        for block in dl.blocks:
            space1 = get_mo_space(dl, block, int(ncore/2), int(nocc/2),
                                                 int(nvirt/2), pos=0)
            space2 = get_mo_space(dl, block, int(ncore/2), int(nocc/2),
                                                 int(nvirt/2), pos=1) 
    
                
            mo_1 = mo_coeff[:,space1]
            mo_2 = mo_coeff[:,space2]
    
            factor = get_factor(block) 
            if block in ["OV", "CV", "OC"]:
                    dl_ao += ( np.linalg.multi_dot([mo_1,
                                dl.matrix[block][:len(space1),:len(space2)],
                                mo_2.T])
                            + np.linalg.multi_dot([mo_2,
                                dl.matrix[block][:len(space1),:len(space2)].T,
                                mo_1.T])
                                ) 
            else:    
                dl_ao += np.linalg.multi_dot([mo_1,
                            dl.matrix[block][:len(space1),:len(space2)], mo_2.T])

    for i in range(molecule.natm):
        hao = gen_hcore(i)
        grad_2pdm[i] += 2.0*np.einsum('xmn,mn->x', hao, dl_ao)


    stop = time.time()
    if verbose:
        print("HCORE    took %10.5f s." % (stop-start))

    return grad_2pdm


def compute_pyscf_ovlp(dl, molecule, mo_coeff, verbose=False):
    """Computes the contribution of a 2PDM to the gradient

    Parameters:
    ----------
    dl      : omega lagrange multiplier, MO basis
    molecule: pyscf molecule object
    mo_coeff: mo coefficients 
    """

    start = time.time()

    ds = - molecule.intor('int1e_ipovlp', aosym='s1')
    ao_slices = molecule.aoslice_by_atom()

    grad_2pdm = np.zeros((molecule.natm,3))

    nocc = dl.reference_state.fock("o1o1").to_ndarray().shape[0]
    nvirt = dl.reference_state.fock("v1v1").to_ndarray().shape[0]
    if dl.reference_state.has_core_occupied_space:
        ncore = dl.reference_state.fock("o2o2").to_ndarray().shape[0]
    else:
        ncore = 0

    nao = mo_coeff.shape[0]
    dl_ao = np.zeros((nao,nao))
    dl_new = np.zeros((nao,nao))

    for block in dl.blocks:
        space1 = get_mo_space(dl, block, int(ncore/2), int(nocc/2),
                                             int(nvirt/2), pos=0)
        space2 = get_mo_space(dl, block, int(ncore/2), int(nocc/2),
                                             int(nvirt/2), pos=1) 

            
        mo_1 = mo_coeff[:,space1]
        mo_2 = mo_coeff[:,space2]

        factor = get_factor(block) 
        if block in ["OV", "CV", "OC"]:
            dl_ao += (np.linalg.multi_dot([mo_1,
                                 dl.matrix[block][:len(space1),:len(space2)],
                                 mo_2.T])
                    + np.linalg.multi_dot([mo_2, 
                                    dl.matrix[block][:len(space1),:len(space2)].T,
                                    mo_1.T])
                    )
        else:           
            dl_ao += factor*( np.einsum('mp,pn->mn', mo_1,
                          np.einsum('nq,pq->pn', mo_2,
                          dl.matrix[block][:len(space1),:len(space2)]))
                         ) #aa
    
    for i in range(molecule.natm):
        sao = np.zeros(ds.shape)
        k0, k1 = ao_slices[i, 2:]
        sao[:,k0:k1] = ds[:,k0:k1]

        sao += sao.transpose(0,2,1) #(dm|n)+(m|dn)

        # !TODO: rename! this is not 2pdm!
        grad_2pdm[i] += 2.0*np.einsum('xmn,mn->x', sao, dl_ao)

    stop = time.time()
    if verbose:
        print("OVLP     took %10.5f s." % (stop-start))

    return grad_2pdm
