#               
#                               ADC_GRADIENT
#	  --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------

import adcc

class ADCDriver():
    """
    ADC driver object, similar to the gator adcdriver
    """

    def __init__(self, scf, tol = 1e-6, method='adc1', states=None,
                 singlets=None, spin_flip=None, triplets=None, core=None):
        """
        Initializes the ADCDriver object;
        
        Parameters:
        ---------
        scf     : scf reference state
        tol     : convergence threshold for adc
        method  : adc level of theory
                  Possible values: hf, gs_mp2, adc0, adc1
                                   cvs0, cvs1, cvs2, cvs2x
        states  : number of states to compute
        singlets: number of singlet states to compute
        triplets: number of triplet states to compute
        core    : core orbital space for cvs-adc
        """
        self.scf = scf
        self.tol = tol
        self.method = method
        self.states = states
        self.singlets = singlets
        self.triplets = triplets

        if states is None:
            if singlets is None:
                if triplets is None:
                    self.singlets = 1
        
        self.spin_flip = spin_flip
        self.core = core

    def reset(self, scf):
        self.scf = scf

    def compute(self):
        """
        Runs the adc calculation and returns an adcc excited state object
        """
        if self.method in ["hf", "gs_mp2"]:
            # goes trhough adc0, but only to get required integrals
            # and Fock matrix elements in the appropriate format.
            adc = adcc.run_adc(self.scf, method="adc0", 
                               core_orbitals=self.core, n_states=self.states,
                               n_singlets=self.singlets, n_triplets=self.triplets,
                               conv_tol=self.tol)
        else:
            adc = adcc.run_adc(self.scf, method=self.method, 
                               core_orbitals=self.core, n_states=self.states,
                               n_singlets=self.singlets, n_triplets=self.triplets,
                               n_spin_flip=self.spin_flip, conv_tol=self.tol)
        return adc

    
