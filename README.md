[![license](https://img.shields.io/badge/License-GPL--3.0-orange)](https://opensource.org/licenses/GPL-3.0)     [![JCP](https://img.shields.io/badge/JCP-10.1063%2F5.0058221-informational)](https://doi.org/10.1063/5.0058221)

## `Adc_gradient` overview

`adc_gradient` is a Python module which can be used to determine analytical energy gradients up to CVS-ADC(2)-x level of theory, based on ADC calculations perfromed with ADC-connect ([`adcc`](https://adc-connect.org/v0.15.1/index.html)). ADC-connect uses an SCF reference state from external electronic structure programs such as [`pyscf`](https://sunqm.github.io/pyscf/) and [`veloxchem`](https://veloxchem.org/). The `adc_gradient` module calls [`adcc`](https://adc-connect.org/v0.15.1/index.html) to perform an ADC calculation at the desired level and then imports the excitation vectors, Fock matrix elements and anti-symmetrized two-electron integrals from [`adcc`](https://adc-connect.org/v0.15.1/index.html). These are then used to construct the density matrices required by the submodule `Orbital_Response.py` which determines the orbital response Lagrange multipliers. 

<div align="center">

![](figures/module_structure_120.png "Structure of the adc_gradient module")

</div>

The density matrices (![\gamma',\Gamma'](https://latex.codecogs.com/svg.latex?&space;\gamma',\Gamma')) and multipliers (![\lambda,\omega,\tilde{t}_{ijab}](https://latex.codecogs.com/svg.latex?&space;\lambda,\omega,\tilde{t}_{ijab})) used in `adc_gradient` are represented as `numpy` arrays, for the purpose of utilizing the tensor contraction and matrix multiplication routines from the `numpy` library. In addition, the conjugate gradient routine from the `scipy` library is adopted to iteratively solve the orbital response equations. Once the density matrices and the Lagrange multipliers are constructed, they are transformed into the AO basis and contracted with the integral derivatives imported from [`pyscf`](https://sunqm.github.io/pyscf/) toward obtaining the gradient. This `adc_gradient` engine further connects to the geometry optimization module `geomeTRIC`(https://github.com/leeping/geomeTRIC). Based on the energy and the gradient provided by `adc_gradient`, [geomeTRIC](https://github.com/leeping/geomeTRIC) approximates the Hessian matrix and performs a full or constrained structure relaxation, thus enabling the geometry optimizations in core-excited states. 

## Prerequisites:

+ [`adcc`](https://adc-connect.org/v0.15.1/index.html)
+ [`pyscf`](https://sunqm.github.io/pyscf/) and/or [`veloxchem`](https://veloxchem.org/) (not all functionalities are enabled with [`veloxchem`](https://veloxchem.org/))
+ [`geomeTRIC`](https://github.com/leeping/geomeTRIC) (optional, for geometry optimization)

## How to install and run:

```
git clone https://gitlab.com/iubr/cvs-adc-grad.git
export PYTHONPATH=$PYTHONPATH:PATH-to-cvs-adc-grad
cd example_scripts
python3 test_cvs_adc2x_gradient.py >> output.out
```

## Current capabilities:
+ MP2
+ ADC(1)
+ CVS-ADC(1)
+ CVS-ADC(2)
+ CVS-ADC(2)-x

## Examples python scripts:
+ [MP2 gradient](example_scripts/test_mp2_gradient.py) 
+ [CVS-ADC(1) gradient](example_scripts/test_cvs_adc1_gradient.py)
+ [CVS-ADC(2) gradient](example_scripts/test_cvs_adc2_gradient.py)
+ [CVS-ADC(2)-x gradient](example_scripts/test_cvs_adc2x_gradient.py)
+ [Core-excited state geometry optimization](example_scripts/opt_h2o.py)
+ [Core-excited state constrained optimization](example_scripts/constrained_opt_h2o.py)
+ [C1s core-excited state analytical gradients](example_scripts/C1s_analytical_grad.py)
+ [N1s core-excited state analytical gradients](example_scripts/N1s_analytical_grad.py)
+ [O1s core-excited state analytical gradients](example_scripts/O1s_analytical_grad.py)

## Citation
[![JCP](https://img.shields.io/badge/JCP-10.1063%2F5.0058221-informational)](https://doi.org/10.1063/5.0058221)

The equations implemented in this module have been described in the following article: [DOI:10.1063/5.0058221](https://doi.org/10.1063/5.0058221). 

The preprint can be found here:  [DOI:10.26434/chemrxiv.14692407](https://www.doi.org/10.26434/chemrxiv.14692407).
