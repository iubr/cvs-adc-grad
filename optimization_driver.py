#               
#                               ADC_GRADIENT
#	  --------------------------------------------------------------------
#                 a module for CVS-ADC analytical gradients
#
#     Copyright (C) 2021 by I. E. Brumboiu
#
#     This file is part of adc_gradient 
#
#     adc_gradient is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     adc_gradient is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#     ----------------------------------------------------------------------

import geometric
import tempfile 
import os
import sys
import contextlib
import adcc
import adc_gradient
import adc_optimization_engine as engine

no = []
try:
    import pyscf
    from pyscf import grad
except ImportError:
    no.append('pyscf')

try:
    import veloxchem as vlx
except ImportError:
    no.append('vlx')

try:
    import psi4
except ImportError:
    no.append('psi4')

try:
    import daltonproject as dp
except ImportError:
    no.append('dp')

if len(no)==4:
    errtxt="At least one of the following "
    errtxt += "electronic structure codes is required: \n"
    for code in no:
        errtxt = errtxt + code + ' '
    raise ImportError(errtxt)

class ADCOptimizationDriver:
    """Enables geometry optimization using geomeTRIC 
       and the ADC energy and gradient computed with adcc and
       adc_gradient
    """

    #TODO: bring to the same format as in veloxchem
    # move instace variables from __init__ to update_settings
    def __init__(self, backend='pyscf', coordsys='tric', state=0,
                 name='tmp_default', maxiter=300, constraints=None):
        """
        Initializes the adc optimization driver.
        
        Parameters:
        ----------
        backend  : code to use for the scf reference state; possible values:
                   pyscf, vlx, psi4, dp? Working: only pyscf
        coordsys : system of coordinates used by geomeTRIC;
                   possible values: tric, dlc, hdlc
        state    : index of the excited state to be optimized
        name     : temporary file name

        """
        self.coordsys = coordsys
        self.backend = backend
        self.state = state 
        self.constraints = constraints
        self.hessian = 'never'
        self.transition = False
        self.check_interval = 0
        self.name = name
        self.maxiter = maxiter

    def compute(self, molecule, scf_drv, adc_drv, grad_drv, verbose=False):
        """
        Performs the geometry optimization
    
        Parameters:
        ----------
        molecule: molecule to be optimized; format for backend
        scf_drv : scf driver
        adc_drv : adc driver
        grad_drv: gradiet driver 
    
        Returns:
        -------
        The molecule with final geometry.
        """

        # create custom optimization engine
        opt_engine = engine.ADCOptimizationEngine(molecule, scf_drv, adc_drv,
                                                  grad_drv, backend=self.backend,
                                                  state=self.state, verbose=verbose,
                                                  maxiter=self.maxiter)

        #prepare input filename for geometric
        #TODO: better filename? as in veloxchem?
        input_fname = self.name

        with tempfile.TemporaryDirectory() as tmp_dir:

            with contextlib.redirect_stdout(sys.stdout):
                with contextlib.redirect_stderr(sys.stdout):

                    m = geometric.optimize.run_optimizer(
                                    customengine=opt_engine,
                                    coordsys=self.coordsys,
                                    check=self.check_interval,
                                    constraints=self.constraints,
                                    transition=self.transition,
                                    hessian=self.hessian,
                                    input=input_fname,
                                    maxiter=self.maxiter)

        coords = m.xyzs[-1] #in Angstrom, for Bohr do / geometric.nifty.bohr2ang
        energy = m.qm_energies[-1]
        return coords, energy

    def get_gradient_internal(self, molecule, scf_drv, adc_drv,
                              grad_drv, verbose=False):
        """
        Use functions from geometric to transform the gradient
        from cartesian to internal coordinates 
        """

        opt_engine = engine.ADCOptimizationEngine(molecule, scf_drv, adc_drv,
                                                  grad_drv, backend=self.backend,
                                                  state=self.state, verbose=verbose,
                                                  maxiter=self.maxiter)

        input_fname = self.name

        with tempfile.TemporaryDirectory() as tmp_dir:
            with contextlib.redirect_stdout(sys.stdout):
                with contextlib.redirect_stderr(sys.stdout):
                    dirname = input_fname+"_dir.tmp"

                    params = geometric.params.OptParams(customengine=opt_engine,
                                    coordsys=self.coordsys,
                                    check=self.check_interval,
                                    constraints=self.constraints,
                                    transition=self.transition,
                                    hessian=self.hessian,
                                    input=input_fname,
                                    maxiter=self.maxiter,
                                    dirname=dirname)

                    Molec, my_engine = geometric.prepare.get_molecule_engine(
                                    customengine=opt_engine,
                                    coordsys=self.coordsys,
                                    check=self.check_interval,
                                    constraints=self.constraints,
                                    transition=self.transition,
                                    hessian=self.hessian,
                                    input=input_fname,
                                    maxiter=self.maxiter,
                                    dirname=dirname)
                    coords = Molec.xyzs[0].flatten() * geometric.nifty.ang2bohr
                    #TODO: enable use of constraints through constraints, CVals
                    IC = geometric.internal.DelocalizedInternalCoordinates(Molec,
                            build=True, connect=True, addcart=False,
                            constraints=None,
                            conmethod=params.conmethod)
                
                    calc_results = my_engine.calc(coords, dirname)
                    gradient = calc_results["gradient"]
                    internals = IC.Prims.calculate(coords)
                    grad_internal = IC.Prims.calcGrad(coords, gradient)

        return internals, grad_internal, gradient
